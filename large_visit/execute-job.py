#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import sys, time, urllib.request, json, pprint, subprocess, re, os
from pyvirtualdisplay import Display

import boto3, signal
from threading import Thread, Event
from PIL import Image, ImageFont, ImageDraw 
import io

from traceback import format_exc

windowsize = (1600, 1200)
proxylocation = "0.0.0.0:8080"
proxybypass = [ "localhost", "127.0.0.1", "0.0.0.0" ]
AWSRegion = "us-east-1"
QueueName = "xxx"
S3BucketInputFileFormat = "s3://StevenVanAcker_OriginManifest/inputdata/{}.json"
TotalWaitTimeout = 60 # for SQS
MaxPageLoadTime = 300 # for the browser
ReadTimeout = TotalWaitTimeout if TotalWaitTimeout <= 20 else 20
maxPasses = 1
maxTriesOnBrowserException = 1 # setting this higher will ruin the statistics!!
maxBrowserRestartTries = 10
mitmdumpMaxMemSize = 90000 # KB
takeScreenshots = False
maxURLsToVisit = 20 # 0 = unlimited
sleepAfterProxyRestart = 5
sleepAfterBrowserRestart = 3
proxyIsUpLocation = "http://ignorethisselftesturl:9999/selftest.json"
proxyIsUpLocationSleepTime = 3
proxyIsUpLocationRetries = 10




proxyinfo = {
    "pre-tcp-counter": {
        "type": "tcp",
        "port": 8080,
        "operations": ["restart", "reset", "saveLogs", "exit", "resetAccounting"],
        "log": "/opt/data/pre-tcp-counter.log"
    },
    "pre-http-counter": {
        "type": "http",
        "port": 8081,
        "operations": ["restart", "avoidMemoryLeak", "reset", "saveLogs", "exit", "resetAccounting"],
    },
    "clientproxy": {
        "type": "http",
        "port": 8180,
        "operations": ["restart", "avoidMemoryLeak", "reset", "saveLogs", "exit"],
    },
    "mid-tcp-counter": {
        "type": "tcp",
        "port": 8280,
        "operations": ["restart", "reset", "saveLogs", "exit", "resetAccounting"],
        "log": "/opt/data/mid-tcp-counter.log"
    },
    "mid-http-counter": {
        "type": "http",
        "port": 8281,
        "operations": ["restart", "avoidMemoryLeak", "reset", "saveLogs", "exit", "resetAccounting"],
    },
    "serverproxy": {
        "type": "http",
        "port": 8380,
        "operations": ["restart", "avoidMemoryLeak", "reset", "saveLogs", "exit", "activateOriginManifest"],
    },
    "post-tcp-counter": {
        "type": "tcp",
        "port": 8480,
        "operations": ["restart", "reset", "saveLogs", "exit", "resetAccounting"],
        "log": "/opt/data/post-tcp-counter.log"
    },
    "post-http-counter": {
        "type": "http",
        "port": 8481,
        "operations": ["restart", "avoidMemoryLeak", "reset", "saveLogs", "exit", "resetAccounting"],
    },
    "outproxy": {
        "type": "http",
        "port": 8580,
        "operations": ["restart", "avoidMemoryLeak"],
    }
}

driver = None

display = Display(visible=0, size=windowsize)  
display.start()


def log(m): #{{{
    currtime = time.ctime()
    msg = "\n".join("[{}] {}".format(currtime, x) for x in m.split("\n"))
    if "PYTHONIOENCODING" in os.environ and os.environ["PYTHONIOENCODING"] in ["utf8", "utf-8"]:
        print(msg)
    else:
        print("PYTHONIOENCODING not set to utf8, not bothering printing...")
    with open("joblog.txt", "a", encoding="utf-8") as fp:
        print(msg, file=fp, flush=True)
#}}}
def queryProxyURL(proxyurl, url): #{{{
    proxy = urllib.request.ProxyHandler({'http': proxyurl })
    opener = urllib.request.build_opener(proxy)
    urllib.request.install_opener(opener)
    result = urllib.request.urlopen(url)
    try:
        data = json.loads(result.read().decode("utf-8"))
        return data
    except:
        return None
#}}}
def queryProxy(name, path): #{{{
    rec = proxyinfo[name]
    proxyurl = '0.0.0.0:{}'.format(rec["port"])
    targeturl = 'http://{}{}'.format(name, path)
    return queryProxyURL(proxyurl, targeturl)
#}}}

def sysexec(cmd): #{{{
    log("Executing command: {}".format(cmd))
    (exitcode, output) = subprocess.getstatusoutput(cmd)
    indentedoutput = "\n".join(["    {}".format(x) for x in output.split("\n")])
    log(indentedoutput)
    log("Exitcode: {}".format(exitcode))
#}}}

def checkThatThingsAreUpAndRunning(): #{{{
    for i in range(proxyIsUpLocationRetries):
        try:
            log("Checking proxyIsUpLocation {}: attempt {} of {}".format(proxyIsUpLocation, i+1, proxyIsUpLocationRetries))
            data = queryProxyURL(proxylocation, proxyIsUpLocation)
            if data["hello"] != "world":
                raise Exception("")
            log("checkThatThingsAreUpAndRunning(): all proxies are running")
            return
        except:
            log("proxyIsUpLocation {} did not serve the expected data (yet): attempt {} of {}".format(proxyIsUpLocation, i+1, proxyIsUpLocationRetries))
            time.sleep(proxyIsUpLocationSleepTime)

    raise Exception("proxyIsUpLocation {} did not serve the expected data after {} attempts".format(proxyIsUpLocation, proxyIsUpLocationRetries))
#}}}
def proxyOperation(op, target=None): # {{{
    afterSleep = False
    matchingEntries = [k for (k,v) in proxyinfo.items() if op in v["operations"]]
    for e in matchingEntries:
        log("proxyOperation({}, {}), entry {}".format(op, target, e))
        pt = proxyinfo[e]["type"]

        if op in ["restart", "avoidMemoryLeak"]: # {{{
            sysexec("supervisorctl restart {}".format(e))
            afterSleep = True
        #}}}
        elif op in ["reset", "resetAccounting"]: # {{{
            if pt == "tcp":
                sysexec("supervisorctl signal SIGUSR2 {}".format(e))
            elif pt == "http":
                queryProxy(e, "/reset")
        #}}}
        elif op == "saveLogs": # {{{
            if pt == "tcp":
                # send signal to dump log
                sysexec("supervisorctl signal SIGUSR1 {}".format(e))
                # copy logfile to target
                fn = proxyinfo[e]["log"]
                sysexec("mv {} {}-{}.txt".format(fn, e, target))
                pass
            elif pt == "http":
                json.dump(queryProxy(e, "/getstate"), open("{}-{}-getstate.json".format(e, target), "w"))
                json.dump(queryProxy(e, "/info"), open("{}-{}-info.json".format(e, target), "w"))
        #}}}
        elif op == "exit": # {{{
            if pt == "tcp":
                sysexec("supervisorctl stop {}".format(e))
            elif pt == "http":
                try:
                    queryProxy(e, "/exit")
                except:
                    pass
        #}}}
        elif op == "activateOriginManifest": #{{{
            queryProxy(e, "/dump/ver{}".format(target))
        #}}}

    # save client and server manifests
    if op == "saveLogs":
        sysexec("cp -r /opt/data/* .")

    if afterSleep:
        time.sleep(sleepAfterProxyRestart)

    if op in ["restart", "avoidMemoryLeak"]: # {{{
        checkThatThingsAreUpAndRunning()
    #}}}
#}}}


def restartBrowser(): #{{{
    global maxBrowserRestartTries
    def inner_restartBrowser(): #{{{
        global driver, windowsize
        if driver != None:
            driver.quit()
            driver = None

        chrome_options = webdriver.ChromeOptions()

        for arg in "--ignore-certificate-errors --disable-component-update --disable-translate --disable-sync --disable-infobars --disable-domain-reliability --disable-cloud-import --disable-captive-portal-bypass-proxy --disable-background-networking --metrics-recording-only --safebrowsing-disable-auto-update".split(" "):
            chrome_options.add_argument(arg)
            
        chrome_options.add_argument('--proxy-server={}'.format(proxylocation))
        chrome_options.add_argument('--proxy-bypass-list="{}"'.format(";".join(proxybypass)))
        # chrome_options.add_argument('--load-extension=/opt/origin-manifest-extension/')

        # enable browser logging
        desired_capabilities = DesiredCapabilities.CHROME
        desired_capabilities['loggingPrefs'] = { 'browser':'ALL' }

        driver = webdriver.Chrome(chrome_options=chrome_options, desired_capabilities=desired_capabilities)
        driver.implicitly_wait(10)
        time.sleep(sleepAfterBrowserRestart)
        # driver = webdriver.Chrome(chrome_options=chrome_options)
        driver.set_window_size(*windowsize)
        driver.set_page_load_timeout(MaxPageLoadTime)
    #}}}
    _exc = None
    for i in range(5):
        try:
            inner_restartBrowser()
            return
        except Exception as e:
            log("Restarting browser failed... attempt {} of {}".format(i+1, maxBrowserRestartTries))
            time.sleep(sleepAfterBrowserRestart)
            _exc = e
    
    log("Restarting browser failed... giving up after {} attempts".format(maxBrowserRestartTries))
    raise _exc
#}}}
def visitURL(url, waittime): #{{{
    global driver, maxTriesOnBrowserException 
    lastException = None
    for t in range(maxTriesOnBrowserException):
        try:
            driver.get(url)
            time.sleep(waittime)
            if takeScreenshots:
                return Image.open(io.BytesIO(driver.get_screenshot_as_png()))
            return None
        except Exception as e:
            log("visitURL exception on attempt {}/{}: MaxPageLoadTime={}".format(t+1, maxTriesOnBrowserException, MaxPageLoadTime))
            log(format_exc())
            lastException = e
    if lastException != None:
        log("visitURL giving up :(")
        raise lastException
    return None
#}}}
class SnoozeResult(Thread): #{{{
    def __init__(self, msg, sleeptime=10, snoozetime=20):
        Thread.__init__(self)
        self.stopped = Event()
        self.msg = msg
        self.snoozeTime = snoozetime
        self.sleepTime = sleeptime

    def stop(self):
        self.stopped.set()

    def run(self):
        while not self.stopped.wait(self.sleepTime):
            log("Snoozing {} seconds".format(self.snoozeTime))
            try:
                self.msg.change_visibility(VisibilityTimeout = self.snoozeTime)
            except:
                log("Error snoozing...")
                self.stopped.set()
#}}} 

def tooMuchMemoryLeaked(): #{{{
    output = subprocess.getoutput("pgrep mitmdump | xargs ps -ho rss | sort -nr | head -1")
    if output == "":
        return False # no mitmdump running
    memsize = int(output)
    fucked = memsize > mitmdumpMaxMemSize
    if fucked:
        log("At least one of the mitmdump process uses more than {} KB RSS: {} KB".format(mitmdumpMaxMemSize, memsize))
    return fucked
#}}}

def getInputData(job, localfn): # {{{
    remotefn = S3BucketInputFileFormat.format(job)
    try:
        m = re.match(r'^s3://([^/]+)/(.*)', remotefn)
        bucket = m.group(1)
        s3fn = m.group(2)
        log("Reading job {} inputfile {} --> S3 bucket {}, filename {}".format(job, remotefn, bucket, s3fn))
        s3 = boto3.resource("s3", region_name=AWSRegion)
        bucket = s3.Bucket(bucket)
        bucket.download_file(s3fn, localfn)
        
        inputdata = json.load(open(localfn))

        log("Read inputdata:")
        log(pprint.pformat(inputdata))

        for k in ["domain", "urls", "successResultsFile", "errorResultsFile"]:
            if not k in inputdata:
                log("Couldn't find key '{}' in inputdata!!".format(k))
                raise ValueError("Couldn't find key '{}' in inputdata!!".format(k))

        if maxURLsToVisit > 0:
            inputdata["urls"] = inputdata["urls"][:maxURLsToVisit]
            log("Limited the URL count to {}".format(maxURLsToVisit))
            log("New inputdata:")
            log(pprint.pformat(inputdata))
        return inputdata
    except Exception as e:
        log("Failed reading input file for job {} from {}".format(job, remotefn))
        raise e

    return None
#}}}
def saveResultData(localfn, remotefn): # {{{
    m = re.match(r'^s3://([^/]+)/(.*)', remotefn)
    if m:
        bucket = m.group(1)
        s3fn = m.group(2)
        log("Saving results {} to {} --> S3 bucket {}, filename {}".format(localfn, remotefn, bucket, s3fn))
        s3 = boto3.resource("s3", region_name=AWSRegion)
        bucket = s3.create_bucket(Bucket=bucket)
        bucket.upload_file(localfn, s3fn)
    else:
        log("Saving results {} to {}".format(localfn, remotefn))
        sysexec("cp {} {}".format(localfn, remotefn))
    log("Results saved")
#}}}


def largeVisit_HandleMessage(msg): # {{{
    idleTime = 3
    _exc = None

    log("Starting largeVisit_HandleMessage('{}')".format(msg))
    inputdata = getInputData(msg, "inputdata.txt")
    urls = inputdata["urls"]
    screenshots = [{
            "uncached-before": None,
            "cached-before": None,
            "uncached-after": None,
            "cached-after": None,
        }] # we count from 1, so first is empty....

    try:
        proxyOperation("restart")
        proxyOperation("reset")

        log("restarting browser")
        restartBrowser()

        for (i, url) in enumerate(urls, start=1):
            log("visiting url uncached before manifest {}/{} --> {}".format(i, len(urls), url))
            screenshots += [{}]
            screenshots[i]["uncached-before"] = visitURL(url, idleTime)
            if tooMuchMemoryLeaked():
                log("Invoking avoidMemoryLeak for fun and profit")
                proxyOperation("avoidMemoryLeak")
                time.sleep(1)
        proxyOperation("saveLogs", "before-uncached")
        proxyOperation("resetAccounting")

        for (i, url) in enumerate(urls, start=1):
            log("visiting url cached before manifest {}/{} --> {}".format(i, len(urls), url))
            screenshots[i]["cached-before"] = visitURL(url, idleTime)
            if tooMuchMemoryLeaked():
                log("Invoking avoidMemoryLeak for fun and profit")
                proxyOperation("avoidMemoryLeak")
                time.sleep(1)
        proxyOperation("saveLogs", "before-cached")
        proxyOperation("resetAccounting")

        proxyOperation("activateOriginManifest", 0)
        log("restarting browser")
        restartBrowser()

        for (i, url) in enumerate(urls, start=1):
            log("visiting url uncached after manifest {}/{} --> {}".format(i, len(urls), url))
            screenshots[i]["uncached-after"] = visitURL(url, idleTime)
            if tooMuchMemoryLeaked():
                log("Invoking avoidMemoryLeak for fun and profit")
                proxyOperation("avoidMemoryLeak")
                time.sleep(1)
        proxyOperation("saveLogs", "after-uncached")
        proxyOperation("resetAccounting")

        for (i, url) in enumerate(urls, start=1):
            log("visiting url cached after manifest {}/{} --> {}".format(i, len(urls), url))
            screenshots[i]["cached-after"] = visitURL(url, idleTime)
            if tooMuchMemoryLeaked():
                log("Invoking avoidMemoryLeak for fun and profit")
                proxyOperation("avoidMemoryLeak")
                time.sleep(1)
        proxyOperation("saveLogs", "after-cached")

        # save all screenshots...
        if takeScreenshots:
            if not os.path.exists("screenshots"):
                os.makedirs("screenshots")
            for (i, url) in enumerate(urls, start=1):
                scfn = "screenshots/{0:06d}.png".format(i)
                log("saving screenshot {} for url {}/{}: {}".format(scfn, i, len(urls), url))
                imgwidth = max([x.size[0] for k,x in screenshots[i].items()])
                imgheight = max([x.size[1] for k,x in screenshots[i].items()])
                outimg = Image.new('RGBA', (imgwidth * 2, imgheight * 2))
                outimg.paste(screenshots[i]["uncached-before"], (0, 0))                 # top-left:     uncached before
                outimg.paste(screenshots[i]["cached-before"],   (0, imgheight))         # bottom-left:  cached before
                outimg.paste(screenshots[i]["uncached-after"],  (imgwidth, 0))          # top-right:    uncached after
                outimg.paste(screenshots[i]["cached-after"],    (imgwidth, imgheight))  # bottom-right: cached after
                outimg.save(scfn)

    except Exception as e:
        log("Pass failed: {}".format(e))
        log(format_exc())
        _exc = e

    if driver != None:
        log("Quitting driver")
        driver.quit()

    # exit proxies
    proxyOperation("exit")

    log("Done.")

    # zip results
    sysexec("tar --xz -vcf /tmp/results.tar.xz *")

    # upload results
    if _exc == None:
        saveResultData("/tmp/results.tar.xz", inputdata["successResultsFile"])
    else:
        saveResultData("/tmp/results.tar.xz", inputdata["errorResultsFile"])
        raise _exc
#}}}

sqs = boto3.resource('sqs', region_name=AWSRegion)
q = sqs.get_queue_by_name(QueueName=QueueName)
startTime = time.time()
st = None

while time.time() <= startTime + TotalWaitTimeout:
    mlist = q.receive_messages(WaitTimeSeconds = ReadTimeout)

    if len(mlist) > 0:
        msg = mlist[0]
        log("Handling message: {}".format(msg.body))
        st = SnoozeResult(msg)
        st.start()

        try:
            largeVisit_HandleMessage(msg.body)
        except Exception as e:
            log("Failed to handle message because: {}".format(format_exc()))
            st.stop()
            st = None
            raise e
            sys.exit(1)

        log("Finished handling message")
        st.stop()
        st = None
        msg.delete()
        sys.exit(0)
    else:
        log("No message queued for {} seconds, sleeping...".format(ReadTimeout))
if st != None:
    st.stop()
log("No message queued after {} seconds, exiting.".format(TotalWaitTimeout))


