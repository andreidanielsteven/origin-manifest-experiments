#!/usr/bin/env python3

import boto3, sys
from itertools import *
from multiprocessing.dummy import Pool

POOLCOUNT = 10
CHUNKSIZE = 10

q = None

def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)

def proc_init():
    global q
    session = boto3.session.Session()
    sqs = session.resource('sqs')
    q = sqs.get_queue_by_name(QueueName=globalQueueName)

def map_func(m):
    global q
    q.send_message(MessageBody=m)

def map_func_arr(a):
    global q
    entries = [{"Id": str(i), "MessageBody": a[i]} for i in range(len(a)) if a[i] != None]
    q.send_messages(Entries=entries)

globalQueueName = sys.argv[1]
pool = Pool(POOLCOUNT, initializer=proc_init)
msgs = iter([x.strip() for x in open(sys.argv[2], encoding="utf8")])

results = pool.map(map_func_arr, [list(x) for x in grouper(msgs, CHUNKSIZE)] )
