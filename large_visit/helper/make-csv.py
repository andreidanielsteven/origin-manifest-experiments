#!/usr/bin/env python3

import sys, json, pprint

def getField(rec, name):
    fields = name.split(".")
    if len(fields) == 1:
        return rec[name]
    else:
        return getField(rec[fields[0]], ".".join(fields[1:]))


def printSummary(f, field):
    data = {}
    try:
        data = json.load(open(f))
    except:
        return

    experiments = ["{}-{}".format(y, x) for y in ["before", "after"] for x in ["uncached", "cached"]]
    locations = ["pre", "mid", "post"]

    out = []

    for e in experiments:
        ref = None
        for l in locations:
            if ref == None:
                # ref = getField(data[e][l], field)
                ref = 1
                if ref == 0:
                    return
            scaled = getField(data[e][l], field) / ref
            out += [scaled]
    
    print("\t".join(["{}".format(x) for x in out])) 

for i in range(10000):
    fn = "{:06}.json".format(i)
    printSummary(fn, sys.argv[1])

