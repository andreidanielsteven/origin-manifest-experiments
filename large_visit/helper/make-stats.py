#!/usr/bin/env python3

# Goal: cleanup data for a single experiment
# we don't care about the order within a single proxylocation

# How we do this:
# per proxylocation, we keep a bucket of a url+method combination
# then we truncate all buckets so they have the same length per experiment

# stuff to remove:
#    selenium links
#    proxy test links
#    broken records

import sys, tarfile, json, pprint, os
from urllib.parse import urlparse

proxylocations = ["pre", "mid", "post"]
experimentnames = [y.format(x) for y in ["{}-uncached", "{}-cached"] for x in ["before", "after"]]
fformat = "{}-http-counter-{}-getstate.json"

def readFileFromTarball(tarball, fn):
    if os.path.isdir(tarball):
        newfn = "{}/{}".format(tarball, fn)
        return open(newfn).read()
    else:
        with tarfile.open(tarball) as tb:
            return tb.extractfile(fn).read().decode("utf-8")

def getMethodFromKey(k):
    (m, u) = k
    return m

def getURLFromKey(k):
    (m, u) = k
    return u

def isManifestURLKey(k):
    return isManifestURL(getURLFromKey(k))
    #return k.startswith("GET-") and isManifestURL(k[4:])

def isManifestURL(url):
    urlparts = urlparse(url)
    return urlparts.path == "/.well-known/origin-policy/ver0"

def isPreflight(rec):
    if rec["method"] != "OPTIONS":
        return False

    headerdict = dict([(hv[0].lower(), hv[1]) for hv in rec["response_headers"]])
    if "access-control-allow-origin" not in headerdict:
        return False

    if "access-control-allow-methods" not in headerdict:
        return False

    return True

def isOurPreflight(rec):
    headerdict = dict([(hv[0].lower(), hv[1]) for hv in rec["response_headers"]])
    if "server" in headerdict and headerdict["server"] != "mitmproxy 2.0.2":
        return False

    return isPreflight(rec)

def safeToIgnoreURL(url):
    urlparts = urlparse(url)

    # These come from selenium... http://localhost:41857/shutdown
    if urlparts.hostname == "localhost": 
        return True

    if url == "http://ignorethisselftesturl:9999/selftest.json":
        return True

    return False

def isValidRecord(rec):
    headerdict = dict([(hv[0].lower(), hv[1]) for hv in rec["response_headers"]])
    # mitmproxy replies to OPTIONS requests, the other records are broken
    if "server" in headerdict and headerdict["server"] == "mitmproxy 2.0.2" and rec["method"] != "OPTIONS":
        return False
    return True
    
def getAllData(tarball):
    out = {}
    for e in experimentnames:
        out[e] = {}
        for l in proxylocations:
            fn = fformat.format(l, e)
            data = json.loads(readFileFromTarball(tarball, fn))
            data = [x for x in data if not safeToIgnoreURL(x["url"])]
            out[e][l] = data
    return out

def getAllURLs(alldata):
    out = set()
    for (e, edata) in alldata.items():
        for (l, ldata) in alldata[e].items():
            out.update([x["url"] for x in alldata[e][l]])
    return list(out)

def getURLStats(data, k):
    url = getURLFromKey(k)

    out = {
        "count": 0,
        "methods": [],
        "request": {
            "headersize": 0,
            "totalsize": 0,
            "manifestheader": 0,
        },
        "response": {
            "headersize": 0,
            "totalsize": 0,
            "manifestheader": 0,
        },
        "cors-preflight": {
            "count": 0,
            "request": {
                "headersize": 0,
                "totalsize": 0,
            },
            "response": {
                "headersize": 0,
                "totalsize": 0,
            },
        }
        #"ignore": False
    }

    reclist = []
    for urlrec in data: 
        reclist += [urlrec]
        out["url"] = url
        out["count"] += 1
        if isPreflight(urlrec):
            out["cors-preflight"]["count"] += 1

        out["methods"] += [urlrec["method"]]

        for direction in ["request", "response"]:
            headerdict = dict([(hv[0].lower(), hv[1]) for hv in urlrec["{}_headers".format(direction)]])
            cl = 0
            # if there is a content-length, there is a body...
            # unless it's a HEAD request
            if "content-length" in headerdict and urlrec["method"] != "HEAD":
                cl = int(headerdict["content-length"])

            thisheadersize = 0

            firstline = ""
            if direction == "request":
                urlparts = urlparse(url)
                firstline = "{} {} HTTP/x.x\n".format(urlrec["method"], urlparts.path)
            elif direction == "response":
                firstline = "HTTP/1.1 200 OK\n" # we assume code 200
            thisheadersize += len(firstline)

            thisheadersize += sum(len("{}: {}\n".format(hv[0], hv[1])) for hv in urlrec["{}_headers".format(direction)])
            thisheadersize += 1 # newline after the header

            out[direction]["headersize"] += thisheadersize
            out[direction]["totalsize"] += (thisheadersize + cl)

            if "sec-origin-policy" in headerdict:
                out[direction]["manifestheader"] += len("sec-origin-policy") + 2 + len(headerdict["sec-origin-policy"]) + 1

            if isPreflight(urlrec):
                out["cors-preflight"][direction]["headersize"] += thisheadersize
                out["cors-preflight"][direction]["totalsize"] += (thisheadersize + cl)
                
    out["methods"] = list(set(out["methods"]))
    return reclist, out

def getCleanData(alldata):
    # first, process all experiments individually
    tmpdata = {}
    for e in experimentnames:
        bucket = {}
        for l in proxylocations:
            bucket[l] = {}
            # create an entry bucket for each URL+method we visited
            for rec in alldata[e][l]:
                url = rec["url"]
                method = rec["method"]
                #key = "{}-{}".format(method, url)
                key = (method, url)

                if isValidRecord(rec) and not safeToIgnoreURL(url):
                    if key not in bucket[l]:
                        bucket[l][key] = []
                    bucket[l][key] += [rec]

        outbucket = {}
        for l in proxylocations:
            outbucket[l] = {}
        # now, compute all keys we've seen so far
        allkeys = list(set(sum([list(b.keys()) for l,b in bucket.items()], [])))

        # for each key, check how many entries are in the bucket for each proxylocation
        # caveat! We know that manifest URLs are only seen once, so keep all of them when they are seen
        # caveat! We know the same about preflight requests!
        for k in allkeys:
            # for each key, look in each proxy location bucket and make sure we have the same amount in the dataset
            # exception: manifest URLs are only seen in "mid"
            # exception: preflight requests coming from mitmproxy are only seen in "pre"

            # bucket["pre"][k] = [...]

            if isManifestURLKey(k):
                # we are handling a manifest URL, only seen at "mid"
                # clear "pre" and "post", and put the entire bucket in "mid"
                outbucket["pre"][k] = []
                outbucket["mid"][k] = bucket["mid"][k]
                outbucket["post"][k] = []

            else:
                # if we are dealing with preflights, then there are 2 options:
                # if the preflight is answered by mitmproxy in "pre", then it's one of our own and we keep it in a safe place
                # all other preflight requests are handled as normal responses
                # afterwards, we add the preflights back to the buckets
                ourpreflights = {}
                for l in proxylocations:
                    ourpreflights[l] = []

                if getMethodFromKey(k) == "OPTIONS":
                    # we split each bucket in 2: our preflights and the rest
                    for l in proxylocations:
                        if k not in bucket[l]:
                            bucket[l][k] = []

                        ours = list(filter(isOurPreflight, bucket[l][k]))
                        notours = list(filter(lambda v: not isOurPreflight(v), bucket[l][k]))
                        ourpreflights[l] = ours
                        bucket[l][k] = notours
                        #if len(ours) > 0:
                        #    print("Holy moly! Found some preflights in {}: {}".format(l, len(ours)))
            
                maxsize = None
                for l,b in bucket.items():
                    # the key must be in each location's bucket, but we only take the same amount in each bucket
                    if k in b:
                        bucklen = len(b[k])
                    else:
                        bucklen = 0
                    
                    if maxsize == None:
                        maxsize = bucklen
                    else:
                        maxsize = min(maxsize, bucklen)
                            
                if maxsize == None:
                    maxsize = 0

                for l in bucket.keys():
                    if maxsize > 0:
                        outbucket[l][k] = bucket[l][k][:maxsize]
                    else:
                        outbucket[l][k] = []

                if getMethodFromKey(k) == "OPTIONS":
                    # we add our preflights to the outbucket
                    # and restore the original buckets
                    for l in proxylocations:
                        bucket[l][k] += ourpreflights[l]
                        if l == "pre":
                            outbucket[l][k] += ourpreflights[l]

        tmpdata[e] = outbucket

    # then, only retain URLs that were observed in all experiments
    allkeys = set()
    for e, edata in tmpdata.items():
        for l, ldata in edata.items():
            allkeys.update(ldata.keys())

    out = {}
    for e, edata in tmpdata.items():
        out[e] = {}
        for l, ldata in edata.items():
            #out[e][l] = sum([recs for k, recs in ldata.items() if k in allkeys], [])
            out[e][l] = dict([(k, d) for k, d in ldata.items() if k in allkeys])

    return out

def summarize(res): # FIXME: cors stuff
    stats = {
        "methods": {
        },
        "count": 0,
        "manifeststuff": {
            "count": 0,
            "requests": {
                "headersize": 0,
                "totalsize": 0
            },
            "responses": {
                "headersize": 0,
                "totalsize": 0
            },
            "traffic": {
                "headersize": 0,
                "totalsize": 0
            }
        },
        "cors-preflight": {
            "count": 0,
            "requests": {
                "headersize": 0,
                "totalsize": 0
            },
            "responses": {
                "headersize": 0,
                "totalsize": 0
            },
            "traffic": {
                "headersize": 0,
                "totalsize": 0
            }
        },
        "requests": {
            "headersize": 0,
            "totalsize": 0
        },
        "responses": {
            "headersize": 0,
            "totalsize": 0
        },
        "traffic": {
            "headersize": 0,
            "totalsize": 0,
            "manifestheader": 0,
        }
    }

    for (k,rec) in res.items():
        method = getMethodFromKey(k)
        url = getURLFromKey(k)
        
        if method not in stats["methods"]:
            stats["methods"][method] = 0
        stats["methods"][method] += rec["count"]
        stats["count"] += rec["count"]
        stats["requests"]["headersize"] += rec["request"]["headersize"]
        stats["requests"]["totalsize"]  += rec["request"]["totalsize"]
        stats["responses"]["headersize"] += rec["response"]["headersize"]
        stats["responses"]["totalsize"]  += rec["response"]["totalsize"]
        stats["traffic"]["headersize"]  += rec["request"]["headersize"] + rec["response"]["headersize"]
        stats["traffic"]["totalsize"]  += rec["request"]["totalsize"] + rec["response"]["totalsize"]

        stats["traffic"]["manifestheader"]  += rec["request"]["manifestheader"] + rec["response"]["manifestheader"]

        if isManifestURL(url):
            stats["manifeststuff"]["count"] += rec["count"]
            stats["manifeststuff"]["requests"]["headersize"] += rec["request"]["headersize"]
            stats["manifeststuff"]["requests"]["totalsize"]  += rec["request"]["totalsize"]
            stats["manifeststuff"]["responses"]["headersize"] += rec["response"]["headersize"]
            stats["manifeststuff"]["responses"]["totalsize"]  += rec["response"]["totalsize"]
            stats["manifeststuff"]["traffic"]["headersize"]  += rec["request"]["headersize"] + rec["response"]["headersize"]
            stats["manifeststuff"]["traffic"]["totalsize"]  += rec["request"]["totalsize"] + rec["response"]["totalsize"]

        stats["cors-preflight"]["count"] += rec["cors-preflight"]["count"]
        stats["cors-preflight"]["requests"]["headersize"] += rec["cors-preflight"]["request"]["headersize"]
        stats["cors-preflight"]["requests"]["totalsize"]  += rec["cors-preflight"]["request"]["totalsize"]
        stats["cors-preflight"]["responses"]["headersize"] += rec["cors-preflight"]["response"]["headersize"]
        stats["cors-preflight"]["responses"]["totalsize"]  += rec["cors-preflight"]["response"]["totalsize"]
        stats["cors-preflight"]["traffic"]["headersize"]  += rec["cors-preflight"]["request"]["headersize"] + rec["cors-preflight"]["response"]["headersize"]
        stats["cors-preflight"]["traffic"]["totalsize"]  += rec["cors-preflight"]["request"]["totalsize"] + rec["cors-preflight"]["response"]["totalsize"]

    return stats

def showDiff(rec1, rec2, rec3):
    tmp = {"rec-0pre.txt": rec1, "rec-1mid.txt": rec2, "rec-2post.txt": rec3}
    for fn, reclist in tmp.items():
        with open(fn, "w") as fp:
            for rec in reclist:
                print("URL: {}".format(rec["url"]), file=fp)
                print("method: {}".format(rec["method"]), file=fp)
                print("origin: {}".format(rec["origin"]), file=fp)
                for direction in ["request_headers", "response_headers"]:
                    print("{}:".format(direction), file=fp)
                    headers = sorted([(x[0].lower(), x[1]) for x in rec[direction]])
                    for x in headers:
                        print("    {}: {}".format(x[0], x[1]), file=fp)
    os.system("vimdiff rec*.txt")

# first, calculate stuff about all normal requests: seen over all datasets, without broken mitmproxy shit, no manifest URLs and no CORS
# next, do manifest stuff only, those will be in "mid"
# finally, do all CORS things, those will be in "pre", without matching "mid" and "post"

alldata = getAllData(sys.argv[1])
alldata = getCleanData(alldata)
allstats = {}

### for e in experimentnames:
###     for l in proxylocations:
###         try:
###             x = len(alldata[e][l])
###             print("{} {}: {}".format(e,l, x))
###         except:
###             print("{} {}: not ok".format(e,l))
###             with open(sys.argv[2], "w"):
###                 pass
###             sys.exit(0)


for e in experimentnames:
    recs = {}
    urldata = {}
    allstats[e] = {}
    for l in proxylocations:
        recs[l] = {}
        urldata[l] = {}
        for k, d in alldata[e][l].items():
            recs[l][k], urldata[l][k] = getURLStats(d, k)
        allstats[e][l] = summarize(urldata[l])

json.dump(allstats, open(sys.argv[2], "w"))
#pprint.pprint(allstats)

