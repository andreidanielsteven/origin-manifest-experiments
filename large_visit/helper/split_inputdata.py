#!/usr/bin/env python

import sys, json

dfile = sys.argv[1]
c = 0

for d in [x.strip() for x in open(dfile).readlines()]:
    data = {
        "domain": d,
        "urls": ["http://%s" % d],
        "successResultsFile": "s3://StevenVanAcker_OriginManifest/success/%06d.tar.xz" % c,
        "errorResultsFile": "s3://StevenVanAcker_OriginManifest/error/%06d.tar.xz" % c
    }

    json.dump(data, open("%06d.json" % c, "w"))
    c += 1

with open("joblist", "w") as fp:
    for i in range(c):
        fp.write("%06d\n" % i)
