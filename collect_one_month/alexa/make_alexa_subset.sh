#!/bin/bash

(
unzip -p top-1m.csv--2017-10-05.zip | awk 'NR<=200'
unzip -p top-1m.csv--2017-10-05.zip | awk 'NR>200 && NR<=1000' | shuf | head -200
unzip -p top-1m.csv--2017-10-05.zip | awk 'NR>1000 && NR<=10000' | shuf | head -200
unzip -p top-1m.csv--2017-10-05.zip | awk 'NR>10000 && NR<=100000' | shuf | head -200
unzip -p top-1m.csv--2017-10-05.zip | awk 'NR>100000 && NR<=1000000' | shuf | head -200
) | sort -n
