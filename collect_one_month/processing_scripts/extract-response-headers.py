#!/usr/bin/env python

import sys, json, pprint
import sqlite3
import urlparse

sqlfile = sys.argv[1]
outfile = sys.argv[2]

def originFromURL(url):
    urlparts = urlparse.urlparse(url)
    scheme = urlparts.scheme
    hostname = urlparts.hostname if urlparts.hostname != None else ""

    if urlparts.port == None:
        port = {
	    "http": "80",
	    "https": "443",
	}[scheme]
    else:
        port = str(urlparts.port)

    return "%s,%s,%s" % (scheme, hostname, port)

outdata = []

def writeHeaders(method, url, headers):
    global outdata
    origin = originFromURL(url)
    outdata += [{
	    "url": url,
        "method": method.upper(),
        "origin": origin,
        "headers": headers
    }]

conn = sqlite3.connect(sqlfile)
c = conn.cursor()
for method, url, headerjson in c.execute('SELECT method, url, headers FROM http_responses'):
    headers = json.loads(headerjson)
    writeHeaders(method, url, headers)

with open(outfile, "w") as fp:
    json.dump(outdata, fp)
