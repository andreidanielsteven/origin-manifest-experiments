#!/usr/bin/env python3

import sys, json, pprint

def addHeaders(base, new):
    for rec in new:
        h = rec[0].lower()
        if h not in base:
            base[h] = {
                "count": 0,
                "size": 0
            }
        base[h]["count"] += 1
        base[h]["size"] += len(rec[0]) + len(rec[1]) + 3 # colon, space and newline


headers = {}

for f in sys.argv[1:]:
    data = json.load(open(f))
    for rec in data:
        addHeaders(headers, rec["headers"])

for (h,c) in sorted(headers.items(), key=lambda x: x[1]["size"]):
    print("{}\t{}\t{}".format(c["size"], c["count"], h))
