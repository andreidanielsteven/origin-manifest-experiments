#!/usr/bin/env python3

import sys, json, bz2

out = set()

def myopen(fn):
    fp = None
    try:
        fp = bz2.open(fn, "rt")
        fp.peek(1)
    except Exception as e:
        fp = open(fn)
    return fp

for f in sys.argv[1:]:
    data = json.load(myopen(f))
    for rec in data:
        print(rec["method"])
