#!/usr/bin/env python3

import sys, json, bz2

out = set()
needle = sys.argv[1].lower()

def myopen(fn):
    fp = None
    try:
        fp = bz2.open(fn, "rt")
    except Exception as e:
        fp = open(fn)
    return fp

for f in sys.argv[2:]:
    data = json.load(myopen(f))
    for rec in data:
        for x in rec["headers"]:
            if needle in x[0].lower():
                out.add(tuple(x))
                #print("{}: {}".format(*x))
print(json.dumps(list(out)))
