#!/usr/bin/env python3

import sys, json, bz2

out = []

ignoremethods = ["GET", "POST", "HEAD"]

def myopen(fn):
    fp = None
    try:
        fp = bz2.open(fn, "rt")
    except Exception as e:
        fp = open(fn)
    return fp

for f in sys.argv[1:]:
    data = json.load(myopen(f))
    for rec in data:
        if not rec["method"] in ignoremethods:
            out += [rec]

print(json.dumps(out))
