#!/bin/bash

set -e

export PYTHONIOENCODING=utf-8

# We start from responses.json, which is the json file extracted from sqlite
# and we produce a tarball with the results

resps=$1
cutoff=$2
alexa=$3
outfile=$4

if [ "$resps" = "" -o "$cutoff" = "" -o "$alexa" = "" -o "$outfile" = "" ];
then
    echo "Usage: $0 <responses.json> <cutoff percentage> <alexa csv> <output tgz file>"
    exit 1
fi

alexa=$(readlink -f $3)
outfile=$(readlink -f $4)

tooldir=$(dirname $(readlink -f $0))
workdir=$(mktemp -d)

cp $resps $workdir/responses.json
cd $workdir
mkdir manifests
$tooldir/make-manifests.py responses.json manifests-compact.json $cutoff
$tooldir/write-manifests-to-files.py manifests-compact.json manifests/
$tooldir/simulate_manifests.py manifests-compact.json responses.json $alexa 2> /dev/null > simulation-results.csv

grep "^TOTAL" simulation-results.csv

tar -czf $outfile *
cd $tooldir
rm -rf $workdir

