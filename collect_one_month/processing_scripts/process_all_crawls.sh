#!/bin/bash

set -e
export PYTHONIOENCODING=utf-8

datadir=$(readlink -f $1)
alexafile=$(readlink -f $2)

tooldir=$(dirname $(readlink -f $0))

for dbf in $(find $datadir -name crawl-data.sqlite);
do
    dbf=$(readlink -f $dbf)
    respfile="$(dirname $dbf)/responses.json"

    if [ ! -e $respfile ];
    then
	$tooldir/extract-response-headers.py $dbf $respfile
    fi

    for cutoff in $(seq 50 5 100);
    do
	resultsfile="$(dirname $dbf)/results-$cutoff.tar.gz"
	if [ ! -e $resultsfile ];
	then
	    echo -n ""
	    # $tooldir/whole-pipeline.sh $respfile $cutoff $alexafile $resultsfile
	fi
    done

    bzip2 $dbf
    bzip2 $respfile
done

