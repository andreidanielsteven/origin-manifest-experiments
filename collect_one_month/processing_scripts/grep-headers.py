#!/usr/bin/env python3

import sys, json

out = []
needle = sys.argv[1].lower()

for f in sys.argv[2:]:
    data = json.load(open(f))
    for rec in data:
        if any(x[0].lower() == needle for x in rec["headers"]):
            out += [rec]

json.dump(out, sys.stdout)
