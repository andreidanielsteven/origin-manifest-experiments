#!/usr/bin/env python

import sys, json

manifests = json.load(open(sys.argv[1]))
outdir = sys.argv[2]

for origin, headers in manifests.items():
    fn = "%s/%s.json" % (outdir, origin)
    fn = fn.encode("ascii", "replace")
    with open(fn, "w") as fp:
        fp.write(("\n".join(["%s: %s" % (h,v) for (h,v) in headers.items()])).encode("utf-8"))

