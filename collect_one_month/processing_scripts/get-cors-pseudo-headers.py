#!/usr/bin/env python3

import sys, json, bz2, pprint

HACK_CORS_HEADERS = [x.lower() for x in [
    "access-control-allow-origin",
    "access-control-allow-credentials",
    "access-control-expose-headers",
    "access-control-max-age",
    "access-control-allow-methods",
    "access-control-allow-headers",
]]

def generatePseudoCORSHeader(headers): #{{{
    # To be able to use the CORS merger, all CORS headers must be combined into a pseudo header
    outdict = dict([tuple(x) for x in headers if x[0].lower() in HACK_CORS_HEADERS])
    #outdict = dict([(x.lower(),headers[x]) for x in HACK_CORS_HEADERS if x.lower() in headers])
    out = json.dumps(outdict)
    return out
#}}}

out = set()

def myopen(fn):
    fp = None
    try:
        fp = bz2.open(fn, "rt")
    except Exception as e:
        fp = open(fn)
    return fp

for f in sys.argv[1:]:
    data = json.load(myopen(f))
    for rec in data:
        psh = generatePseudoCORSHeader(rec["headers"])
        out.add(("x-pseudo-cors-consolidation-header", psh))
        #for x in rec["headers"]:
print(json.dumps(list(out)))
