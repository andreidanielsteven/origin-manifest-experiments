#!/usr/bin/env python

defaultbodysize = 1000

import sys, json, pprint, tldextract
import logging
logging.basicConfig()

# for each record, log:
#	origin
# 	how many bytes are transferred normally
# 	how many bytes are transferred with origin manifest (considering only security headers)
# 	how many bytes are transferred with origin manifest (considering all headers)

def readAlexa(fn):
    lines = [x.strip().split(",") for x in open(fn).readlines()]
    return dict([(y,int(x)) for (x,y) in lines])

manifests = json.load(open(sys.argv[1]))
reqs = json.load(open(sys.argv[2]))
alexaRanks = readAlexa(sys.argv[3])

def p(a,b):
    return "%0.4f" % (1.0 - (1.0 * a/b))

def getAlexaRank(dom):
    global alexaRanks
    if dom in alexaRanks:
        return alexaRanks[dom]
    return -1

def getDomainAndRank(origin):
    hn = origin.split(",")[1]
    tldinfo = tldextract.extract(hn)
    dom = tldinfo.registered_domain
    if dom == "":
	dom = "UNKNOWN"
    return (dom, getAlexaRank(dom))

def getBodysize(h):
    hlc = dict([(k.lower(),v) for (k,v) in h.items()])
    if "content-length" in hlc:
        return int(hlc["content-length"])
    return defaultbodysize

def getManifest(origin):
    global manifests
    if origin in manifests:
        return manifests[origin]
    return {}

def calcHeadersize(headers, manifest):
    newheaders = dict([(h,v) for (h,v) in headers.items() if (h,v) not in manifest.items()])
    txt = "\n".join(["%s: %s" % (h,v) for (h,v) in newheaders.items()])
    return len(txt)

def limitManifest(manifest):
    allowed = [
	"Strict-Transport-Security",
	"Expect-CT",
	"Referrer-Policy",
	"Content-Security-Policy",
	"CORS",
	"client-hints",
    ]

    allowed = [x.lower() for x in allowed]

    return dict([(h,v) for (h,v) in manifest.items() if h.lower() in allowed])

results = {}
total = {
    "count": 0,
    "rank": 0,
    "normal": 0,
    "manifest_limited": 0,
    "manifest_all": 0,
    "hnormal": 0,
    "hmanifest_limited": 0,
    "hmanifest_all": 0
}
alexaresults = {}

for req in reqs:
	origin = req["origin"]
	dom, rank = getDomainAndRank(origin)
	headers = dict(req["headers"])
	bodysize = getBodysize(headers)
	manifest = getManifest(origin)

	headersize_normal 		= calcHeadersize(headers, manifest={})
	headersize_manifest_limited 	= calcHeadersize(headers, manifest=limitManifest(manifest))
	headersize_manifest_all 	= calcHeadersize(headers, manifest=manifest)

	size_normal 		= bodysize + headersize_normal
	size_manifest_limited 	= bodysize + headersize_manifest_limited
	size_manifest_all 	= bodysize + headersize_manifest_all

	if origin not in results:
	    results[origin] = {
		"rank": 0,
	    	"count": 0,
	    	"normal": 0,
		"manifest_limited": 0,
		"manifest_all": 0,
	    	"hnormal": 0,
		"hmanifest_limited": 0,
		"hmanifest_all": 0
	    }

	results[origin]["count"] += 1
	results[origin]["normal"] += size_normal
	results[origin]["manifest_limited"] += size_manifest_limited
	results[origin]["manifest_all"] += size_manifest_all
	results[origin]["hnormal"] += headersize_normal
	results[origin]["hmanifest_limited"] += headersize_manifest_limited
	results[origin]["hmanifest_all"] += headersize_manifest_all

	if dom not in alexaresults:
	    alexaresults[dom] = {
	    	"rank": rank,
	    	"count": 0,
	    	"normal": 0,
		"manifest_limited": 0,
		"manifest_all": 0,
	    	"hnormal": 0,
		"hmanifest_limited": 0,
		"hmanifest_all": 0
	    }

	alexaresults[dom]["count"] += 1
	alexaresults[dom]["normal"] += size_normal
	alexaresults[dom]["manifest_limited"] += size_manifest_limited
	alexaresults[dom]["manifest_all"] += size_manifest_all
	alexaresults[dom]["hnormal"] += headersize_normal
	alexaresults[dom]["hmanifest_limited"] += headersize_manifest_limited
	alexaresults[dom]["hmanifest_all"] += headersize_manifest_all

	total["count"] += 1
	total["normal"] += size_normal
	total["manifest_limited"] += size_manifest_limited
	total["manifest_all"] += size_manifest_all
	total["hnormal"] += headersize_normal
	total["hmanifest_limited"] += headersize_manifest_limited
	total["hmanifest_all"] += headersize_manifest_all

	#print "%s\t%d\t%d\t%d" % (origin, size_normal, size_manifest_limited, size_manifest_all)

for (origin,v) in results.items():
#for (origin, v) in alexaresults.items():
    print "%s\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t%d\t%d\t%d\t%s\t%s" % (origin, v["rank"], v["count"], v["normal"], v["manifest_limited"], v["manifest_all"], p(v["manifest_limited"], v["normal"]), p(v["manifest_all"], v["normal"]), v["hnormal"], v["hmanifest_limited"], v["hmanifest_all"], p(v["hmanifest_limited"], v["hnormal"]), p(v["hmanifest_all"], v["hnormal"]))

print "%s\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t%d\t%d\t%d\t%s\t%s" % ("TOTAL", total["rank"], total["count"], total["normal"], total["manifest_limited"], total["manifest_all"], p(total["manifest_limited"], total["normal"]), p(total["manifest_all"], total["normal"]), total["hnormal"], total["hmanifest_limited"], total["hmanifest_all"], p(total["hmanifest_limited"], total["hnormal"]), p(total["hmanifest_all"], total["hnormal"]))

