#!/bin/bash

alexafile=/data/alexa/alexa-subset.csv

cd $(dirname $(readlink -f $0))

d=$(date +"%Y-%m-%d")
datadir="data/$d"
mkdir -p $datadir

docker run --rm -v $PWD:/data openwpm bash -c "cp /data/openwpm-script.py . && xvfb-run python openwpm-script.py $alexafile /data/$datadir"
