#!/bin/bash -e

inf=$1

alexatop=10000
visualcount=500
interactcount=50

totalcount=$(((visualcount+interactcount)*2))
tmpfile=$(mktemp)

head -$alexatop $inf | shuf > $tmpfile
head -$((visualcount*2)) $tmpfile > $tmpfile.v
tail -$((interactcount*2)) $tmpfile > $tmpfile.i

head -$visualcount $tmpfile.v > original-csv-daniel-$visualcount.csv
tail -$visualcount $tmpfile.v > original-csv-steven-$visualcount.csv
head -$interactcount $tmpfile.i > original-csv-daniel-$interactcount.csv
tail -$interactcount $tmpfile.i > original-csv-steven-$interactcount.csv

cut -d, -f2 original-csv-daniel-$visualcount.csv > domainlist-daniel-$visualcount.txt
cut -d, -f2 original-csv-steven-$visualcount.csv > domainlist-steven-$visualcount.txt
cat original-csv-daniel-$interactcount.csv | sed 's;,;\thttp://;' > spreadsheet-daniel-$interactcount.csv
cat original-csv-steven-$interactcount.csv | sed 's;,;\thttp://;' > spreadsheet-steven-$interactcount.csv

rm -f $tmpfile $tmpfile.i $tmpfile.v
