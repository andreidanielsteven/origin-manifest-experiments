import bz2, json, os, sys, time, itertools
from urllib.parse import urlparse
from ChangeRecord import ChangeRecord

BZ2_FILENAME = "responses.json.bz2"
 
DIFF_ADDED = "ADDED"
DIFF_REMOVED = "REMOVED"
DIFF_MODIFIED = "MODIFIED"


# see "--responses" execution mode
def responseStats(args):
  raw_data_dir = args[0]
  output_file = args[1]

  responses = []

  # get the list of sub-directories
  dirs = sorted(next(os.walk(raw_data_dir))[1])
  counter = 0
  total = len(dirs)
  for d in dirs:
    print("Processing... [{0}/{1}]".format(counter,total),end="\r")
    fname = "{0}/{1}/{2}".format(raw_data_dir, d, BZ2_FILENAME)
    if os.path.isfile(fname):
      # decompress and read-in the data as a JSON object
      responses.append(len(json.loads(bz2.open(fname).read().decode("utf-8"))))
  print()

  # write the URLs to file
  total = sum(responses)
  print("total: {0}".format(total))
  print("average per day: {0}".format(total / len(responses)))


# see "--urls" execution mode
def generateURLs(args):
  raw_data_dir = args[0]
  output_file = args[1]

  common_urls = None
  distinct_urls = set()

  # get the list of sub-directories
  for d in sorted(next(os.walk(raw_data_dir))[1]):
    fname = "{0}/{1}/{2}".format(raw_data_dir, d, BZ2_FILENAME)
    if os.path.isfile(fname):
      # decompress and read-in the data as a JSON object
      responses = json.loads(bz2.open(fname).read().decode("utf-8"))
      urls = set(res["url"] for res in responses)
      if common_urls == None:
        common_urls = urls
      else:
        common_urls = common_urls.intersection(urls)
      distinct_urls = distinct_urls.union(urls)
      print("{0}: common URLs: {1}, distinct URLs: {2}"
              .format(d, len(common_urls), len(distinct_urls)))
    else:
      print("NOT FOUND: {0}".format(fname))

  # write the URLs to file
  json.dump(
    {
      "common_urls": list(common_urls),
      "distinct_urls": list(distinct_urls),
    },
    open(output_file,'w')
  )


# see "--origins" execution mode
def generateOrigins(args):
  urls_lists = json.load(open(args[0], "r"))
  output_file = args[1]

  common_urls = dict((url, 1) for url in urls_lists["common_urls"])

  common_origins = dict()
  distinct_origins = dict()

  # common_urls is a subset of distinct_urls
  counter = 0
  total = len(urls_lists["distinct_urls"])
  for url_str in urls_lists["distinct_urls"]:
    counter += 1
    print("Processing... {0}% [{1}/{2}]".format(round((counter/total)*100,2), counter, total), end="\r")
    url = urlparse(url_str)
    port = url.port if url.port != None else { "http": 80, "https": 443 }[url.scheme]
    origin = "{0},{1},{2}".format(url.scheme,url.hostname,port)

    if url_str in common_urls:
      if origin in common_origins:
        common_origins[origin] += 1
      else:
        common_origins[origin] = 1
    if origin in distinct_origins:
      distinct_origins[origin] += 1
    else:
      distinct_origins[origin] = 1
  print()

  # write the URLs to file
  json.dump(
    {
      "common_origins": list(common_origins),
      "distinct_origins": list(distinct_origins),
    },
    open(output_file,'w')
  )
  print("common_origins: {0}".format(len(common_origins)))
  print("distinct_origins: {0}".format(len(distinct_origins)))


# see "--headers" execution mode
def generateHeaders(args):
  raw_data_dir = args[0]
  output_file = args[1]

  result = dict()
  responses_count = 0

  # get the list of sub-directories
  dirs = next(os.walk(raw_data_dir))[1]
  counter = 0
  total = len(dirs)
  for d in dirs:
    fname = "{0}/{1}/{2}".format(raw_data_dir, d, BZ2_FILENAME)
    counter += 1
    print("Processing... {0}% [{1}/{2}]".format(round((counter/total)*100,2), counter, total), end="\r")
    if os.path.isfile(fname):
      # decompress and read-in the data as a JSON object
      responses = json.loads(bz2.open(fname).read().decode("utf-8"))
      responses_count += len(responses)
      # iterate over all header lists of form [key, value] in all "headers" fields of all responses
      for header in itertools.chain.from_iterable(res["headers"] for res in responses):
        header_name = header[0].lower()
        header_value = header[1]
        if header_name in result:
          result[header_name]["count"] += 1
          result[header_name]["size"] += len(header_name)+len(header_value)
        else:
          result[header_name] = {
            "count": 1,
            "size": len(header_value)
          }
  print()

  # compute frequency
  for header in result:
    result[header]["frequency"] = (result[header]["count"]/responses_count)*100
    result[header]["average_size"] = result[header]["size"]/result[header]["count"]

  # write the URLs to file
  json.dump(
    result,
    open(output_file,'w')
  )


# see "--headerdiffs" execution mode
def generateHeaderDiffs(args):
  raw_data_dir = args[0]
  urls_file = args[1]
  output_file = args[2]

  # result->day->url->header->DIFF_X
  result = dict()
  common_urls = set(json.load(open(urls_file,"r"))["common_urls"])
  current = None
  previous = None

  # get the list of sub-directories
  dirs = sorted(next(os.walk(raw_data_dir))[1])
  counter = 0
  total = len(dirs)
  for d in dirs:
    fname = "{0}/{1}/{2}".format(raw_data_dir, d, BZ2_FILENAME)
    counter += 1
    print("{0}: Processing... {1}% [{2}/{3}]".format(d,round((counter/total)*100,2), counter, total), end="\r")
    if not os.path.isfile(fname):
      print("ERROR: file {0} is missing! Exiting now.".format(fname))
      sys.exit()
    # decompress and read-in the data as a JSON object
    responses = json.loads(bz2.open(fname).read().decode("utf-8"))
    previous = current
    current = dict()
    for res in [res for res in responses if res["url"] in common_urls]:
      url = res["url"]
      current[url] = { "headers": dict(), "changes": dict() }
      for header in res["headers"]:
        # make sure we are case in-sensitive
        current[url]["headers"][header[0].lower()] = header[1]

      if previous == None:
        continue

      shared_headers = set()
      # identify added headers
      for h in current[url]["headers"]:
        if h in previous[url]["headers"]:
          shared_headers.add(h)
        else:
          current[url]["changes"][h] = DIFF_ADDED
      # identify removed headers
      for h in previous[url]["headers"]:
        if h not in shared_headers:
          current[url]["changes"][h] = DIFF_REMOVED
      # identify modified headers
      for h in shared_headers:
        if current[url]["headers"][h] != previous[url]["headers"][h]:
          current[url]["changes"][h] = DIFF_MODIFIED

    # we only need to remember the diffs
    result[d] = dict((url,current[url]["changes"]) for url in current)

  # write the result to file
  json.dump(result,open(output_file,"w"))


# see "--changerecords" execution mode
def generateChangeRecords(args):
  raw_data_dir = args[0]
  urls_file = args[1]
  origins_file = args[2]
  output_dir = args[3]
  cutoff_start = int(args[4])
  cutoff_step = int(args[5])
  cutoff_max = int(args[6])

  previous_record = None
  current_record = None

  common_urls = json.load(open(urls_file, "r"))["common_urls"]
  origins = json.load(open(origins_file, "r"))["common_origins"]

  # get the list of sub-directories
  days = next(os.walk(raw_data_dir))[1]
  days.sort()
  for cutoff in range(cutoff_start, cutoff_max+cutoff_step, cutoff_step):
    counter = 0
    total = len(days)
    for day in days:
      counter += 1
      print("Processing cutoff {0}, day {1}... {2}% [{3}/{4}]"
               .format(cutoff,day,round((counter/total)*100,2), counter, total), end="\r")
      response_bz2 = "{0}/{1}/{2}".format(raw_data_dir, day, BZ2_FILENAME)
      if os.path.isfile(response_bz2):
        previous_record = current_record
        current_record = ChangeRecord.createChangeRecord(day, response_bz2, common_urls,
                                                         origins, cutoff, previous_record)
        path = "{0}/{1}".format(output_dir, cutoff)
        os.makedirs(path, exist_ok=True)
        current_record.writeToFile("{0}/{1}.json".format(path, day))
      else:
        print("NOT FOUND: {0}".format(response_bz2))
    print()


def generateOriginManifestHeaders(args):
  originmanifest_dir = args[0]
  output_file = args[1]

  # result->header->{ "count": 42, "size": 23 }
  result = dict()

  def getCORSpreflightListSize(preflight_list):
    result = 0
    for preflight in preflight_list:
      for header in preflight:
        result += len(header)+len(preflight[header])
    return result

  # collect data
  om_dir_info = next(os.walk(originmanifest_dir))
  num_origins = 0
  counter = 0
  total = len(om_dir_info[2])
  for jsonfile in om_dir_info[2]:
    counter += 1
    print("Processing... {0}% [{1}/{2}]".format(round((counter/total)*100,2), counter, total), end="\r")
    originmanifests = json.load(open("{0}/{1}".format(om_dir_info[0],jsonfile),"r"))["origin_manifests"]
    num_origins = len(originmanifests)
    for origin in originmanifests:
      for section in [s for s in originmanifests[origin] if not s.endswith("_comments")]:
        section_content = originmanifests[origin][section]
        # dict -> headers section
        if isinstance(section_content, dict):
          if section not in result:
            result[section] = dict()
          for header in section_content:
            header_name = header.lower()
            if header_name in result[section]:
              result[section][header_name]["count"] += 1
              result[section][header_name]["size"] += len(header_name) + len(section_content[header])
            else:
              result[section][header_name] = {
                "count": 1,
                "size": len(section_content[header]),
              }

        # list -> CORS preflight section
        if isinstance(section_content, list):
          if section in result:
            result[section]["count"] += len(section_content)
            result[section]["size"] += getCORSpreflightListSize(section_content)
          else:
            result[section] = {
              "count": len(section_content),
              "size": getCORSpreflightListSize(section_content),
            }
  print()
  
  
  # compute average size and popularity in %
  total = len(om_dir_info[2]) * num_origins
  for section in result:
    # CORS preflight sections
    if "count" in result[section] and "size" in result[section]:
      result[section]["frequency"] = (result[section]["count"] / total) if total > 0 else 0
      result[section]["average_size"] = (result[section]["size"] / result[section]["count"]) \
                                           if result[section]["count"] > 0 else 0
    else:
      for header in result[section]:
        result[section][header]["frequency"] = (result[section][header]["count"] / total) * 100
        result[section][header]["average_size"] = result[section][header]["size"] / result[section][header]["count"]

  #print(result)
  # write result to file
  json.dump(result, open(output_file,"w"))


def main():
  # check running mode
  modes = {
    "--responses": { "usage": ["raw data dir", "output file"],
                     "description": "Compute some statistics about the responses "
                                    "in general, e.g. average responses per day",
                     "call": responseStats },
    "--urls": { "usage": ["raw data dir", "output file"],
                "description": "Computes the set of different URLs and the set of common URLs",
                "call": generateURLs },
    "--origins": { "usage": ["urls list", "output file"],
                   "description": "Computes the set of different origins and the set of common origins."
                                  " The URLs list must be generated with the --urls option",
                   "call": generateOrigins },
    "--headers": { "usage": ["raw data dir", "output file"],
                   "description": "Computes the average size in bytes and frequency of "
                                  "headers in % relative to the number of responsens",
                   "call": generateHeaders },
    "--headerdiffs": { "usage": ["raw data dir", "urls list", "output file"],
                       "description": "Computes changes of headers for the same URL over all days.",
                       "call": generateHeaderDiffs },
    "--changerecords": { "usage": ["raw data dir", "urls list", "origins list", "output dir",
                                   "cutoff start", "cutoff step", "cutoff end"],
                         "description": "Computes all Origin Manifests and their changes for each day",
                         "call": generateChangeRecords },
    "--om-headers": { "usage": ["change records subdir", "output file"],
                      "description": "Computes the average size in bytes and frequency of "
                                     "Origin Manifest headers in % relative to the presence of a "
                                     "header in an Origin Manifest over all origins and days",
                      "call": generateOriginManifestHeaders },
  }

  if len(sys.argv) < 2 or sys.argv[1] not in modes:
    print("Usage (pick one of the following options):")
    for mode in modes:
      print("\t{0} {1}".format(mode," ".join("<{0}>".format(arg) for arg in modes[mode]["usage"])))
      print("\t\t{0}".format(modes[mode]["description"]))
    return

  mode = modes[sys.argv[1]]
  start = time.time()
  mode["call"](sys.argv[2:len(mode["usage"])+2])
  print("Finished after {0}".format(time.time() - start))


if __name__ == '__main__':
    main()
