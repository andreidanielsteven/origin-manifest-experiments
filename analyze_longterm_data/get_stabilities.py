import bz2, json, os, sys, time
from datetime import datetime
from urllib.parse import urlparse

BZ2_FILENAME = "responses.json.bz2"
DATE_FORMAT = "%Y-%m-%d"


def __getOrigin(url_str):
  url = urlparse(url_str)
  port = url.port if url.port != None else { "http": 80, "https": 443 }[url.scheme]
  return "{0},{1},{2}".format(url.scheme,url.hostname,port)


def __headerStability(headerdiffs_file, raw_data_dir, urls_file):
  # headerdiffs->day->url->header->DIFF_X
  headerdiffs = json.load(open(headerdiffs_file, "r"))
  days = sorted(day for day in headerdiffs)

  # result->header->url->{ <some stats> }
  result = dict()

  # by adding it we record that the URL started with a specific header
  response_bz2 = "{0}/{1}/{2}".format(raw_data_dir,days[0],BZ2_FILENAME)
  responses = json.loads(bz2.open(response_bz2).read().decode("utf-8"))
  common_urls = set(json.load(open(urls_file,"r"))["common_urls"])
  counter = 0
  for res in [res for res in responses if res["url"] in common_urls]:
    counter += 1
    for header in res["headers"]:
      header_name = header[0].lower()
      if header_name not in result:
        result[header_name] = dict()
      result[header_name][res["url"]] = {
        "latest": datetime.strptime(days[0], DATE_FORMAT),
        "streak_history": [],
      }

  # aggregate header changes per URL
  counter = 0
  total = len(days)
  print("-> Aggregate header changes per URL")
  for day in days:
    counter += 1
    print("Processing... {0}% [{1}/{2}]".format(round((counter/total)*100,2), counter, total), end="\r")
    current_date = datetime.strptime(day, DATE_FORMAT)
    for url in headerdiffs[day]:
      for header_name in headerdiffs[day][url]:
        header_name = header_name.lower()
        if header_name not in result:
          result[header_name] = dict()
        if url not in result[header_name]:
          result[header_name][url] = {
            "latest": datetime.strptime(days[0], DATE_FORMAT),
            "streak_history": [],
          }
        renewal = result[header_name][url]
        days_alive = (current_date - renewal["latest"]).days
        renewal["latest"] = current_date
        renewal["streak_history"].append(days_alive)
  print()

  return result,day


def headerStability(args):
  headerdiffs_file = args[0]
  raw_data_dir = args[1].rstrip("/")
  urls_file = args[2]
  output_file = args[3]

  result,last_day = __headerStability(headerdiffs_file, raw_data_dir, urls_file)

  # reduce data per header
  counter = 0
  total = len(result)
  print("-> Reduce data per header")
  for h in result:
    counter += 1
    print("Processing... {0}% [{1}/{2}]".format(round((counter/total)*100,2), counter, total), end="\r")
    stats = {
      "streak_history": [],
      "url_counter": 0,
    }
    for url in result[h]:
      renewal = result[h][url]
      # first update the origin's header stats...
      current_date = datetime.strptime(last_day, DATE_FORMAT)
      days_alive = (current_date - renewal["latest"]).days
      if days_alive > 0:
        renewal["streak_history"].append(days_alive)
      # ...then merge it with the overall header stats
      stats["streak_history"].extend(renewal["streak_history"])
      stats["url_counter"] += 1
    result[h] = stats
  print()

  # we have all histories, time to calculate the average streaks
  counter = 0
  total = len(result)
  print("-> Get average streaks")
  for h in result:
    counter += 1
    print("Processing... {0}% [{1}/{2}]".format(round((counter/total)*100,2), counter, total), end="\r")
    stats = result[h]
    stats["average_streak"] = sum(stats["streak_history"]) / len(stats["streak_history"])
    del stats["streak_history"]
  print()

  # write result to file
  json.dump(result,open(output_file,"w"))


def headerStabilityPerURL(args):
  # { "Cache-Control": {"count": 99, "size": 234, "frequency": 80.77, "average_size": 23.54}, ... }
  #headers_file = args[0]
  headerdiffs_file = args[0]
  raw_data_dir = args[1].rstrip("/")
  urls_file = args[2]
  output_file = args[3]

  result,last_day = __headerStability(headerdiffs_file, raw_data_dir, urls_file)

  # we have all histories, time to calculate the average streaks
  counter = 0
  total = len(result)
  print("-> Get average streaks")
  for h in result:
    counter += 1
    print("Processing... {0}% [{1}/{2}]".format(round((counter/total)*100,2), counter, total), end="\r")
    for url in result[h]:
      stats = result[h][url]
      current_date = datetime.strptime(last_day, DATE_FORMAT)
      days_alive = (current_date - stats["latest"]).days
      if days_alive > 0:
        stats["streak_history"].append(days_alive)
      result[h][url] = {
        "average_streak": sum(stats["streak_history"]) / len(stats["streak_history"]),
      }
      
  print()

  # write result to file
  json.dump(result,open(output_file,"w"))


def headerStabilityPerOrigin(args):
  # { "Cache-Control": {"count": 99, "size": 234, "frequency": 80.77, "average_size": 23.54}, ... }
  #headers_file = args[0]
  headerdiffs_file = args[0]
  raw_data_dir = args[1].rstrip("/")
  urls_file = args[2]
  output_file = args[3]

  result,last_day = __headerStability(headerdiffs_file, raw_data_dir, urls_file)

  # reduce data per header to origins
  counter = 0
  total = len(result)
  print("-> Reduce data per header")
  for h in result:
    counter += 1
    print("Processing... {0}% [{1}/{2}]".format(round((counter/total)*100,2), counter, total), end="\r")
    stats = {
      "streak_history": [],
      "url_counter": 0,
    }
    origins = dict()
    for url in result[h]:
      renewal = result[h][url]
      # first update the origin's header stats...
      current_date = datetime.strptime(last_day, DATE_FORMAT)
      days_alive = (current_date - renewal["latest"]).days
      if days_alive > 0:
        renewal["streak_history"].append(days_alive)
      # ...then merge it with the overall origin stats
      origin = __getOrigin(url)
      if origin not in origins:
        origins[origin] = {
          "streak_history": [],
          "url_counter": 0,
        }
      origins[origin]["streak_history"].extend(renewal["streak_history"])
      origins[origin]["url_counter"] += 1
    result[h] = origins
  print()

  # we have all histories, time to calculate the average streaks
  counter = 0
  total = len(result)
  print("-> Get average streaks")
  for h in result:
    counter += 1
    print("Processing... {0}% [{1}/{2}]".format(round((counter/total)*100,2), counter, total), end="\r")
    for origin in result[h]:
      stats = result[h][origin]
      stats["average_streak"] = sum(stats["streak_history"]) / len(stats["streak_history"])
      stats["average_changes"] = len(stats["streak_history"]) / stats["url_counter"]
      del stats["streak_history"]
  print()

  # write result to file
  json.dump(result,open(output_file,"w"))


def originManifestHeaderStability(args):
  om_headers_file = args[0]
  changerecords_dir = args[1]
  output_file = args[2]

  # result->section->header->origin->{ <some stats> }
  result = dict()

  # {"fallback": {"server": {"count": 129, "size": 14, "frequency": 0.86, "average_size": 10.83}, ... }, ... } 
  om_headers = json.load(open(om_headers_file,"r"))

  for section in om_headers:
    # we don't handle CORS preflight sections quite yet
    if "count" in om_headers[section] and "size" in om_headers[section]:
      continue
    result[section] = dict((header.lower(),dict()) for header in om_headers[section])
     
  changerecords_files = next(os.walk(changerecords_dir))
  first_day = None
  last_day = None
  for json_file in sorted(changerecords_files[2]):
    changes = json.load(open("{0}/{1}".format(changerecords_files[0],json_file), "r"))
    day = changes["day"]
    last_day = changes["day"]
    if first_day == None:
      first_day = day
      # we add the origin that we know the header was set but potentially never changed
      for origin in changes["origin_manifests"]:
        for section in changes["origin_manifests"][origin]:
          # we don't handle CORS preflight sections quite yet
          if isinstance(changes["origin_manifests"][origin][section],list) \
              or section.endswith("_comments"):
            continue
          for header in changes["origin_manifests"][origin][section]:
            header_name = header.lower()
            result[section][header_name][origin] = {
              "latest": datetime.strptime(first_day, DATE_FORMAT),
              "streak_history": [],
            }
      continue
    changes = changes["changes"]
    for origin in changes:
      change = changes[origin]
      for section in change:
        # we don't handle CORS preflight sections quite yet
        if section not in result:
          continue
        for header in change[section]:
          header_name = header.lower()
          if origin not in result[section][header_name]:
            result[section][header_name][origin] = {
              "latest": datetime.strptime(first_day, DATE_FORMAT),
              "streak_history": [],
            }
          renewal = result[section][header_name][origin]
          current_date = datetime.strptime(day, DATE_FORMAT)
          days_alive = (current_date - renewal["latest"]).days
          renewal["latest"] = current_date
          renewal["streak_history"].append(days_alive)

  # reduce collected data
  for section in result:
    for header in result[section]:
      stats = {
        "streak_history": [],
        "origins_count": 0
      }
      for origin in result[section][header]:
        renewal = result[section][header][origin]
        # first update the origin's header stats...
        current_date = datetime.strptime(last_day, DATE_FORMAT)
        days_alive = (current_date - renewal["latest"]).days
        if days_alive > 0:
          renewal["streak_history"].append(days_alive)
        # ...then merge it with the overall header stats
        stats["streak_history"].extend(renewal["streak_history"])
        stats["origins_count"] += 1
      result[section][header] = stats 

  for section in list(result):
    for header in list(result[section]):
      stats = result[section][header]
      if len(stats["streak_history"]) > 0:
        stats["average_streak"] = sum(stats["streak_history"]) / len(stats["streak_history"])
      else:
        #print(section, header, stats["streak_history"], stats["origins_count"])
        if stats["origins_count"] == 0:
          del result[section][header]
          continue
      del stats["streak_history"]

  # write result to output file
  json.dump(result, open(output_file, "w"))


def originManifestStability(args):
  changerecords_dir = args[0]
  included_headers_file = args[1]
  output_file = args[2]

  # result->origin->{ <some stats> }
  result = dict()
  included_headers = json.load(open(included_headers_file,"r"))

  changerecords_files = next(os.walk(changerecords_dir))
  first_day = None
  last_day = None
  for changerecords_file in sorted(f for f in changerecords_files[2] if f.endswith("json")):
    changes = json.load(open("{0}/{1}".format(changerecords_files[0],changerecords_file),"r"))
    manifests = changes["origin_manifests"]
    if first_day == None:
      first_day = changes["day"]
      for origin in manifests:
        manifest = manifests[origin]
        # we are only interested in origins with a manifest that has wanted headers
        sub_manifest = dict()
        is_interesting = False
        for section in included_headers:
          if len(included_headers[section]) == 0 or section not in manifest:
            continue
          section_headers = dict((h.lower(),manifest[section][h]) for h in manifest[section] \
                                    if h.lower() in included_headers[section])
          if len(section_headers) > 0:
            sub_manifest[section] = section_headers
            is_interesting = True
        if not is_interesting:
          continue
        result[origin] = {
          "latest": datetime.strptime(first_day, DATE_FORMAT),
          "streak_history": [],
          "sizes": [len(json.dumps(manifest))],
          "manifest": sub_manifest,
        }
      continue
    day = changes["day"]
    last_day = changes["day"]
    changes = changes["changes"]
    for origin in changes:
      # record a change iff it is a header we are concerned about
      has_interesting_change = False
      change = changes[origin]
      for section in change:
        if section not in included_headers:
          continue
        for header in change[section]:
          header_name = header.lower()
          if header_name in included_headers[section]:
            has_interesting_change = True
            break
        if has_interesting_change:
          break
      if not has_interesting_change:
        continue
      if origin not in result:
        result[origin] = {
          "latest": datetime.strptime(first_day, DATE_FORMAT),
          "streak_history": [],
          "sizes": [],
        }
      renewal = result[origin]
      current_date = datetime.strptime(day, DATE_FORMAT)
      days_alive = (current_date - renewal["latest"]).days
      renewal["latest"] = current_date
      renewal["streak_history"].append(days_alive)
      # compute the size of only the interesting parts
      manifest = manifests[origin]
      sub_manifest = dict()
      for section in included_headers:
        if len(included_headers[section]) == 0 or section not in manifest:
          continue
        section_headers = dict((h.lower(),manifest[section][h]) for h in manifest[section] \
                                  if h.lower() in included_headers[section])
        if len(section_headers) > 0:
          sub_manifest[section] = section_headers
      renewal["sizes"].append(len(json.dumps(sub_manifest)))
  #print(result)

  stats = {
    "streak_history": [],
    "origins_count": 0,
    "sizes": [],
    "no_changes": dict(),
  }
  for origin in result:
    renewal = result[origin]
    # first update the origin's header stats...
    current_date = datetime.strptime(last_day, DATE_FORMAT)
    days_alive = (current_date - renewal["latest"]).days
    if days_alive > 0:
      renewal["streak_history"].append(days_alive)
    # ...then merge it with the overall header stats
    stats["streak_history"].extend(renewal["streak_history"])
    stats["origins_count"] += 1
    stats["sizes"].extend(renewal["sizes"])
    if len(renewal["streak_history"]) == 1 and "manifest" in renewal:
      # renewal["manifest"] is unique to never changing manifests
      # if the first interesting change was recorded on the last day, we have |streak_history| == 1
      stats["no_changes"][origin] = renewal["manifest"]

  stats["average_streak"] = sum(stats["streak_history"]) / len(stats["streak_history"])
  del stats["streak_history"]
  stats["average_size"] = sum(stats["sizes"]) / len(stats["sizes"])
  del stats["sizes"]

  # write result to output file
  json.dump(stats, open(output_file, "w"))


def urlsOrigins(args):
  urls_file = args[0]
  origins_file = args[1]
  headers_file = args[2]
  om_stability_file = args[3]
  output_file = args[4]

  urls = json.load(open(urls_file,"r"))
  origins = json.load(open(origins_file,"r"))
  headers = json.load(open(headers_file,"r"))
  om_stability = json.load(open(om_stability_file,"r"))

  result = {
    "urls": {
      "common": len(urls["common_urls"]),
      "distinct": len(urls["distinct_urls"]),
    },
    "origins": {
      "common": len(origins["common_origins"]),
      "distinct": len(origins["distinct_origins"]),
    },
    "origin_manifests": {
      "origins_count": om_stability["origins_count"],
      "no_changes": len(om_stability["no_changes"]),
      "average_size": round(om_stability["average_size"],2),
      "average_streak": round(om_stability["average_streak"],2),
    },
    "headers": {
      "unique": len(set(h.lower() for h in headers))
    },
  }
  print(json.dumps(result,indent=2))
  json.dump(result, open(output_file, "w"), indent=2)


def main():
  # check running mode
  modes = {
    "--headers": { "usage": ["headers diff file", "raw data dir", "urls file", "output file"],
                   "description": "Print the stability of headers for each URL to file. That "
                                  "is TTL, change history, etc",
                   "call": headerStability },
    "--headers-per-url": { "usage": ["headers diff file", "raw data dir", "urls file", "output file"],
                           "description": "Print the stability of headers for each URL to file. That "
                                          "is TTL, change history, etc",
                           "call": headerStabilityPerURL },
    "--headers-per-origin": { "usage": ["headers diff file", "raw data dir", "urls file", "output file"],
                             "description": "Print the stability of headers for each origin to file. "
                                            "That is TTL, change history, etc",
                           "call": headerStabilityPerOrigin },
    "--om-headers": { "usage": ["Origin Manifests headers file", "change records subdir", "output file"],
                      "description": "Print the stability of headers to file. That is TTL, "
                                     "change history, etc",
                      "call": originManifestHeaderStability },
    "--om": { "usage": ["change records subdir", "list of included headers", "output file"],
              "description": "Print the stability of headers to file. That is TTL, "
                                     "change history, etc",
              "call": originManifestStability },
    "--numbers": { "usage": ["urls file", "origins file", "headers", "Origin Manifest stability file", "output file"],
                   "description": "Print the total number of distinct and common URLs/origins",
                   "call": urlsOrigins },
  }

  if len(sys.argv) < 2 or sys.argv[1] not in modes:
    print("Usage (pick one of the following options):")
    for mode in modes:
      print("\t{0} {1}".format(mode," ".join("<{0}>".format(arg) for arg in modes[mode]["usage"])))
      print("\t\t{0}".format(modes[mode]["description"]))
    return

  mode = modes[sys.argv[1]]
  start = time.time()
  mode["call"](sys.argv[2:len(mode["usage"])+2])
  print("Finished after {0}".format(time.time() - start))


if __name__ == '__main__':
    main()
