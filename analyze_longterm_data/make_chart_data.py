import json, math, sys, time
from get_top_headers import mostFrequentHeaders, biggestHeaders, mostFrequentOriginManifestHeaders
from collections import OrderedDict


SECURITY_HEADERS = [
  "set-cookie",
  "content-security-policy",
  "x-xss-protection",
  "timing-allow-origin",
  "strict-transport-security",
  "public-key-pins",
  "x-frame-options",
]


def mostFrequentHeadersStability(args):
  header_stability_file = args[0]
  headers_stats_file = args[1]
  top = args[2]
  output_file = args[3]

  # { "header": "X", "count", 42, "frequency": 0.23, ...}
  top_headers = mostFrequentHeaders([headers_stats_file,top],False)
  # transform into ["x", "y", ...]
  top_headers = list(h["header"].lower() for h in top_headers)
  # add security headers
  top_headers.extend(h.lower() for h in SECURITY_HEADERS)

  # { "X": {"longest_streak": 97, "shortest_streak": 0, "average_streak": 6.49 }, ...}
  headers = json.load(open(header_stability_file,"r"))
  # filter for the headers we want, make header names lowercase
  headers = dict((h.lower(),headers[h]) for h in headers if h.lower() in top_headers)

  json_data = [["Headers","Average header stability in days"]]
  json_data.extend([header,headers[header]["average_streak"]] for header in headers)

  # write result to file
  json.dump({
      "title": "Header stability",
      "subtitle": "TODO",
      "json_data": json_data,
      "info": top_headers,
    },
    open(output_file,"w")
  )


def biggestHeadersStability(args):
  header_stability_file = args[0]
  headers_stats_file = args[1]
  top = args[2]
  output_file = args[3]

  # { "header": "X", "count", 42, "frequency": 0.23, ...}
  top_headers = biggestHeaders([headers_stats_file,top],False)
  # transform into ["x", "y", ...]
  top_headers = list(h["header"].lower() for h in top_headers)
  # add security headers
  top_headers.extend(h.lower() for h in SECURITY_HEADERS)

  # { "X": {"longest_streak": 97, "shortest_streak": 0, "average_streak": 6.49 }, ...}
  headers = json.load(open(header_stability_file,"r"))
  # filter for the headers we want, make header names lowercase
  headers = dict((h.lower(),headers[h]) for h in headers if h.lower() in top_headers)

  json_data = [["Headers","Average header stability in days"]]
  json_data.extend([header,headers[header]["average_streak"]] for header in headers)

  # write result to file
  json.dump({
      "title": "Header stability",
      "subtitle": "TODO",
      "json_data": json_data,
      "info": str(top_headers),
    },
    open(output_file,"w")
  )


def headerStabilityPerOrigin(args):
  #            1    2    3    4  ...
  # header_x   3%  10%  27%  13% ...
  headers_file = args[0]
  header_stability_file = args[1]
  top = int(args[2])
  additional_headers_file = args[3]
  header_stability_per_origin_file = args[4]
  output_file = args[5]

  days = list(range(1,101,1))
  # { "header": "X", "count", 42, "frequency": 0.23, ...}
  top_headers = mostFrequentHeaders([headers_file,header_stability_file,top,additional_headers_file],False)
  # transform into ["x", "y", ...]
  top_headers = list(OrderedDict.fromkeys(h["header"].lower() for h in top_headers))

  # stability->header->origin->{"url_counter": 1, "average_streak": 33.33}
  stability = json.load(open(header_stability_per_origin_file,"r"))
  
  # result->header->day->"number of origins with average header stability of ~d"
  result = dict((h, dict((d,0) for d in days)) for h in top_headers)

  for header in top_headers:
    for origin in stability[header]:
      avg_streak = round(stability[header][origin]["average_streak"])
      result[header][avg_streak] += 1

  output_str = "header name,{0}\n".format(",".join(str(d) for d in days))
  for header in result:
    output_str += "{0},{1}\n".format(header,",".join(str(result[header][d]) for d in days))

  # write to file
  with open(output_file,"w") as f:
    f.write(output_str)


def headerStability(args):
  headers_file = args[0]
  header_stability_file = args[1]
  top = int(args[2])
  additional_headers_file = args[3]
  output_file = args[4]

  # [{ "header": "X", "count", 42, "frequency": 0.23, ...}, ...]
  top_headers = mostFrequentHeaders([headers_file,header_stability_file,top,additional_headers_file],False)
  # transform into ["x", "y", ...]
  top_header_names = list(OrderedDict.fromkeys(h["header"].lower() for h in top_headers))

  output_str = "header name,{0}\n".format(",".join(top_header_names))
  output_str += "days alive,{0}\n".format(",".join(next(str(th["average_streak"]) for th in top_headers if th["header"] == h) for h in top_header_names))

  # write to file
  with open(output_file,"w") as f:
    f.write(output_str)


def originManifestHeaderStability(args):
  headers_file = args[0]
  header_stability_file = args[1]
  top = int(args[2])
  additional_headers_file = args[3]
  output_file = args[4]

  # { "fallback": [{ "header": "X", "count", 42, "frequency": 0.23, ...}, ...], .. }
  top_headers = mostFrequentOriginManifestHeaders([headers_file,header_stability_file,top,additional_headers_file],False)
  output_str = ""
  for section in top_headers:
    if isinstance(top_headers[section],dict):
      continue
    # transform into ["x", "y", ...]
    top_header_names = list(OrderedDict.fromkeys(h["header"].lower() for h in top_headers[section]))

    output_str += "header name,{0}\n".format(",".join(top_header_names))
    output_str += "days alive,{0}\n\n".format(",".join(next(str(th["average_streak"]) for th in top_headers[section] if th["header"] == h) for h in top_header_names))

  # write to file
  with open(output_file,"w") as f:
    f.write(output_str)


def main():
  # check running mode
  modes = {
    "--most-frequent-headers-stability":
        { "usage": ["header stability file", "headers stats file", "top x", "output file"],
          "description": "Create most frequent headers stability data for Google Chart",
          "call": mostFrequentHeadersStability },
    "--biggest-headers-stability":
        { "usage": ["header stability file", "headers stats file", "top x", "output file"],
          "description": "Create biggest headers stability data for Google Chart",
          "call": biggestHeadersStability },
    "--header-stability-per-origin":
        { "usage": ["headers stats file", "headers stability file", "top x", 
                    "additional headers file", "headers stability per origin file", "output file"],
          "description": "Create a basic text file for import in Google Sheets",
          "call": headerStabilityPerOrigin },
    "--header-stability":
        { "usage": ["headers stats file", "headers stability file", "top x", 
                    "additional headers file", "headers stability per origin file", "output file"],
          "description": "Create a basic text file for import in Google Sheets",
          "call": headerStability },
    "--om-header-stability":
        { "usage": ["Origin Manifest headers stats file", "Origin Manifest headers stability file",
                                  "top x", "additional headers file", "headers stability per origin file", "output file"],
          "description": "Create a basic text file for import in Google Sheets",
          "call": originManifestHeaderStability },
  }

  if len(sys.argv) < 2 or sys.argv[1] not in modes:
    print("Usage (pick one of the following options):")
    for mode in modes:
      print("\t{0} {1}".format(mode," ".join("<{0}>".format(arg) for arg in modes[mode]["usage"])))
      print("\t\t{0}".format(modes[mode]["description"]))
    return

  mode = modes[sys.argv[1]]
  start = time.time()
  mode["call"](sys.argv[2:len(mode["usage"])+2])
  print("Finished after {0}".format(time.time() - start))


if __name__ == '__main__':
    main()
