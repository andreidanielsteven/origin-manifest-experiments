import json, sys, time


def mostFrequentHeaders(args, print_to_console=True):
  headers_file = args[0]
  header_stability_file = args[1]
  top = int(args[2])
  additional_headers_file = args[3]

  header_dict = json.load(open(headers_file, "r"))
  header_stability_dict = json.load(open(header_stability_file, "r"))
  header_list = list({
      "header": header,
      "count": header_dict[header]["count"],
      "frequency": header_dict[header]["frequency"],
      "size": header_dict[header]["size"],
      "average_size": header_dict[header]["average_size"],
    } for header in header_dict)
  header_list = sorted(header_list, key=lambda x: x["frequency"], reverse=True)
  for index,stat in enumerate(header_list):
    stat["rank"] = index+1
    if stat["header"] in header_stability_dict:
      stability = header_stability_dict[stat["header"]]
      for s in stability:
        stat[s] = stability[s]
  additional_headers = list(h for h in header_list if h["header"] in json.load(open(additional_headers_file,"r")))
  header_list = header_list[:top]
  header_list.extend(additional_headers)
  if print_to_console:
    #print(json.dumps(header_list, indent=2))
    columns = ["rank","header","frequency","average_size","average_streak"]
    latex_output_str = "{0} \\\\\n".format(" & ".join(columns))
    csv_output_str = "{0}\n".format(",".join(columns))
    for h in header_list:
      rank = str(h["rank"])
      header = h["header"]
      frequency = str(round(float(h["frequency"]),2))
      average_size = str(round(float(h["average_size"]),2))
      average_streak = str(round(float(h["average_streak"]),2))
      latex_output_str += "{0} \\\\\n".format(" & ".join([rank,header,frequency,average_size,average_streak]))
      csv_output_str += "{0}\n".format(",".join([rank,header,frequency,average_size,average_streak]))
    print(latex_output_str)
    print()
    print(csv_output_str)
  else:
    return header_list


def biggestHeaders(args, print_to_console=True):
  headers_file = args[0]
  top = int(args[1])
  additional_headers_file = args[2]

  header_dict = json.load(open(headers_file, "r"))
  header_list = list({
      "header": header,
      "count": header_dict[header]["count"],
      "frequency": header_dict[header]["frequency"],
      "size": header_dict[header]["size"],
      "average_size": header_dict[header]["average_size"],
    } for header in header_dict)
  additional_headers = list(h for h in header_list if h["header"] in json.load(open(additional_headers_file,"r")))
  header_list = sorted(header_list, key=lambda x: x["average_size"], reverse=True)[:top]
  header_list.extend(additional_headers)
  if print_to_console:
    print(json.dumps(header_list, indent=2))
  else:
    return header_list


def mostStableHeaders(args, print_to_console=True):
  header_stability_file = args[0]
  top = int(args[1])
  additional_headers_file = args[2]

  header_stability_dict = json.load(open(header_stability_file, "r"))
  header_stability_list = list({
      "header": header,
      "url_counter": header_stability_dict[header]["url_counter"],
      "average_streak": header_stability_dict[header]["average_streak"],
    } for header in header_stability_dict)
  additional_headers = list(h for h in header_stability_list if h["header"] in json.load(open(additional_headers_file,"r")))
  header_stability_list = sorted(header_stability_list, key=lambda x: x["average_streak"], reverse=True)[:top]
  header_stability_list.extend(additional_headers)
  if print_to_console:
    print(json.dumps(header_stability_list, indent=2))
  else:
    return header_stability_list


def mostFrequentOriginManifestHeaders(args, print_to_console=True):
  pseudo_cors_headers = [
    #"x-pseudo-cors-consolidation-header",
    #"x-pseudo-cors-preflight-consolidation-header",
  ]

  om_headers_file = args[0]
  om_header_stability_file = args[1]
  top = int(args[2])
  additional_headers_file = args[3]

  result = dict()
  additional_headers_dict = json.load(open(additional_headers_file,"r"))

  om_headers_dict = json.load(open(om_headers_file,"r"))
  om_header_stability_dict = json.load(open(om_header_stability_file,"r"))
  for section in list(om_headers_dict):
    if "count" in om_headers_dict[section] and "size" in om_headers_dict[section]:
      continue
    headers_list = list({
        "header": header,
        "count": om_headers_dict[section][header]["count"],
        "frequency": om_headers_dict[section][header]["frequency"],
        "size": om_headers_dict[section][header]["size"],
        "average_size": om_headers_dict[section][header]["average_size"],
      } for header in om_headers_dict[section] if header not in pseudo_cors_headers)
    headers_list = sorted(headers_list, key=lambda x: x["frequency"], reverse=True)
    for index,stats in enumerate(headers_list):
      stats["rank"] = index+1
      if stats["header"] in om_header_stability_dict[section]:
        stability = om_header_stability_dict[section][stats["header"]]
        for s in stability:
          stats[s] = stability[s]
    additional_headers_list = []
    if section in additional_headers_dict:
      additional_headers_list = list(h for h in headers_list 
                                       if h["header"] in additional_headers_dict[section])
    headers_list = headers_list[:top]
    headers_list.extend(additional_headers_list)
    om_headers_dict[section] = headers_list

  if print_to_console:
    #print(json.dumps(om_headers_dict,indent=2))
    columns = ["rank","header","frequency","average_size","average_streak"]
    latex_output_str = "{0} \\\\\n".format(" & ".join(columns))
    csv_output_str = "{0}\n".format(",".join(columns))
    for section in om_headers_dict:
      if isinstance(om_headers_dict[section],dict):
        #latex_output_str += "{0} \\\\\n".format(" & ".join([]))
        #csv_output_str += "{0}\n".format(",".join([rank,header,frequency,average_size,average_streak]))
        print(om_headers_dict[section])
        continue
      for h in om_headers_dict[section]:
        rank = str(h["rank"])
        header = h["header"]
        frequency = str(round(float(h["frequency"]),2))
        average_size = str(round(float(h["average_size"]),2))
        average_streak = str(round(float(h["average_streak"]),2))
        latex_output_str += "{0} \\\\\n".format(" & ".join([rank,header,frequency,average_size,average_streak]))
        csv_output_str += "{0}\n".format(",".join([rank,header,frequency,average_size,average_streak]))
      latex_output_str += "{0} \\\\\n".format(" & "*(len(columns)-1))
      csv_output_str += "\n"
    print(latex_output_str)
    print()
    print(csv_output_str)
  else:
    return om_headers_dict


def main():
  # check running mode
  modes = {
    "--frequency": { "usage": ["headers stats file", "headers stability file", "top x", "additional headers file"],
                     "description": "Print the top x most frequent headers",
                     "call": mostFrequentHeaders },
    "--size": { "usage": ["headers stats file", "top x", "additional headers file"],
                "description": "Print the top x biggest headers (uses average size)",
                "call": biggestHeaders },
    "--stability": { "usage": ["headers stability file", "top x", "additional headers file"],
                     "description": "Print the top x stable headers",
                     "call": mostStableHeaders },
    "--om-frequency": { "usage": ["Origin Manifest headers stats file", "Origin Manifest headers stability file",
                                  "top x", "additional headers file"],
                        "description": "Print the top x most frequent Origin Manifest headers",
                        "call": mostFrequentOriginManifestHeaders },
  }

  if len(sys.argv) < 2 or sys.argv[1] not in modes:
    print("Usage (pick one of the following options):")
    for mode in modes:
      print("\t{0} {1}".format(mode," ".join("<{0}>".format(arg) for arg in modes[mode]["usage"])))
      print("\t\t{0}".format(modes[mode]["description"]))
    return

  mode = modes[sys.argv[1]]
  start = time.time()
  mode["call"](sys.argv[2:len(mode["usage"])+2])
  print("Finished after {0}".format(time.time() - start))


if __name__ == '__main__':
    main()
