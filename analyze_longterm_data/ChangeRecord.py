import bz2, json, os, statistics, sys, time
from enum import Enum
from urllib.parse import urlparse
from OriginManifestTools import *
from datetime import datetime


BZ2_FILENAME = "responses.json.bz2"

MINSIZE = 2

DIFF_ADDED = "ADDED"
DIFF_REMOVED = "REMOVED"
DIFF_MODIFIED = "MODIFIED"

SECTIONS = [OriginManifestSectionFallback,
            OriginManifestSectionBaseline,
            OriginManifestSectionAugmentOnly]
CORS_SECTIONS = [OriginManifestSectionCorsPreflight,
                OriginManifestSectionCorsPreflightWithCredentials]

'''
SECURITY_HEADERS = {
  OriginManifestSectionFallback: [
    #"set-cookie",
    "content-security-policy",
    "x-xss-protection",
    "timing-allow-origin",
    "strict-transport-security",
    "public-key-pins",
    "x-frame-options"
  ],
  OriginManifestSectionBaseline: [
    #"set-cookie",
    "content-security-policy",
    "x-xss-protection",
    "timing-allow-origin",
    "strict-transport-security",
    "public-key-pins",
    "x-frame-options"
  ],
  OriginManifestSectionAugmentOnly: [
    "set-cookie",
    #"content-security-policy",
    #"x-xss-protection",
    #"timing-allow-origin",
    #"strict-transport-security",
    #"public-key-pins",
    #"x-frame-options"
  ]
}
'''



class ChangeRecord:
  def __init__(self, day_str):
    self.day = day_str
    self.origin_manifests = dict()
    self.changes = dict()

  @staticmethod
  def createChangeRecord(day_str, response_bz2, common_urls, origins, cutoff, previous_record):
    changerecord = ChangeRecord(day_str)
    changerecord.__populateOriginManifests(response_bz2, common_urls, origins, cutoff)
    changerecord.__populateChanges(previous_record)
    return changerecord

  @staticmethod
  def loadFromFile(fname):
    json_obj = json.load(open(fname, "r"))
    changerecord = ChangeRecord(json_obj["day"])
    if "origin_manifests" in json_obj:
      changerecord.origin_manifests = json_obj["origin_manifests"]
    changerecord.changes = json_obj["changes"]
    return changerecord


  def __populateOriginManifests(self, response_bz2, common_urls, origins, cutoff):
    blacklist = []
    OMlearner = OriginManifestLearner()
    OMmerger = OriginManifestHeaderMerger()
    OMgenerator = OriginManifestGenerator(OMlearner, OMmerger, cutoff, minsize=MINSIZE,
                                          blacklist=blacklist)
    common_urls = dict((url, 1) for url in common_urls)

    # response_bz2 must parse to a list
    responses = json.loads(bz2.open(response_bz2).read().decode("utf-8"))
    for res in responses:
      # only consider what consistently re-appears
      if res["url"] in common_urls:
        headerdict = dict((x[0], x[1]) for x in res["headers"])
        OMlearner.learn(res["method"],res["url"],headerdict)
  
    self.origin_manifests = dict(om for om in ((o, OMgenerator.generateManifest(o)) for o in origins) if om[1] != None)


  def __populateChanges(self, previous_record):
    if not isinstance(previous_record, ChangeRecord):
      return
    # this assumes that the set of origins in the current and the previous ChangeRecord are the same
    for origin in self.origin_manifests:
      current = self.origin_manifests[origin]
      previous = previous_record.origin_manifests[origin]

      changes = dict()
      for section in SECTIONS:
        sec_change = dict()
        current_headers = current[section]
        previous_headers = previous[section]
        shared_headers = set()
        # identify added headers
        for h in current_headers:
          if h in previous_headers:
            shared_headers.add(h)
          else:
            sec_change[h] = DIFF_ADDED
        # identify removed headers
        for h in previous_headers:
          if h not in shared_headers:
            sec_change[h] = DIFF_REMOVED
        # identify modified headers
        for h in shared_headers:
          if current_headers[h] != previous_headers[h]:
            sec_change[h] = DIFF_MODIFIED
        # if there was a change remember it
        if len(sec_change):
          changes[section] = sec_change

      # CORS sections are a list of dicts, not a dict of header->values
      for section in CORS_SECTIONS:
        sec_change = dict()
        current_headers = current[section]
        previous_headers = previous[section]
        # identify added headers
        for h in current_headers:
          if h not in previous_headers:
            if DIFF_ADDED not in sec_change:
              sec_change[DIFF_ADDED] = []
            sec_change[DIFF_ADDED].append(h)
        # identify removed headers
        for h in previous_headers:
          if h not in current_headers:
            if DIFF_REMOVED not in sec_change:
              sec_change[DIFF_REMOVED] = []
            sec_change[DIFF_REMOVED].append(h)
        # identify modified headers
        # does not make sense because they are a set of values

        # if there was a change remember it
        if len(sec_change):
          changes[section] = sec_change

      # if there was a change add it to the overall record
      if len(changes):
        self.changes[origin] = changes


  def writeToFile(self, fname):
    json.dump({
        "day": self.day,
        "origin_manifests": self.origin_manifests,
        "changes": self.changes
      }, open(fname,"w"))


'''
def generateStatOriginManifestHeadersCount(changerecord_list):
  # result->section->header->count
  result = dict()
  for record in changerecord_list:
    for origin in record.origin_manifests:
      for section in SECTIONS:
        result[section] = dict()
        for header in record.origin_manifests[origin][section]:
          if header not in result[section]:
            result[section][header] = 0
          result[section][header] += 1
      for section in CORS_SECTIONS:
        result[section] = dict()
  print(result)
'''


'''
# Compute the number of Origin Manifest modifications made per day and in total
def generateStatManifestModsPerDay(headers, changerecord_list):
  # get per day how much every header of interest changed and how
  result = dict()
  result["total"] = dict()
  for section in SECTIONS:
    result["total"][section] = dict((header, 0) for header in headers[section])
  for section in CORS_SECTIONS:
    result["total"][section] = 0
  for record in changerecord_list:
    day = record.day
    result[day] = dict()
    # init the stats to 0 each
    for section in SECTIONS:
      result[day][section] = {
        DIFF_ADDED: 0,
        DIFF_REMOVED: 0,
        DIFF_MODIFIED: 0,
        "total": 0,
      }
    for section in CORS_SECTIONS:
      result[day][section] = {
        DIFF_ADDED: 0,
        DIFF_REMOVED: 0,
        DIFF_MODIFIED: 0,
        "total": 0,
      }
    # let's collect the real stats
    for origin in record.changes:
      for section in SECTIONS:
        res_sec = result[day][section]
        if section in record.changes[origin] and isinstance(record.changes[origin][section], dict):
          rec_sec = record.changes[origin][section]
          # we are only interested in particular headers
          for header in [h for h in rec_sec if h in headers[section]]:
            res_sec[rec_sec[header]] += 1
            res_sec["total"] += 1
            result["total"][section][header] += 1
        
      for section in CORS_SECTIONS:
        res_sec = result[day][section]
        if section in record.changes[origin] and isinstance(record.changes[origin][section], dict):
          rec_sec = record.changes[origin][section]
          for diff in rec_sec:
            res_sec[diff] += len(rec_sec[diff])
            res_sec["total"] += len(rec_sec[diff])
            result["total"][section] += len(rec_sec[diff])
  return result
'''


'''
# how often do particular headers appear in Origin Manifest?
def generateStatHeaderOccurrences(headers, changerecord_list):
  result = dict()
  result["total"] = dict()
  for section in SECTIONS:
    result["total"][section] = dict((h,0) for h in headers[section])
  for section in CORS_SECTIONS:
    result["total"][section] = 0
  for record in changerecord_list:
    day = record.day
    result[day] = dict()
    # init everything to 0
    for section in SECTIONS:
      result[day][section] = dict((h,0) for h in headers[section])
    for section in CORS_SECTIONS:
      result[day][section] = 0
    # let's collect the real stats
    for origin in record.origin_manifests:
      manifest = record.origin_manifests[origin]
      for section in SECTIONS:
        if section not in manifest:
          continue
        for header in [h for h in manifest[section] if h in headers[section]]:
          result[day][section][header] += 1
          result["total"][section][header] += 1
      for section in CORS_SECTIONS:
        if section not in manifest:
          continue
        result[day][section] += len(manifest[section])
        result["total"][section] += len(manifest[section])
  return result
'''


'''
# how often do Origin Manifests change? How stable are they?
# return: list of origins per day which had a change in at least one of the |headers|
def generateManifestChangesPerOrigin(origins, headers, changerecord_list):
  date_format = "%Y-%m-%d"
  result = dict()
  result["days"] = dict()
  for record in changerecord_list:
    day = record.day
    result["days"][day] = list()
    # we only do that once because all days have the same origins
    if "renewals" not in result:
      result["renewals"] = dict(
        (origin, {
          "latest": datetime.strptime(day, date_format),
          "longest_streak": 0,
          "shortest_streak": len(changerecord_list),
          "streak_history": list()
        })
        for origin in origins)
    for origin in [o for o in record.changes if o in origins]:
      changes_origin = record.changes[origin]
      for section in changes_origin:
        if (section in SECTIONS and \
            len([h for h in changes_origin[section] if h in headers[section]]) > 0) \
           or (section in CORS_SECTIONS):
          result["days"][day].append(origin)
          # update renewal stats
          renewal = result["renewals"][origin]
          current_date = datetime.strptime(day, date_format)
          days_alive = (current_date - renewal["latest"]).days
          if days_alive > renewal["longest_streak"]:
            renewal["longest_streak"] = days_alive
          if days_alive < renewal["shortest_streak"]:
            renewal["shortest_streak"] = days_alive
          renewal["latest"] = current_date
          renewal["streak_history"].append(days_alive)
          break

  # update renewal stats one last time
  for origin in result["renewals"]:
    renewal = result["renewals"][origin]
    current_date = datetime.strptime(day, date_format)
    days_alive = (current_date - renewal["latest"]).days+1
    if days_alive > renewal["longest_streak"]:
      renewal["longest_streak"] = days_alive
    if days_alive < renewal["shortest_streak"]:
      renewal["shortest_streak"] = days_alive
    renewal["latest"] = renewal["latest"].strftime(date_format)
    #renewal["streak_history"].append(days_alive)
    if len(renewal["streak_history"]) > 0:
      renewal["average_streak"] = round(sum(renewal["streak_history"]) \
                                        / len(renewal["streak_history"]),2)
      renewal["median_streak"] = round(statistics.median(renewal["streak_history"]),2)
    else:
      renewal["average_streak"] = 100
      renewal["median_streak"] = 100
  return result
'''


'''
def generateStatAlwaysEmptyOriginManifest(origins, headers, changerecord_list):
  always_empty = set(origins)
  non_empty = set()
  for record in changerecord_list:
    for origin in set(always_empty):
      is_empty = True
      for section in SECTIONS:
        if len([h for h in record.origin_manifests[origin][section] if h in headers[section]]) > 0:
          #this is not the origin we are looking for
          is_empty = False
          break
      if is_empty:
        for section in CORS_SECTIONS:
          if len(record.origin_manifests[origin][section]) > 0:
            is_empty = False
            break
      # it was not empty at some point, we don't want it 
      if not is_empty:
        always_empty.remove(origin)
        non_empty.add(origin)
  print("always_empty: {0}".format(len(always_empty))),
  print("non_empty: {0}".format(len(non_empty))),
  return {
    "always_empty": list(always_empty),
    "non_empty": list(non_empty)
  }
'''


'''
def runGenerateCommonURLs(mode_id, usage_str):
  if len(sys.argv) < 4:
    print("{0} {1} {2}".format(sys.argv[0], mode_id, usage_str))
    return
  raw_data_dir = sys.argv[2].strip("/")
  output_file = sys.argv[3]
  common_urls = extractCommonURLs(raw_data_dir,output_file)
'''


'''
def runGenerateChangeRecords(mode_id, usage_str):
  # we do not require cutoff_max but set it to 100 by default
  if len(sys.argv) < 7 or not sys.argv[5].isdigit() or not sys.argv[6].isdigit() \
     or (len(sys.argv) >= 8 and not sys.argv[7].isdigit()):
    print("{0} {1} {2}".format(sys.argv[0], mode_id, usage_str))
    return
  common_urls_file = sys.argv[2]
  raw_data_dir = sys.argv[3].strip("/")
  output_dir = sys.argv[4].strip("/")
  cutoff_start = int(sys.argv[5])
  cutoff_step = int(sys.argv[6])
  if cutoff_step <= 0:
    cutoff_step = 10
  cutoff_max = int(sys.argv[7]) if len(sys.argv) >= 8 else 100
  generateChangeRecords(common_urls_file, raw_data_dir, output_dir, cutoff_start, cutoff_step, cutoff_max)
'''


'''
def runGenerateStatOriginManifestHeadersCount(mode_id, usage_str):
  if len(sys.argv) < 4:
    print("{0} {1} {2}".format(sys.argv[0], mode_id, usage_str))
    return
  origins = json.load(open(sys.argv[2], "r"))["50"]["non_empty"]
  record_dir = sys.argv[3]
  record_files = next(os.walk(record_dir))[2]
  record_files.sort()
  changerecord_list = list(ChangeRecord.loadFromFile("{0}/{1}".format(record_dir,fname))
                             for fname in record_files)
  generateStatOriginManifestHeadersCount(changerecord_list)
'''


'''
def runGenerateStatManifestModsPerDay(mode_id, usage_str):
  if len(sys.argv) < 4:
    print("{0} {1} {2}".format(sys.argv[0], mode_id, usage_str))
    return
  record_dir = sys.argv[2]
  record_files = next(os.walk(record_dir))[2]
  record_files.sort()
  changerecord_list = list(ChangeRecord.loadFromFile("{0}/{1}".format(record_dir,fname))
                             for fname in record_files)
  stat_om_mods = generateStatManifestModsPerDay(SECURITY_HEADERS, changerecord_list)
  json.dump(stat_om_mods,open(sys.argv[3],"w"),indent=2)
'''


'''
def runGenerateStatHeaderOccurrences(mode_id, usage_str):
  if len(sys.argv) < 4:
    print("{0} {1} {2}".format(sys.argv[0], mode_id, usage_str))
    return
  record_dir = sys.argv[2]
  record_files = next(os.walk(record_dir))[2]
  record_files.sort()
  changerecord_list = list(ChangeRecord.loadFromFile("{0}/{1}".format(record_dir,fname))
                             for fname in record_files)
  stat_header_occ = generateStatHeaderOccurrences(SECURITY_HEADERS, changerecord_list)
  json.dump(stat_header_occ,open(sys.argv[3],"w"),indent=2)
'''


'''
def runGenerateManifestChangesPerOrigin(mode_id, usage_str):
  if len(sys.argv) < 5:
    print("{0} {1} {2}".format(sys.argv[0], mode_id, usage_str))
    return
  origins = extractOrigins(json.load(open(sys.argv[2], "r")), MINSIZE)
  record_dir = sys.argv[3]
  record_files = next(os.walk(record_dir))[2]
  record_files.sort()
  changerecord_list = list(ChangeRecord.loadFromFile("{0}/{1}".format(record_dir,fname))
                             for fname in record_files)
  stat_header_occ = generateManifestChangesPerOrigin(origins, SECURITY_HEADERS, changerecord_list)
  json.dump(stat_header_occ,open(sys.argv[4],"w"),indent=2)
'''


'''
def runGenerateManifestChangesPerOriginNonEmpty(mode_id, usage_str):
  if len(sys.argv) < 5:
    print("{0} {1} {2}".format(sys.argv[0], mode_id, usage_str))
    return
  origins = json.load(open(sys.argv[2], "r"))["50"]["non_empty"]
  print(len(origins))
  record_dir = sys.argv[3]
  record_files = next(os.walk(record_dir))[2]
  record_files.sort()
  changerecord_list = list(ChangeRecord.loadFromFile("{0}/{1}".format(record_dir,fname))
                             for fname in record_files)
  stat_header_occ = generateManifestChangesPerOrigin(origins, SECURITY_HEADERS, changerecord_list)
  json.dump(stat_header_occ,open(sys.argv[4],"w"),indent=2)
'''


'''
def runGenerateStatAlwaysEmptyOriginManifest(mode_id, usage_str):
  if len(sys.argv) < 4:
    print("{0} {1} {2}".format(sys.argv[0], mode_id, usage_str))
    return
  origins = extractOrigins(json.load(open(sys.argv[2], "r")), MINSIZE)
  record_dir = sys.argv[3]
  output_file = sys.argv[4]
  record_subdirs = next(os.walk(record_dir))[1]
  record_subdirs.sort()
  stat_empty_manifests = dict()
  stat_empty_manifests["num_origins"] = len(origins)
  for subdir in record_subdirs:
    print("cutoff: {0}".format(subdir))
    record_files = next(os.walk("{0}/{1}".format(record_dir,subdir)))[2]
    changerecord_list = list(ChangeRecord.loadFromFile("{0}/{1}/{2}".format(record_dir,subdir,fname))
                             for fname in record_files)
    stat_empty_manifests[subdir] = generateStatAlwaysEmptyOriginManifest(origins, SECURITY_HEADERS, changerecord_list)
  json.dump(stat_empty_manifests, open(output_file,"w"), indent=2)
'''


'''
def main():
  # check running mode
  modes = {
    "--common-urls": { "usage": "<raw data dir> <output file>",
                       "call": runGenerateCommonURLs },
    "--changes": { "usage": "<common urls file> <raw data dir> <output dir> <start cutoff value> <cutoff value steps> <max cutoff value>",
                   "call": runGenerateChangeRecords },
    "--stat-om-mods": { "usage": "<change records dir> <output file>",
                        "call": runGenerateStatManifestModsPerDay },
    "--stat-header-occurrences": { "usage": "<change records dir> <output file>",
                        "call": runGenerateStatHeaderOccurrences },
    "--stat-changes-per-day": { "usage": "<common urls file> <change records dir> <output file>",
                                "call": runGenerateManifestChangesPerOrigin },
    "--stat-changes-per-day-non-empty": { "usage": "<non-empty origins file> <change records dir> <output file>",
                                "call": runGenerateManifestChangesPerOriginNonEmpty },
    "--stat-always-empty": { "usage": "<common urls file> <change records dir> <output file>",
                                "call": runGenerateStatAlwaysEmptyOriginManifest },
    "--stat-headers-count": { "usage": "<common urls file> <change records subdir>",
                               "call": runGenerateStatOriginManifestHeadersCount }
  }
  if len(sys.argv) < 2 or sys.argv[1] not in modes:
    print("Usage (pick one of the following options):")
    for mode in modes:
      print("  {0} {1}".format(mode,modes[mode]["usage"]))
    return

  mode = modes[sys.argv[1]]
  start = time.time()
  mode["call"](sys.argv[1], mode["usage"])
  print("Finished after {0}".format(time.time() - start))


if __name__ == '__main__':
    main()
'''
