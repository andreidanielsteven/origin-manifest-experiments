import re 
import sys 

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Functions                                                                   #
###############################################################################

class HPKP:
  # Public-Key-Pins: pin-sha256="<pin-value>"; 
  #               max-age=<expire-time>; 
  #               includeSubDomains; 
  #               report-uri="<uri>"
  def __init__(self, pin_sha256=None, max_age=None, includeSubDomains=False):
    self.pin_sha256 = pin_sha256 if isinstance(pin_sha256, set) else set();
    self.max_age = max_age if isinstance(max_age, int) else None;
    self.includeSubDomains = includeSubDomains if isinstance(includeSubDomains, bool) else False;

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, HPKP) \
      and self.pin_sha256 == other.pin_sha256 \
      and self.max_age == other.max_age \
      and self.includeSubDomains == other.includeSubDomains;

  def FromString(input_str):
    if not isinstance(input_str, str):
      raise TypeError("'FromString' expects a string.");
    result = HPKP();
    for directive in map(str.strip, input_str.split(";")):
      match =  re.match("^pin-sha256=\"(.+)\"$",directive,re.I);
      if match != None and len(match.groups()) > 0:
        result.pin_sha256.add(match.group(1));
        continue;
      match =  re.match("^max-age=(.+)$",directive,re.I);
      if match != None and len(match.groups()) > 0:
        value = match.group(1).strip('"');
        if value.isdigit():
          result.max_age = int(value);
        continue;
      match =  re.match("^includeSubDomains$",directive,re.I);
      if match != None:
        result.includeSubDomains = True;
        continue;
    return result;

  def isValidHeader(self):
    return isinstance(self.pin_sha256, set) and len(self.pin_sha256) > 0 \
      and isinstance(self.max_age, int) and isinstance(self.includeSubDomains, bool);

  def toString(self):
    pins_str = "; ".join(["pin-sha256=\"{0}\"".format(pin) for pin in self.pin_sha256]);
    max_age_str = "max-age={0}".format(self.max_age) if isinstance(self.max_age, int) else "";
    includeSubDomains_str = "includeSubDomains" if self.includeSubDomains else "";
    return "; ".join([x for x in [pins_str, max_age_str, includeSubDomains_str] if x != ""]);

# end class HSTS


def isValidHPKP(hpkp):
  return isinstance(hpkp, HPKP) and hpkp.isValidHeader();


def mergeToWeakerHPKP(hpkp1, hpkp2):
  if not isValidHPKP(hpkp1) or not isValidHPKP(hpkp2):
    return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE,
                       "Tried to merge with invalid header.");

  result = HPKP();
  if len(hpkp1.pin_sha256) > 0 and len(hpkp2.pin_sha256) > 0:
    result.pin_sha256 = hpkp1.pin_sha256.union(hpkp2.pin_sha256);
  result.max_age = min(hpkp1.max_age,hpkp2.max_age);
  result.includeSubDomains = hpkp1.includeSubDomains and hpkp2.includeSubDomains;

  if result.isValidHeader():
    return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE);
  return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE, "Invalid merge result.");


def mergeToStricterHPKP(hpkp1, hpkp2):
  if not isValidHPKP(hpkp1) and not isValidHPKP(hpkp2):
    return MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER,
                       "Tried to merge with invalid header.");
  elif not isValidHPKP(hpkp1):
    return MergeResult(hpkp2, Ops.OP_USE_IN_RESPONSE_HEADER);
  elif not isValidHPKP(hpkp2):
    return MergeResult(hpkp1, Ops.OP_USE_IN_RESPONSE_HEADER);

  result = HPKP();
  if len(hpkp1.pin_sha256) > 0 and len(hpkp2.pin_sha256) > 0:
    result.pin_sha256 = hpkp1.pin_sha256.intersection(hpkp2.pin_sha256);
    if len(result.pin_sha256) == 0:
      return MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER,
                         "Invalid merge result: no pins.");
  elif len(hpkp1.pin_sha256) == 0 and len(hpkp2.pin_sha256) > 0:
    result.pin_sha256 = hpkp2.pin_sha256;
  elif len(hpkp1.pin_sha256) > 0 and len(hpkp2.pin_sha256) == 0:
    result.pin_sha256 = hpkp1.pin_sha256;

  result.max_age = max(hpkp1.max_age,hpkp2.max_age);
  result.includeSubDomains = hpkp1.includeSubDomains or hpkp2.includeSubDomains;

  if result.isValidHeader():
    return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER);
  return MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER, "Invalid merge result.");


def mergeToWeaker(hpkp1_str, hpkp2_str):
  if not isinstance(hpkp1_str, str) or not isinstance(hpkp2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  # from Section 2.3.1 in RFC 7469: "the UA MUST process only the first PKP header
  # field (if present) and only the first PKP-RO header field (if present)."
  hpkp1_list = hpkp1_str.split(",");
  if len(hpkp1_list) > 0:
    hpkp1_str = hpkp1_list[0];
  hpkp2_list = hpkp2_str.split(",");
  if len(hpkp2_list) > 0:
    hpkp2_str = hpkp2_list[0];
  result = mergeToWeakerHPKP(HPKP.FromString(hpkp1_str), HPKP.FromString(hpkp2_str));
  if isinstance(result.value, HPKP):
    result.value = str(result.value);
  return result;


def mergeToStricter(hpkp1_str, hpkp2_str):
  if not isinstance(hpkp1_str, str) or not isinstance(hpkp2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  # from Section 2.3.1 in RFC 7469: "the UA MUST process only the first PKP header
  # field (if present) and only the first PKP-RO header field (if present)."
  hpkp1_list = hpkp1_str.split(",");
  if len(hpkp1_list) > 0:
    hpkp1_str = hpkp1_list[0];
  hpkp2_list = hpkp2_str.split(",");
  if len(hpkp2_list) > 0:
    hpkp2_str = hpkp2_list[0];
  result = mergeToStricterHPKP(HPKP.FromString(hpkp1_str), HPKP.FromString(hpkp2_str));
  if isinstance(result.value, HPKP):
    result.value = str(result.value);
  return result;


def applyToHeader(policy, header):
  return mergeToStricter(policy, header);
