import sys 

from .set_cookie_merger import *
from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testSetCookiefromString();
  testMergeToWeakerSetCookie();
  testMergeToStricterSetCookie();
  testSetCookieToString();
  testMergeToWeaker();
  testMergeToStricter();
  testApplyToHeader();


def testSetCookiefromString():
  print("Running 'testSetCookiefromString'...");
  test_cases = [
    { "source": "", "expected": SetCookie() },
    { "source": "x=asdf", "expected": SetCookie() },
    { "source": "x=asdf; ; ; ;", "expected": SetCookie() },
    { "source": "x=asdf; Expires=Wed, 21 Oct 2015 07:28:00 GIT",
      "expected": SetCookie() },
    { "source": "x=asdf; Max-Age=23", "expected": SetCookie() },
    { "source": "x=asdf; Domain=a.com", "expected": SetCookie() },
    { "source": "x=asdf; Path=/xyz", "expected": SetCookie() },
    { "source": "x=asdf; Secure", "expected": SetCookie(True) },
    { "source": "x=asdf; HttpOnly", "expected": SetCookie(False,True) },
    { "source": "x=asdf; SameSite", "expected": SetCookie(False,False) },
    { "source": "x=asdf; SameSite=Strict", "expected": SetCookie(False,False) },
    { "source": "x=asdf; SameSite=Lax", "expected": SetCookie(False,False) },
    { "source": "x=asdf; Expires=Wed, 21 Oct 2015 07:28:00 GMT; Max-Age=23; "
                "Domain=a.com; Path=/xyz; Secure; HttpOnly; SameSite=strict",
      "expected": SetCookie(True,True) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    sc = SetCookie.FromString(test["source"]);
    if sc != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' parsed to '{2}' but '{3}' was expected.".format( \
             index, test["source"], sc, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerSetCookie():
  print("Running 'testMergeToWeakerSetCookie'...");

  test_cases = [
    { "policy1": SetCookie(), "policy2": SetCookie(),
      "expected": MergeResult(SetCookie(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": SetCookie(False, False), "policy2": SetCookie(False, False),
      "expected": MergeResult(SetCookie(False,False), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": SetCookie(True, False), "policy2": SetCookie(False, False),
      "expected": MergeResult(SetCookie(False,False), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": SetCookie(False, True), "policy2": SetCookie(False, False),
      "expected": MergeResult(SetCookie(False,False), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": SetCookie(True, True), "policy2": SetCookie(False, False),
      "expected": MergeResult(SetCookie(False,False), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": SetCookie(True, False), "policy2": SetCookie(True, False),
      "expected": MergeResult(SetCookie(True,False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": SetCookie(False, True), "policy2": SetCookie(True, False),
      "expected": MergeResult(SetCookie(False,False), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": SetCookie(True, True), "policy2": SetCookie(True, False),
      "expected": MergeResult(SetCookie(True,False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": SetCookie(False, True), "policy2": SetCookie(False, True),
      "expected": MergeResult(SetCookie(False,True), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": SetCookie(True, True), "policy2": SetCookie(False, True),
      "expected": MergeResult(SetCookie(False,True), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": SetCookie(True, True), "policy2": SetCookie(True, True),
      "expected": MergeResult(SetCookie(True,True), Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    sc1 = mergeToWeakerSetCookie(test["policy1"],test["policy2"]);
    sc2 = mergeToWeakerSetCookie(test["policy2"],test["policy1"]);
    if sc1 != test["expected"] or sc2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], sc1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterSetCookie():
  print("Running 'testMergeToStricterSetCookie'...");
  test_cases = [
    { "policy1": SetCookie(), "policy2": SetCookie(),
      "expected": MergeResult(SetCookie(), Ops.OP_NOOP) },
    { "policy1": SetCookie(False, False), "policy2": SetCookie(False, False),
      "expected": MergeResult(SetCookie(False,False), Ops.OP_NOOP) },
    { "policy1": SetCookie(True, False), "policy2": SetCookie(False, False),
      "expected": MergeResult(SetCookie(True,False), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": SetCookie(False, True), "policy2": SetCookie(False, False),
      "expected": MergeResult(SetCookie(False,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": SetCookie(True, True), "policy2": SetCookie(False, False),
      "expected": MergeResult(SetCookie(True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": SetCookie(True, False), "policy2": SetCookie(True, False),
      "expected": MergeResult(SetCookie(True,False), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": SetCookie(False, True), "policy2": SetCookie(True, False),
      "expected": MergeResult(SetCookie(True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": SetCookie(True, True), "policy2": SetCookie(True, False),
      "expected": MergeResult(SetCookie(True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": SetCookie(False, True), "policy2": SetCookie(False, True),
      "expected": MergeResult(SetCookie(False,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": SetCookie(True, True), "policy2": SetCookie(False, True),
      "expected": MergeResult(SetCookie(True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": SetCookie(True, True), "policy2": SetCookie(True, True),
      "expected": MergeResult(SetCookie(True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    sc1 = mergeToStricterSetCookie(test["policy1"],test["policy2"]);
    sc2 = mergeToStricterSetCookie(test["policy2"],test["policy1"]);
    if sc1 != test["expected"] or sc2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], sc1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testSetCookieToString():
  print("Running 'testSetCookieToString'...");
  test_cases = [
    { "source": SetCookie(), "expected": "" },
    { "source": SetCookie(False, False), "expected": "" },
    { "source": SetCookie(True, False), "expected": "secure" },
    { "source": SetCookie(False, True), "expected": "httpOnly" },
    { "source": SetCookie(True, True), "expected": ["secure; httpOnly", "httpOnly; secure"] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output_str = test["source"].toString();
    if output_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' transformed to '{2}' but any in '{3}' was "
            "expected.".format(index, test["source"], output_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "a=b; secure", "policy2": "c=d",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "a=b; httpOnly", "policy2": "c=d",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "a=b; secure; httpOnly", "policy2": "c=d",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "a=b; secure", "policy2": "c=d; secure",
      "expected": MergeResult("secure", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "a=b; httpOnly", "policy2": "c=d; secure",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "a=b; secure; httpOnly", "policy2": "c=d; secure",
      "expected": MergeResult("secure", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "a=b; httpOnly", "policy2": "c=d; httpOnly",
      "expected": MergeResult("httpOnly", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "a=b; secure; httpOnly", "policy2": "c=d; httpOnly",
      "expected": MergeResult("httpOnly", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "a=b; secure; httpOnly", "policy2": "c=d; secure; httpOnly",
      "expected": MergeResult("secure; httpOnly", Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToWeaker(test["policy1"],test["policy2"]);
    result2 = mergeToWeaker(test["policy1"],test["policy2"]);
    if result1 != test["expected"] or result2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult("", Ops.OP_NOOP) },
    { "policy1": "a=b; secure", "policy2": "c=d",
      "expected": MergeResult("secure", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "a=b; httpOnly", "policy2": "c=d",
      "expected": MergeResult("httpOnly", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "a=b; secure; httpOnly", "policy2": "c=d",
      "expected": MergeResult("secure; httpOnly", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "a=b; secure", "policy2": "c=d; secure",
      "expected": MergeResult("secure", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "a=b; httpOnly", "policy2": "c=d; secure",
      "expected": MergeResult("secure; httpOnly", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "a=b; secure; httpOnly", "policy2": "c=d; secure",
      "expected": MergeResult("secure; httpOnly", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "a=b; httpOnly", "policy2": "c=d; httpOnly",
      "expected": MergeResult("httpOnly", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "a=b; secure; httpOnly", "policy2": "c=d; httpOnly",
      "expected": MergeResult("secure; httpOnly", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "a=b; secure; httpOnly", "policy2": "c=d; secure; httpOnly",
      "expected": MergeResult("secure; httpOnly", Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToStricter(test["policy1"],test["policy2"]);
    result2 = mergeToStricter(test["policy1"],test["policy2"]);
    if result1 != test["expected"] or result2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testApplyToHeader():
  print("Running 'testApplyToHeader'...");
  test_cases = [
    { "policy": "", "header": "",
      "expected": [MergeResult("", Ops.OP_NOOP)] },
    { "policy": "", "header": "a=b",
      "expected": [MergeResult("a=b", Ops.OP_NOOP)] },
    { "policy": "secure", "header": "a=b",
      "expected": [MergeResult("a=b; secure", Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy": "httpOnly", "header": "a=b",
      "expected": [MergeResult("a=b; httpOnly", Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy": "secure; httpOnly", "header": "a=b",
      "expected": [MergeResult("a=b; secure; httpOnly",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy": "secure", "header": "a=b; secure",
      "expected": [MergeResult("a=b; secure",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy": "httpOnly", "header": "a=b; secure",
      "expected": [MergeResult("a=b; secure; httpOnly",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy": "secure; httpOnly", "header": "a=b; secure",
      "expected": [MergeResult("a=b; secure; httpOnly",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy": "secure", "header": "a=b; httpOnly",
      "expected": [MergeResult("a=b; secure; httpOnly",
                               Ops.OP_USE_IN_RESPONSE_HEADER),
                   MergeResult("a=b; httpOnly; secure",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy": "httpOnly", "header": "a=b; httpOnly",
      "expected": [MergeResult("a=b; httpOnly",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy": "secure; httpOnly", "header": "a=b; httpOnly",
      "expected": [MergeResult("a=b; secure; httpOnly",
                               Ops.OP_USE_IN_RESPONSE_HEADER),
                   MergeResult("a=b; httpOnly; secure",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    augmented = applyToHeader(test["policy"], test["header"]);
    if augmented not in test["expected"]:
      success = False;
      print("test {0} failed: Applying header '{1}' to cookie '{2}' resulted in '{3}' "
            "but expected was '{4}'"
            .format(index, test["header"], test["policy"], augmented, test["expected"]));
      break;
  print("success." if success else "FAILED.");
 

###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
