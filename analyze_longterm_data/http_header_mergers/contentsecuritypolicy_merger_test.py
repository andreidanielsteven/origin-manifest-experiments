import sys 

from .contentsecuritypolicy_merger import *
from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testIsValidDirectiveName();
  testContentSecurityPolicyFromString();
  testMergeToWeakerContentSecurityPolicy();
  testMergeToStricterContentSecurityPolicy();
  testIsAllowedByWhitelist();
  testToString();
  testMergeToWeaker();
  testMergeToStricter();


def testIsValidDirectiveName():
  print("Running 'testIsValidDirectiveName'...");
  test_cases = [
    { "name": 42, "expected": False },
    { "name": "asdf", "expected": False },
    { "name": "default-src", "expected": True },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    if (ContentSecurityPolicy.isValidDirectiveName(test["name"]) != test["expected"]):
      success = False;
      print("test {0} failed: {1}".format(test));
      break;
  print("success." if success else "FAILED.");


def testContentSecurityPolicyFromString():
  print("Running 'testContentSecurityPolicyFromString'...");
  test_cases = [
    { "csp_str": 42, "expected": "'FromString' expects a string." },
    { "csp_str": "asdf", "expected": ContentSecurityPolicy() },
    { "csp_str": "default-src 'none'",
      "expected": ContentSecurityPolicy({ "default-src": {"'none'"} }) },
    { "csp_str": "default-src 'none'; script-src http://a.com 'unsafe-inline'",
      "expected": ContentSecurityPolicy({ "default-src": {"'none'"}, "script-src": {"http://a.com", "'unsafe-inline'"} }) },
    { "csp_str": "default-src 'none'; asdf http://a.com; 'unsafe-inline'",
      "expected": ContentSecurityPolicy({ 'default-src': {"'none'"} }) },
    { "csp_str": "default-src 'none'; asdf http://a.com; script-src 'unsafe-inline'",
      "expected": ContentSecurityPolicy({ "default-src": {"'none'"}, "script-src": {"'unsafe-inline'"} }) },
    { "csp_str": "default-src 'none' a.com; script-src 'unsafe-inline' 'none'",
      "expected": ContentSecurityPolicy({ "default-src": {"a.com"}, "script-src": {"'unsafe-inline'"} }) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    csp = None;
    try:
      csp = ContentSecurityPolicy.FromString(test["csp_str"]);
    except Exception as e:
      csp = str(e);
    if csp != test["expected"]:
      success = False;
      print("test {0} failed: Actual CSP '{1}' does not match expected CSP '{2}'"
               .format(index, csp, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerContentSecurityPolicy():
  print("Running 'testMergeToWeakerContentSecurityPolicy'...");
  test_cases = [
    { "csp1": ContentSecurityPolicy(), "csp2": ContentSecurityPolicy(),
      "expected": MergeResult(ContentSecurityPolicy(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "img-src": {"*"} }),
      "csp2": ContentSecurityPolicy({ "img-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy({ "img-src": {"*"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "img-src": {"http:"} }),
      "csp2": ContentSecurityPolicy({ "img-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy({ "img-src": {"http:", "a.com"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'nonce-FOO'"} }),
      "csp2": ContentSecurityPolicy({ "img-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy({ "img-src": {"a.com"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "a.com"} }),
      "csp2": ContentSecurityPolicy({ "img-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "a.com"},
                                      "img-src": {"a.com"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "a.com"},
                                      "img-src": {"b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "a.com"},
                                  "img-src": {"a.com", "b.com"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "a.com"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"'unsafe-inline'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "a.com",
                                                 "b.com"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'unsafe-inline'", "a.com"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"'unsafe-inline'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"'unsafe-inline'", "a.com",
                                                  "b.com"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "block-all-mixed-content": {} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"}}),
      "expected": MergeResult(ContentSecurityPolicy(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "plugin-types": {"application/x-shockwave-flash"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"}}),
      "expected": MergeResult(ContentSecurityPolicy(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "referrer": {"no-referrer"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"}}),
      "expected": MergeResult(ContentSecurityPolicy(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "require-sri-for-script": {} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"}}),
      "expected": MergeResult(ContentSecurityPolicy(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "sandbox": {"allow-same-origin"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"}}),
      "expected": MergeResult(ContentSecurityPolicy(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "upgrade-insecure-requests": {} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"}}),
      "expected": MergeResult(ContentSecurityPolicy(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'none'"},
                                      "script-src": {"a.com", "'unsafe-inline'"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"'none'"},
                                      "script-src": {"'unsafe-inline'"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"'none'"},
                                  "script-src": {"'unsafe-inline'", "a.com"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'none'"},
                                      "script-src": {"a.com", "'unsafe-inline'"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"'none'"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"'none'"},
                                  "script-src": {"a.com", "'unsafe-inline'"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "data:"},
                                      "img-src": {"a.com"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "*"},
                                      "img-src": {"b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "*", "data:"},
                                  "img-src": {"a.com", "b.com"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'none'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "b.com"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'nonce-FOO'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "data:", "*",
                                                 "http:", "https:", "ws:", "wss:"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "'nonce-FOO'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'nonce-FOO'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "data:", "*",
                                                 "http:", "https:", "ws:", "wss:"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'strict-dynamic'", "'nonce-FOO'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-eval'", "'unsafe-inline'",
                                                 "data:", "*", "http:", "https:",
                                                 "ws:", "wss:"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'unsafe-inline'", "'nonce-FOO'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'nonce-FOO'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "data:", "*",
                                                 "http:", "https:", "ws:", "wss:"} }),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    combined1 = mergeToWeakerContentSecurityPolicy(test["csp1"], test["csp2"]);
    combined2 = mergeToWeakerContentSecurityPolicy(test["csp2"], test["csp1"]);
    if combined1 != test["expected"] or combined2 != test["expected"]:
      success = False;
      print("test {0} failed: combining '{1}' with '{2}' resulted in '{3}' but "
            "'{4}' was expected"
            .format(index,test["csp1"],test["csp2"],combined1,test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterContentSecurityPolicy():
  print("Running 'testMergeToStricterContentSecurityPolicy'...");
  test_cases = [
    { "csp1": ContentSecurityPolicy(), "csp2": ContentSecurityPolicy(),
      "expected": MergeResult(ContentSecurityPolicy(),
                              Ops.OP_DELETE_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "img-src": {"*"} }),
      "csp2": ContentSecurityPolicy({ "img-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy({ "img-src": {"a.com"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "img-src": {"http:"} }),
      "csp2": ContentSecurityPolicy({ "img-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy({ "img-src": {"http://a.com"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'nonce-FOO'"} }),
      "csp2": ContentSecurityPolicy({ "img-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"'none'"},
                                  "img-src": {"'none'"},
                                  "script-src": {"'unsafe-inline'", "data:", "*",
                                                 "http:", "https:", "ws:", "wss:"},
                                  "style-src": {"'unsafe-inline'", "data:", "*",
                                                "http:", "https:", "ws:", "wss:"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "a.com"} }),
      "csp2": ContentSecurityPolicy({ "img-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "a.com"},
                                  "img-src": {"a.com"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "a.com"},
                                      "img-src": {"a.com"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "a.com"},
                                      "img-src": {"b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "a.com"},
                                  "img-src": {"'none'"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "a.com"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"'unsafe-inline'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"'unsafe-inline'", "b.com"},
                                  "script-src": {"'unsafe-inline'"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'unsafe-inline'", "a.com"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"'unsafe-inline'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"'unsafe-inline'"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "block-all-mixed-content": set() }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"a.com"},
                                  "block-all-mixed-content": set() }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "plugin-types": {"application/x-shockwave-flash"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"a.com"},
                                  "plugin-types": {"application/x-shockwave-flash"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "referrer": {"no-referrer"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"a.com"},
                                  "referrer": {"no-referrer"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "require-sri-for-script": set() }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"a.com"},
                                  "require-sri-for-script": set() }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "sandbox": {"allow-same-origin"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"}}),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"a.com"},
                                  "sandbox": {"allow-same-origin"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "upgrade-insecure-requests": set() }),
      "csp2": ContentSecurityPolicy({ "default-src": {"a.com"}}),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"a.com"},
                                  "upgrade-insecure-requests": set() }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'none'"},
                                      "script-src": {"a.com", "'unsafe-inline'"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"'none'"},
                                      "script-src": {"'unsafe-inline'"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"'none'"},
                                  "script-src": {"'unsafe-inline'"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'none'"},
                                      "script-src": {"a.com", "'unsafe-inline'"} }),
      "csp2": ContentSecurityPolicy({ "default-src": {"'none'"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"'none'"},
                                  "script-src": {"'none'"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "data:"},
                                      "img-src": {"a.com"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "*"},
                                      "img-src": {"b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'"},
                                  "img-src": {"'none'"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'none'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy({ "script-src": {"'none'"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'nonce-FOO'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'",
                                                     "'nonce-FOO'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'nonce-FOO'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'", "data:", "*",
                                                 "http:", "https:", "ws:", "wss:",
                                                 "http://b.com", "https://b.com",
                                                 "ws://b.com", "wss://b.com"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'strict-dynamic'",
                                                     "'nonce-FOO'", "b.com"} }),
      "expected": MergeResult(ContentSecurityPolicy(
                                { "script-src": {"'unsafe-inline'"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": ContentSecurityPolicy({ "default-src": {"'unsafe-inline'",
                                                      "'nonce-FOO'"} }),
      "csp2": ContentSecurityPolicy({ "script-src": {"'nonce-FOO'", "b.com"} }),
      # TODO for cosmetics remove the 'unsafe-inline'
      "expected": MergeResult(ContentSecurityPolicy(
                                { "default-src": {"'unsafe-inline'"},
                                  "script-src": {"'unsafe-inline'", "data:", "*",
                                                 "http:", "https:", "ws:", "wss:",
                                                 "http://b.com", "https://b.com",
                                                 "ws://b.com", "wss://b.com"},
                                  "style-src": {"'unsafe-inline'", "data:", "*",
                                                "http:", "https:", "ws:", "wss:"} }),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    combined1 = mergeToStricterContentSecurityPolicy(test["csp1"], test["csp2"]);
    combined2 = mergeToStricterContentSecurityPolicy(test["csp2"], test["csp1"]);
    if combined1 != test["expected"] or combined2 != test["expected"]:
      success = False;
      print("test {0} failed: combining '{1}' with '{2}' resulted in '{3}' but "
            "'{4}' was expected"
            .format(index,test["csp1"],test["csp2"],combined1,test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testIsAllowedByWhitelist():
  print("Running 'testIsAllowedByWhitelist'...");
  test_cases = [
    { "url": 23, "hostSources": [], "schemes": [], "expected": set() },
    { "url": "http:", "hostSources": [], "schemes": [], "expected": set() },
    { "url": "a.com", "hostSources": [23], "schemes": [""], "expected": set() },
    { "url": "", "hostSources": [""], "schemes": [""], "expected": set() },
    { "url": "a.com", "hostSources": [""], "schemes": [""], "expected": set() },
    { "url": "a.com", "hostSources": ["a.com"], "schemes": [], "expected": {"a.com"} },
    { "url": "a.com", "hostSources": ["b.com"], "schemes": [], "expected": set() },
    { "url": "a.com", "hostSources": [], "schemes": ["http:"], "expected": {"http://a.com"} },
    { "url": "a.com", "hostSources": [], "schemes": ["http:", "wss:"],
      "expected": {"http://a.com","wss://a.com"} },
    { "url": "a.com", "hostSources": [], "schemes": ["http:", "https:"],
      "expected": {"http://a.com","https://a.com"} },
    { "url": "http://a.com", "hostSources": [], "schemes": ["http:", "https:"],
      "expected": {"http://a.com"} },
    { "url": "https://a.com", "hostSources": [], "schemes": ["http:", "https:"],
      "expected": {"https://a.com"} },
    { "url": "ws://a.com", "hostSources": [], "schemes": ["http:", "https:"],
      "expected": set() },
    { "url": "http://a.com", "hostSources": ["http://a.com"], "schemes": [],
      "expected": {"http://a.com"} },
    { "url": "https://a.com", "hostSources": ["http://a.com"], "schemes": [],
      "expected": {"https://a.com"} },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    matches = isAllowedByWhitelist(test["url"], test["hostSources"], test["schemes"]);
    if matches != test["expected"]:
      success = False;
      print("test {0} failed: {1} and {2} resulted in {3} but expected was {4}".format(
        index,test["url"],test["whitelist"],matches,test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testToString():
  print("Running 'testToString'...");
  test_cases = [
    { "csp": ContentSecurityPolicy(42), "expected": "" },
    { "csp": ContentSecurityPolicy({ "script-src": {"'unsafe-inline'", "a.com"}, "img-src": {"a.com", "b.com"} }),
      "expected": "script-src a.com 'unsafe-inline'; img-src a.com b.com" },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    csp_str = str(test["csp"]);
    # we call toCSP here because there are no guarantees for a particular order
    # in sets and therefore the resulting string may vary
    if ContentSecurityPolicy.FromString(csp_str) \
         != ContentSecurityPolicy.FromString(test["expected"]):
      success = False;
      print("test {0} failed: '{1}' resulted in '{2}' but '{3}' was expected."
              .format(index,test["csp"],csp_str,test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  test_cases = [
    { "csp1": "", "csp2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": "img-src *", "csp2": "img-src a.com",
      "expected": MergeResult("img-src *", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "img-src http:", "csp2": "img-src a.com",
      "expected": MergeResult("img-src http: a.com", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "default-src 'nonce-FOO'", "csp2": "img-src a.com",
      "expected": MergeResult("img-src a.com", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "script-src 'unsafe-inline' a.com", "csp2": "img-src a.com",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": "script-src 'unsafe-inline' a.com; img-src a.com",
      "csp2": "script-src 'unsafe-inline' a.com; img-src b.com",
      "expected": MergeResult(
                    "script-src 'unsafe-inline' a.com; img-src a.com b.com",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "script-src 'unsafe-inline' a.com",
      "csp2": "default-src 'unsafe-inline' b.com",
      "expected": MergeResult(
                    "script-src 'unsafe-inline' a.com b.com",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "default-src 'unsafe-inline' a.com",
      "csp2": "default-src 'unsafe-inline' b.com",
      "expected": MergeResult(
                    "default-src 'unsafe-inline' a.com b.com",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "block-all-mixed-content", "csp2": "default-src a.com",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": "plugin-types application/x-shockwave-flash",
      "csp2": "default-src a.com",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": "referrer no-referrer", "csp2": "default-src a.com",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": "require-sri-for-script", "csp2": "default-src a.com",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": "sandbox allow-same-origin", "csp2": "default-src a.com",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": "upgrade-insecure-requests", "csp2": "default-src a.com",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "csp1": "default-src 'none'; script-src a.com 'unsafe-inline'",
      "csp2": "default-src 'none'; script-src 'unsafe-inline'",
      "expected": MergeResult(
                    "default-src 'none'; script-src 'unsafe-inline' a.com",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "default-src 'none'; script-src a.com 'unsafe-inline'",
      "csp2": "default-src 'none'",
      "expected": MergeResult(
                    "default-src 'none'; script-src a.com 'unsafe-inline'",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "script-src 'unsafe-inline' data:; img-src a.com",
      "csp2": "script-src 'unsafe-inline' *; img-src b.com",
      "expected": MergeResult(
                    "script-src 'unsafe-inline' * data:; img-src a.com b.com",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "script-src 'none'", "csp2": "script-src 'unsafe-inline' b.com",
      "expected": MergeResult(
                    "script-src 'unsafe-inline' b.com",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "script-src 'unsafe-inline'", "csp2": "script-src 'nonce-FOO' b.com",
      "expected": MergeResult(
                    "script-src 'unsafe-inline' data: * http: https: ws: wss:",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "script-src 'unsafe-inline' 'nonce-FOO'",
      "csp2": "script-src 'nonce-FOO' b.com",
      "expected": MergeResult(
                    "script-src 'unsafe-inline' data: * http: https: ws: wss:",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "script-src 'unsafe-inline'",
      "csp2": "script-src 'strict-dynamic' 'nonce-FOO' b.com",
      "expected": MergeResult(
                    "script-src 'unsafe-eval' 'unsafe-inline' data: * http: "
                    "https: ws: wss:",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "default-src 'unsafe-inline' 'nonce-FOO'",
      "csp2": "script-src 'nonce-FOO' b.com",
      "expected": MergeResult(
                    "script-src 'unsafe-inline' data: * http: https: ws: wss:",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "csp1": "default-src 'none', default-src 'unsafe-inline'",
      "csp2": "default-src 'unsafe-inline'",
      "expected": MergeResult(
                    "default-src 'unsafe-inline'",
                    Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToWeaker(test["csp1"], test["csp2"]);
    result2 = mergeToWeaker(test["csp2"], test["csp1"]);
    csp1 = ContentSecurityPolicy.FromString(result1.value);
    csp2 = ContentSecurityPolicy.FromString(result2.value);
    expected_csp = ContentSecurityPolicy.FromString(test["expected"].value);
    if csp1 != expected_csp or result1.operation != test["expected"].operation \
       or csp2 != expected_csp or result2.operation != test["expected"].operation:
      success = False;
      print("test {0} failed: combining '{1}' with '{2}' resulted in '{3}' but "
            "'{4}' was expected"
            .format(index,test["csp1"],test["csp2"],result1,test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  test_cases = [
    { "csp1": "", "csp2": "",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "csp1": "img-src *", "csp2": "img-src a.com",
      "expected": MergeResult("img-src a.com", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "img-src http:", "csp2": "img-src a.com",
      "expected": MergeResult("img-src http://a.com", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "default-src 'nonce-FOO'", "csp2": "img-src a.com",
      "expected": MergeResult("default-src 'none'; img-src 'none'; "
                              "script-src https: ws: 'unsafe-inline' wss: * data:"
                              " http:; "
                              "style-src https: ws: 'unsafe-inline' wss: * data:"
                              " http:",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "script-src 'unsafe-inline' a.com", "csp2": "img-src a.com",
      "expected": MergeResult("script-src a.com 'unsafe-inline'; img-src a.com",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "script-src 'unsafe-inline' a.com; img-src a.com",
      "csp2": "script-src 'unsafe-inline' a.com; img-src b.com",
      "expected": MergeResult("script-src 'unsafe-inline' a.com; img-src 'none'",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "script-src 'unsafe-inline' a.com",
      "csp2": "default-src 'unsafe-inline' b.com",
      "expected": MergeResult("default-src 'unsafe-inline' b.com; "
                              "script-src 'unsafe-inline'",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "default-src 'unsafe-inline' a.com",
      "csp2": "default-src 'unsafe-inline' b.com",
      "expected": MergeResult("default-src 'unsafe-inline'",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "block-all-mixed-content", "csp2": "default-src a.com",
      "expected": MergeResult("default-src a.com; block-all-mixed-content",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "plugin-types application/x-shockwave-flash",
      "csp2": "default-src a.com",
      "expected": MergeResult("default-src a.com; "
                              "plugin-types application/x-shockwave-flash",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "referrer no-referrer", "csp2": "default-src a.com",
      "expected": MergeResult("default-src a.com; referrer no-referrer",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "require-sri-for-script", "csp2": "default-src a.com",
      "expected": MergeResult("default-src a.com; require-sri-for-script",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "sandbox allow-same-origin", "csp2": "default-src a.com",
      "expected": MergeResult("default-src a.com; sandbox allow-same-origin",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "upgrade-insecure-requests", "csp2": "default-src a.com",
      "expected": MergeResult("default-src a.com; upgrade-insecure-requests",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "default-src 'none'; script-src a.com 'unsafe-inline'",
      "csp2": "default-src 'none'; script-src 'unsafe-inline'",
      "expected": MergeResult("default-src 'none'; script-src 'unsafe-inline'",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "default-src 'none'; script-src a.com 'unsafe-inline'",
      "csp2": "default-src 'none'",
      "expected": MergeResult("default-src 'none'; script-src 'none'",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "script-src 'unsafe-inline' data:; img-src a.com",
      "csp2": "script-src 'unsafe-inline' *; img-src b.com",
      "expected": MergeResult("script-src 'unsafe-inline'; img-src 'none'",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "script-src 'none'", "csp2": "script-src 'unsafe-inline' b.com",
      "expected": MergeResult("script-src 'none'",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "script-src 'unsafe-inline'", "csp2": "script-src 'nonce-FOO' b.com",
      "expected": MergeResult("script-src 'unsafe-inline'",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "script-src 'unsafe-inline'",
      "csp2": "script-src 'strict-dynamic' 'nonce-FOO' b.com",
      "expected": MergeResult("script-src 'unsafe-inline'",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "script-src 'unsafe-inline' 'nonce-FOO'",
      "csp2": "script-src 'nonce-FOO' b.com",
      "expected": MergeResult("script-src 'unsafe-inline' data: *"
                                " http://b.com https://b.com ws://b.com wss://b.com"
                                " http: https: ws: wss:",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "csp1": "default-src 'unsafe-inline' 'nonce-FOO'",
      "csp2": "script-src 'nonce-FOO' b.com",
      "expected": MergeResult("default-src 'unsafe-inline';"
                              "script-src 'unsafe-inline' data: *"
                              " http://b.com https://b.com ws://b.com wss://b.com"
                              " http: https: ws: wss:;"
                              "style-src 'unsafe-inline' * data: http: https: ws:"
                              " wss:",
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToStricter(test["csp1"], test["csp2"]);
    result2 = mergeToStricter(test["csp2"], test["csp1"]);
    csp1 = ContentSecurityPolicy.FromString(result1.value);
    csp2 = ContentSecurityPolicy.FromString(result2.value);
    expected_csp = ContentSecurityPolicy.FromString(test["expected"].value);
    if csp1 != expected_csp or result1.operation != test["expected"].operation \
       or csp2 != expected_csp or result2.operation != test["expected"].operation:
      success = False;
      print("test {0} failed: combining '{1}' with '{2}' resulted in '{3}' but "
            "'{4}' was expected"
            .format(index,test["csp1"],test["csp2"],result1,test["expected"]));
      break;
  print("success." if success else "FAILED.");


###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
