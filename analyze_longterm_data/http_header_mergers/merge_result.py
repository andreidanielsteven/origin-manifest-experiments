from .merge_operation import Ops

class MergeResult:
  def __init__(self,value=None,operation=Ops.OP_NOOP,comment=""):
    self.value = value;
    self.operation = operation if isinstance(operation,Ops) else Ops.OP_NOOP;
    self.comment = comment if isinstance(comment,str) else "";

  def toDict(self):
    result = dict();
    result["value"] = self.value;
    result["operation"] = self.operation;
    result["comment"] = self.comment;
    return result;

  def __eq__(self,other):
    return isinstance(other, MergeResult) \
      and self.value == other.value \
      and self.operation == other.operation;

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def toString(self):
    return str(self.toDict());
