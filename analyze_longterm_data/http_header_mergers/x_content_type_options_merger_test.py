import sys 

from .x_content_type_options_merger import *
from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testXContentTypeOptionsfromString();
  testMergeToWeakerXContentTypeOptions();
  testMergeToStricterXContentTypeOptions();
  testXContentTypeOptionstoString();
  testMergeToWeaker();
  testMergeToStricter();


def testXContentTypeOptionsfromString():
  print("Running 'testXContentTypeOptionsfromString'...");
  test_cases = [
    { "source": "", "expected": XContentTypeOptions() },
    { "source": "asdf", "expected": XContentTypeOptions() },
    { "source": "nosniff", "expected": XContentTypeOptions(True) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    xcto = XContentTypeOptions.FromString(test["source"]);
    if xcto != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' parsed to '{2}' but '{3}' was expected.".format( \
             index, test["source"], xcto, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerXContentTypeOptions():
  print("Running 'testMergeToWeakerXContentTypeOptions'...");
  test_cases = [
    { "policy1": XContentTypeOptions(False), "policy2": XContentTypeOptions(False),
      "expected": MergeResult(XContentTypeOptions(False),
                              Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XContentTypeOptions(True), "policy2": XContentTypeOptions(False),
      "expected": MergeResult(XContentTypeOptions(False),
                              Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XContentTypeOptions(True), "policy2": XContentTypeOptions(True),
      "expected": MergeResult(XContentTypeOptions(True),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    xcto1 = mergeToWeakerXContentTypeOptions(test["policy1"],test["policy2"]);
    xcto2 = mergeToWeakerXContentTypeOptions(test["policy2"],test["policy1"]);
    if xcto1 != test["expected"] or xcto2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], xcto1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterXContentTypeOptions():
  print("Running 'testMergeToStricterXContentTypeOptions'...");
  test_cases = [
    { "policy1": XContentTypeOptions(False), "policy2": XContentTypeOptions(False),
      "expected": MergeResult(XContentTypeOptions(False),
                              Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": XContentTypeOptions(True), "policy2": XContentTypeOptions(False),
      "expected": MergeResult(XContentTypeOptions(True),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XContentTypeOptions(True), "policy2": XContentTypeOptions(True),
      "expected": MergeResult(XContentTypeOptions(True),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    xcto1 = mergeToStricterXContentTypeOptions(test["policy1"],test["policy2"]);
    xcto2 = mergeToStricterXContentTypeOptions(test["policy2"],test["policy1"]);
    if xcto1 != test["expected"] or xcto2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], xcto1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testXContentTypeOptionstoString():
  print("Running 'testXContentTypeOptionstoString'...");
  test_cases = [
    { "source": XContentTypeOptions(), "expected": "" },
    { "source": XContentTypeOptions(True), "expected": "nosniff" },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output_str = test["source"].toString();
    if output_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' transformed to '{2}' but any in '{3}' was "
            "expected.".format(index, test["source"], output_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "asdf", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "nosniff", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "nosniff", "policy2": "asdf",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "nosniff", "policy2": "nosniff",
      "expected": MergeResult("nosniff", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "asdf, nosniff, nosniff", "policy2": "nosniff",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "nosniff, nosniff, nosniff", "policy2": "nosniff",
      "expected": MergeResult("nosniff", Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToWeaker(test["policy1"],test["policy2"]);
    result2 = mergeToWeaker(test["policy1"],test["policy2"]);
    if result1 != test["expected"] or result2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "asdf", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "nosniff", "policy2": "",
      "expected": MergeResult("nosniff", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "nosniff", "policy2": "asdf",
      "expected": MergeResult("nosniff", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "nosniff", "policy2": "nosniff",
      "expected": MergeResult("nosniff", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "asdf, nosniff, nosniff", "policy2": "nosniff",
      "expected": MergeResult("nosniff", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "nosniff, nosniff, nosniff", "policy2": "nosniff",
      "expected": MergeResult("nosniff", Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToStricter(test["policy1"],test["policy2"]);
    result2 = mergeToStricter(test["policy1"],test["policy2"]);
    if result1 != test["expected"] or result2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
