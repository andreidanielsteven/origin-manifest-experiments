import sys 

from .hsts_merger import *
from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testHSTSfromString();
  testMergeToWeakerHSTS();
  testMergeToStricterHSTS();
  testHSTStoString();
  testMergeToWeaker();
  testMergeToStricter();


def testHSTSfromString():
  print("Running 'testHSTSfromString'...");
  test_cases = [
    { "source": "", "expected": HSTS() },
    { "source": "asdf", "expected": HSTS() },
    { "source": "max-age=-123", "expected": HSTS() },
    { "source": "max-age=123; ;;;", "expected": HSTS(123,False,False) },
    { "source": "max-age=123; includeDomains", "expected": HSTS(123,False,False) },
    { "source": "max-age=123; includeSubDomains", "expected": HSTS(123,True,False) },
    { "source": "max-age=123; preload", "expected": HSTS(123,False,True) },
    { "source": "max-age=123; includeSubDomains; preload",
      "expected": HSTS(123,True,True) },
    { "source": "includeSubDomains; max-age=123; preload",
      "expected": HSTS(123,True,True) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    hsts = HSTS.FromString(test["source"]);
    if hsts != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' parsed to '{2}' but '{3}' was expected.".format( \
             index, test["source"], hsts, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerHSTS():
  print("Running 'testMergeToWeakerHSTS'...");
  test_cases = [
    { "policy1": HSTS(), "policy2": HSTS(),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": HSTS(23), "policy2": None,
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": None, "policy2": None,
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": HSTS(23), "policy2": HSTS(-42),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": HSTS(23), "policy2": HSTS(42),
      "expected": MergeResult(HSTS(23), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HSTS(23,True,True), "policy2": HSTS(42,True,True),
      "expected": MergeResult(HSTS(23,True,True), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HSTS(23,True,False), "policy2": HSTS(42,True,True),
      "expected": MergeResult(HSTS(23,True,False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HSTS(23,False,True), "policy2": HSTS(42,True,True),
      "expected": MergeResult(HSTS(23,False,True), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HSTS(23,False,False), "policy2": HSTS(42,True,True),
      "expected": MergeResult(HSTS(23,False,False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HSTS(23,True,False), "policy2": HSTS(42,True,False),
      "expected": MergeResult(HSTS(23,True,False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HSTS(23,False,True), "policy2": HSTS(42,True,False),
      "expected": MergeResult(HSTS(23,False,False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HSTS(23,False,False), "policy2": HSTS(42,True,False),
      "expected": MergeResult(HSTS(23,False,False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HSTS(23,False,True), "policy2": HSTS(42,False,False),
      "expected": MergeResult(HSTS(23,False,False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HSTS(23,False,False), "policy2": HSTS(42,False,False),
      "expected": MergeResult(HSTS(23,False,False), Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    hsts1 = mergeToWeakerHSTS(test["policy1"], test["policy2"]);
    hsts2 = mergeToWeakerHSTS(test["policy2"], test["policy1"]);
    if hsts1 != test["expected"] or hsts2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], hsts1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterHSTS():
  print("Running 'testMergeToStricterHSTS'...");
  test_cases = [
    { "policy1": HSTS(), "policy2": HSTS(),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": HSTS(23), "policy2": None,
      "expected": MergeResult(HSTS(23), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": None, "policy2": None,
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": HSTS(23), "policy2": HSTS(-42),
      "expected": MergeResult(HSTS(23), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23), "policy2": HSTS(42),
      "expected": MergeResult(HSTS(42), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23,True,True), "policy2": HSTS(42,True,True),
      "expected": MergeResult(HSTS(42,True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23,True,False), "policy2": HSTS(42,True,True),
      "expected": MergeResult(HSTS(42,True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23,False,True), "policy2": HSTS(42,True,True),
      "expected": MergeResult(HSTS(42,True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23,False,False), "policy2": HSTS(42,True,True),
      "expected": MergeResult(HSTS(42,True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23,True,False), "policy2": HSTS(42,True,False),
      "expected": MergeResult(HSTS(42,True,False), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23,False,True), "policy2": HSTS(42,True,False),
      "expected": MergeResult(HSTS(42,True,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23,False,False), "policy2": HSTS(42,True,False),
      "expected": MergeResult(HSTS(42,True,False), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23,False,True), "policy2": HSTS(42,False,False),
      "expected": MergeResult(HSTS(42,False,True), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HSTS(23,False,False), "policy2": HSTS(42,False,False),
      "expected": MergeResult(HSTS(42,False,False), Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    hsts1 = mergeToStricterHSTS(test["policy1"], test["policy2"]);
    hsts2 = mergeToStricterHSTS(test["policy2"], test["policy1"]);
    if hsts1 != test["expected"] or hsts2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], hsts1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testHSTStoString():
  print("Running 'testHSTStoString'...");
  test_cases = [
    # max_age=None,includeSubDomains=False,preload=False
    { "policy": HSTS(), "expected": "" },
    { "policy": HSTS(), "expected": "" },
    { "policy": HSTS(42), "expected": "max-age=42" },
    { "policy": HSTS(42, False, False), "expected": "max-age=42" },
    { "policy": HSTS(42, True, False), "expected": "max-age=42; includeSubDomains" },
    { "policy": HSTS(42, False, True), "expected": "max-age=42; preload" },
    { "policy": HSTS(42, True, True), "expected": "max-age=42; includeSubDomains; preload" },
    { "policy": HSTS(None, False, False), "expected": "" },
    { "policy": HSTS(None, True, False), "expected": "includeSubDomains" },
    { "policy": HSTS(None, False, True), "expected": "preload" },
    { "policy": HSTS(None, True, True), "expected": "includeSubDomains; preload" },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result = test["policy"].toString();
    if result != test["expected"]:
      print("test {0} failed: policy '{1}' was expected to result in the string '{2}'."
              .format(index, test["policy"], test["expected"]));
      success = False;
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "max-age=23", "policy2": "",
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "max-age=23; includeSubDomains; preload",
      "policy2": "maxage=42; includeSubDomains; preload",
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "max-age=23", "policy2": "max-age=42",
      "expected": MergeResult("max-age=23", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "max-age=23; includeSubDomains; preload",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=23; includeSubDomains; preload", 
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "max-age=23; includeSubDomains",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=23; includeSubDomains", 
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "max-age=23; preload",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=23; preload", 
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "max-age=23",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=23", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "max-age=23; preload",
      "policy2": "max-age=42; includeSubDomains",
      "expected": MergeResult("max-age=23", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "max-age=23", "policy2": "max-age=42; includeSubDomains",
      "expected": MergeResult("max-age=23", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "max-age=23; preload", "policy2": "max-age=42",
      "expected": MergeResult("max-age=23", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "max-age=23", "policy2": "max-age=42",
      "expected": MergeResult("max-age=23", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "max-age=-23", "policy2": "max-age=42",
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "max-age=23; preload; includeSubDomains",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=23; includeSubDomains; preload", 
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToWeaker(test["policy1"],test["policy2"]);
    result2 = mergeToWeaker(test["policy2"],test["policy1"]);
    if result1 != test["expected"] or result2 != test["expected"]:
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but expected was '{4}'"
              .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      success = False;
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "max-age=23", "policy2": "",
      "expected": MergeResult("max-age=23", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23; includeSubDomains; preload",
      "policy2": "maxage=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=23; includeSubDomains; preload", 
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23", "policy2": "max-age=42",
      "expected": MergeResult("max-age=42", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23; includeSubDomains; preload",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=42; includeSubDomains; preload", 
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23; includeSubDomains",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=42; includeSubDomains; preload", 
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23; preload",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=42; includeSubDomains; preload", 
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=42; includeSubDomains; preload", 
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23; preload",
      "policy2": "max-age=42; includeSubDomains",
      "expected": MergeResult("max-age=42; includeSubDomains; preload", 
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23",
      "policy2": "max-age=42; includeSubDomains",
      "expected": MergeResult("max-age=42; includeSubDomains", 
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23; preload",
      "policy2": "max-age=42",
      "expected": MergeResult("max-age=42; preload", 
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23", "policy2": "max-age=42",
      "expected": MergeResult("max-age=42", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23", "policy2": "max-age=-42",
      "expected": MergeResult("max-age=23", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "max-age=23; preload; includeSubDomains",
      "policy2": "max-age=42; includeSubDomains; preload",
      "expected": MergeResult("max-age=42; includeSubDomains; preload", 
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToStricter(test["policy1"],test["policy2"]);
    result2 = mergeToStricter(test["policy2"],test["policy1"]);
    if result1 != test["expected"] or result2 != test["expected"]:
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but expected was '{4}'"
              .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      success = False;
      break;
  print("success." if success else "FAILED.");


###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
