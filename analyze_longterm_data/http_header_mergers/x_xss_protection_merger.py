import functools
import re 
import sys 

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Functions                                                                   #
###############################################################################

# X-XSS-Protection: 1; mode=block; report=https://a.com/report
class XXSSProtection:
  def __init__(self, enabled=None, mode=None):
    self.enabled = enabled if isinstance(enabled, bool) else None;
    self.mode = mode if isinstance(mode, str) and re.match("^mode=block$", mode,re.I) \
                     else None;

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, XXSSProtection) \
      and self.enabled == other.enabled \
      and self.mode == other.mode;


  def FromString(input_str):
    if not isinstance(input_str, str):
      raise TypeError("'FromString' expects a string.");
    result = XXSSProtection();
    name_value = input_str.strip().split(";");
    if len(name_value) == 0:
      return result;
    elif name_value[0] == "0":
      result.enabled = False;
    elif name_value[0].strip() == "1":
      result.enabled = True;
      if len(name_value) > 1:
        name_value[1] = name_value[1].strip();
        match = re.match("^mode=block$", name_value[1],re.I);
        if match != None:
          result.mode = "mode=block";
    return result;


  def isValidHeader(self):
    return isinstance(self.enabled, bool) \
        and (self.mode == None \
            or (isinstance(self.mode, str) \
                and re.match("^mode=block$", self.mode,re.I)));

  def toString(self):
    return "{0}{1}{2}".format(
               "1" if self.enabled == True else "0" if self.enabled == False else "",
               "; " if self.enabled and self.mode != None else "",
               self.mode if self.enabled != False and self.mode != None else "");

# end class XXSSProtection


def mergeToWeakerXXSSProtection(xxp1, xxp2):
  result = XXSSProtection();

  if xxp1.enabled == False or xxp2.enabled == False:
    result.enabled = False;
  elif xxp1.enabled == None or xxp2.enabled == None:
    return MergeResult(result, Ops.OP_DELETE_MANIFEST_VALUE,
                       "Invalid or empty header");
  elif xxp1.enabled == True and xxp2.enabled == True:
    result.enabled = True;

  if xxp1.mode == "mode=block" and xxp2.mode == "mode=block":
    result.mode = "mode=block";

  if result.isValidHeader():
    return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE, "");
  return MergeResult(result, Ops.OP_DELETE_MANIFEST_VALUE, "Invalid values.");


def mergeToStricterXXSSProtection(xxp1, xxp2):
  result = XXSSProtection();
  if xxp1.enabled == True or xxp2.enabled == True:
    result.enabled = True;
  elif xxp1.enabled == False and xxp2.enabled == False:
    result.enabled = False;
  if result.enabled == True:
    if xxp1.mode == "mode=block" or xxp2.mode == "mode=block":
      result.mode = "mode=block";

  if result.isValidHeader():
    return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER, "");
  return MergeResult(result, Ops.OP_DELETE_RESPONSE_HEADER, "Invalid values.");


def mergeToWeaker(xxp1_str, xxp2_str):
  if not isinstance(xxp1_str, str) or not isinstance(xxp2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  xxp1_list = xxp1_str.split(",");
  if len(xxp1_list) > 0:
    xxp1_str = xxp1_list[0].strip();
  xxp2_list = xxp2_str.split(",");
  if len(xxp2_list) > 0:
    xxp2_str = xxp2_list[0].strip();
  result = mergeToWeakerXXSSProtection(XXSSProtection.FromString(xxp1_str),
                                       XXSSProtection.FromString(xxp2_str));
  result.value = str(result.value);
  return result;


def mergeToStricter(xxp1_str, xxp2_str):
  if not isinstance(xxp1_str, str) or not isinstance(xxp2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  xxp1_list = xxp1_str.split(",");
  if len(xxp1_list) > 0:
    xxp1_str = xxp1_list[0].strip();
  xxp2_list = xxp2_str.split(",");
  if len(xxp2_list) > 0:
    xxp2_str = xxp2_list[0].strip();
  result = mergeToStricterXXSSProtection(XXSSProtection.FromString(xxp1_str),
                                         XXSSProtection.FromString(xxp2_str));
  result.value = str(result.value);
  return result;


def applyToHeader(policy, header):
  return mergeToStricter(policy, header);
