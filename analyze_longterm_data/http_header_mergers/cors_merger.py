import json 
import re 
import sys 

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Functions                                                                   #
###############################################################################

class CORS:
  # As listed on
  # https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#The_HTTP_response_headers
  #
  # Access-Control-Allow-Origin: http://foo.example (mandatory)
  # Access-Control-Expose-Headers: X-My-Custom-Header, X-Another-Custom-Header (optional)
  # Access-Control-Max-Age: 86400 (mandatory)
  # Access-Control-Allow-Credentials: true (optional)
  # Access-Control-Allow-Methods: <method>[, <method>]* (mandatory)
  # Access-Control-Allow-Headers: <field-name>[, <field-name>]* (mandatory)
  def __init__(self, origin=None,creds=None):
    self.origin = origin if isinstance(origin, str) else None;
    self.creds = creds if isinstance(creds, bool) else False;

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, CORS) \
      and self.origin == other.origin \
      and self.creds == other.creds;


  def FromStrings(input_dict):
    if not isinstance(input_dict, dict):
      raise TypeError("'FromStrings' expects a dict of strings.");
    result = CORS();
    for header in input_dict:
      if not isinstance(input_dict[header], str):
        continue;
      header = header.strip();
      value = input_dict[header].strip();
      if re.match("^Access-Control-Allow-Origin$", header, re.I):
        result.origin = value;
      elif re.match("^Access-Control-Allow-Credentials$", header, re.I):
        # Fetch 3.2.4 defines "true" to be case-sensitive
        result.creds = re.match("^true$", value) != None;
    return result;


  def isValidHeaderSet(self):
    # This follows Fetch 4.9 "CORS check"
    return isinstance(self.origin, str) and isinstance(self.creds, bool) \
       and not ("*" in self.origin and self.creds == True);


  def toString(self):
    result = {};
    if isinstance(self.origin, str) and len(self.origin) > 0:
      result["Access-Control-Allow-Origin"] = self.origin;
    if self.creds == True:
      # "true" is the only valid value for this header
      result["Access-Control-Allow-Credentials"] = "true";
    return json.dumps(result);

# end class CORS


def isValidCORS(cors):
  return isinstance(cors, CORS) and cors.isValidHeaderSet();


def mergeToWeakerCORS(cors1, cors2):
  # Not having a CORS response definitely blocks access.
  # So we take what we can get.
  if not isValidCORS(cors1) and not isValidCORS(cors2):
    return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE);
  elif not isValidCORS(cors1) and isValidCORS(cors2):
    return MergeResult(cors2, Ops.OP_USE_AS_MANIFEST_VALUE);
  elif isValidCORS(cors1) and not isValidCORS(cors2):
    return MergeResult(cors1, Ops.OP_USE_AS_MANIFEST_VALUE);

  # This follows Fetch 4.9 "CORS check"
  result = CORS();
  result.origin = cors1.origin if cors1.origin == cors2.origin else "*";
  result.creds = (cors1.creds or cors2.creds) and "*" not in result.origin;

  if result.isValidHeaderSet():
    return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE);
  return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE, "Invalid headers.");


def mergeToStricterCORS(cors1, cors2):
  # If we cannot merge it for sure we do not return a CORS decision.
  # Nothing is more restrictive because no CORS response blocks for sure.
  if not isValidCORS(cors1) or not isValidCORS(cors2):
    return MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE);

  result = CORS();
  # Access-Control-Allow-Origin may be set to the wildcard * but might be unmergable
  # example: how would you strict-merge a.com and b.com? Well, it's not possible
  if cors1.origin == "*" and isinstance(cors2.origin, str):
    result.origin = cors2.origin;
  elif (isinstance(cors1.origin, str) and cors2.origin == "*") \
       or (cors1.origin == cors2.origin):
    result.origin = cors1.origin;
  else:
    return MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE,
                       "Stricter-merging of two origins is not meaningful.");
  # the wildcard and credentials do not work in combination
  result.creds = cors1.creds == True and cors2.creds == True \
                   and isinstance(result.origin, str) and result.origin != "*";

  if result.isValidHeaderSet():
    return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER);
  return MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE, "Invalid headers.");


def mergeToWeaker(cors1_str, cors2_str):
  if not isinstance(cors1_str, str) or not isinstance(cors2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  try:
    list1 = json.loads(cors1_str);
    list2 = json.loads(cors2_str);
  except Exception as e:
    return MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE,
               "Input data invalid because it is not valid JSON: {0}".format(e));
  result = mergeToWeakerCORS(CORS.FromStrings(list1),
                             CORS.FromStrings(list2));
  if isinstance(result.value, CORS):
    result.value = str(result.value);
  return result;


def mergeToStricter(cors1_str, cors2_str):
  if not isinstance(cors1_str, str) or not isinstance(cors2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  try:
    list1 = json.loads(cors1_str);
    list2 = json.loads(cors2_str);
  except Exception as e:
    return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE,
               "Input data invalid because it is not valid JSON: {0}".format(e));
  result = mergeToStricterCORS(CORS.FromStrings(list1),
                               CORS.FromStrings(list2));
  if isinstance(result.value, CORS):
    result.value = str(result.value);
  return result;


def applyToHeader(policy, header):
  return mergeToStricter(policy, header);
