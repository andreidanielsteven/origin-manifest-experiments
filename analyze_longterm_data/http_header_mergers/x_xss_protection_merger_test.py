import sys 

from .x_xss_protection_merger import *
from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testXXSSProtectionfromString();
  testMergeToWeakerXXSSProtection();
  testMergeToStricterXXSSProtection();
  testXXSSProtectionToString();
  testMergeToWeaker();
  testMergeToStricter();


def testXXSSProtectionfromString():
  print("Running 'testXXSSProtectionfromString'...");
  test_cases = [
    { "source": "", "expected": XXSSProtection(None,None) },
    { "source": "asdf", "expected": XXSSProtection(None,None) },
    { "source": "0", "expected": XXSSProtection(False,None) },
    { "source": "1", "expected": XXSSProtection(True) },
    { "source": "1;", "expected": XXSSProtection(True) },
    { "source": "1 asdf", "expected": XXSSProtection(None) },
    { "source": "1; asdf", "expected": XXSSProtection(True,None) },
    { "source": "1; mode", "expected": XXSSProtection(True,None) },
    { "source": "1; mode=asdf", "expected": XXSSProtection(True,None) },
    { "source": "1; mode=block", "expected": XXSSProtection(True,"mode=block") },
    { "source": "0; mode=block", "expected": XXSSProtection(False,None) },
    { "source": "mode=block; 1", "expected": XXSSProtection(None,None) },
    { "source": "1; report", "expected": XXSSProtection(True,None) },
    { "source": "1; report=a.com", "expected": XXSSProtection(True,None) },
    { "source": "0; report=a.com", "expected": XXSSProtection(False,None) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    xfo = XXSSProtection.FromString(test["source"]);
    if xfo != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' parsed to '{2}' but '{3}' was expected.".format( \
             index, test["source"], xfo, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerXXSSProtection():
  print("Running 'testMergeToWeakerXXSSProtection'...");
  test_cases = [
    { "policy1": XXSSProtection(), "policy2": XXSSProtection(),
      "expected": MergeResult(XXSSProtection(), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XXSSProtection(False), "policy2": XXSSProtection(False),
      "expected": MergeResult(XXSSProtection(False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": XXSSProtection(False), "policy2": XXSSProtection(None),
      "expected": MergeResult(XXSSProtection(False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": XXSSProtection(True), "policy2": XXSSProtection(False),
      "expected": MergeResult(XXSSProtection(False), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": XXSSProtection(True), "policy2": XXSSProtection(True),
      "expected": MergeResult(XXSSProtection(True), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": XXSSProtection(True), "policy2": XXSSProtection(None),
      "expected": MergeResult(XXSSProtection(None), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XXSSProtection(True,"mode=block"),
      "policy2": XXSSProtection(True,None),
      "expected": MergeResult(XXSSProtection(True,None),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": XXSSProtection(True,"mode=block"),
      "policy2": XXSSProtection(True,"mode=block"),
      "expected": MergeResult(XXSSProtection(True,"mode=block"),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    xxp1 = mergeToWeakerXXSSProtection(test["policy1"],test["policy2"]);
    xxp2 = mergeToWeakerXXSSProtection(test["policy2"],test["policy1"]);
    if xxp1 != test["expected"] or xxp2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], xxp1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterXXSSProtection():
  print("Running 'testMergeToStricterXXSSProtection'...");
  test_cases = [
    { "policy1": XXSSProtection(), "policy2": XXSSProtection(),
      "expected": MergeResult(XXSSProtection(),
                              Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": XXSSProtection(False), "policy2": XXSSProtection(False),
      "expected": MergeResult(XXSSProtection(False),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XXSSProtection(False), "policy2": XXSSProtection(None),
      "expected": MergeResult(XXSSProtection(None),
                              Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": XXSSProtection(True), "policy2": XXSSProtection(False),
      "expected": MergeResult(XXSSProtection(True),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XXSSProtection(True), "policy2": XXSSProtection(True),
      "expected": MergeResult(XXSSProtection(True),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XXSSProtection(True), "policy2": XXSSProtection(None),
      "expected": MergeResult(XXSSProtection(True),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XXSSProtection(True,"mode=block"),
      "policy2": XXSSProtection(True,None),
      "expected": MergeResult(XXSSProtection(True,"mode=block"),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XXSSProtection(True,"mode=block"),
      "policy2": XXSSProtection(True,"mode=block"),
      "expected": MergeResult(XXSSProtection(True,"mode=block"),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    xxp1 = mergeToStricterXXSSProtection(test["policy1"],test["policy2"]);
    xxp2 = mergeToStricterXXSSProtection(test["policy2"],test["policy1"]);
    if xxp1 != test["expected"] or xxp2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], xxp1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testXXSSProtectionToString():
  print("Running 'testXXSSProtectionToString'...");
  test_cases = [
    { "source": XXSSProtection(), "expected": "" },
    { "source": XXSSProtection(None), "expected": "" },
    { "source": XXSSProtection(False), "expected": "0" },
    { "source": XXSSProtection(True,None), "expected": "1" },
    { "source": XXSSProtection(True,"mode=block"), "expected": "1; mode=block" },
    { "source": XXSSProtection(None,"mode=block"), "expected": "mode=block" },
    { "source": XXSSProtection(False,"mode=block"), "expected": "0" },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output_str = test["source"].toString();
    if output_str != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' transformed to '{2}' but '{3}' was expected."
              .format(index, test["source"], output_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "asdf", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "1", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "1", "policy2": "0",
      "expected": MergeResult("0", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "1", "policy2": "1",
      "expected": MergeResult("1", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "1; mode=block", "policy2": "1",
      "expected": MergeResult("1", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "1; mode=block;", "policy2": "1",
      "expected": MergeResult("1", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "1; mode=block", "policy2": "1; report=a.com",
      "expected": MergeResult("1", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "1; report=a.com", "policy2": "1; report=b.com",
      "expected": MergeResult("1", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "1;mode=block", "policy2": "1; mode=block",
      "expected": MergeResult("1; mode=block", Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToWeaker(test["policy1"],test["policy2"]);
    result2 = mergeToWeaker(test["policy1"],test["policy2"]);
    if result1 != test["expected"] or result2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "asdf", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "1;", "policy2": "",
      "expected": MergeResult("1", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1;", "policy2": "0",
      "expected": MergeResult("1", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1;", "policy2": "1",
      "expected": MergeResult("1", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1; mode=block", "policy2": "1",
      "expected": MergeResult("1; mode=block", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1; mode=block;", "policy2": "1",
      "expected": MergeResult("1; mode=block", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1; mode=block", "policy2": "1; report=a.com",
      "expected": MergeResult("1; mode=block", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1; report=b.com", "policy2": "1; report=a.com",
      "expected": MergeResult("1", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1;mode=block", "policy2": "1; mode=block",
      "expected": MergeResult("1; mode=block", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1; mode=block; report=a.com", "policy2": "1",
      "expected": MergeResult("1; mode=block", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1; mode=block; report=a.com", "policy2": "1; report=a.com",
      "expected": MergeResult("1; mode=block", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "1; report=a.com; mode=block", "policy2": "1",
      "expected": MergeResult("1", Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToStricter(test["policy1"],test["policy2"]);
    result2 = mergeToStricter(test["policy1"],test["policy2"]);
    if result1 != test["expected"] or result2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1,
                    test["expected"]));
      break;
  print("success." if success else "FAILED.");


###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
