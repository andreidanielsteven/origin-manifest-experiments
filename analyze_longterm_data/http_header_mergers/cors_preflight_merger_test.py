import json 
import sys 

from .cors_preflight_merger import *
from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testCORSPreflightfromStrings();
  testMergeToWeakerCORSPreflight();
  testMergeToStricterCORSPreflight();
  testCORSPreflighttoString();
  testMergeToWeaker();
  testMergeToStricter();


def testCORSPreflightfromStrings():
  print("Running 'testCORSPreflightfromStrings'...");
  test_cases = [
    { "source": 23, "expected": "'FromStrings' expects a dict of strings." },
    { "source": dict(), "expected": CORSPreflight() },
    { "source": { "Not-A-CORS-Header": "foobar" }, "expected": CORSPreflight() },
    # Access-Control-Allow-Origin: http://foo.example
    { "source": {"Access-Control-Allow-Origin": "http://foo.example"},
      "expected": CORSPreflight("http://foo.example") },
    { "source": {"Access-Control-Allow-Origin": "xyz"}, "expected": CORSPreflight("xyz") },
    { "source": {"Access-Control-Allow-Origin": "  asdf   "}, "expected": CORSPreflight("asdf") },
    # Access-Control-Expose-Headers: X-My-Custom-Header, X-Another-Custom-Header
    { "source": {"Access-Control-Expose-Headers": "HEADER1, HEADER2"},
      "expected": CORSPreflight(None, {"HEADER1", "HEADER2"}) },
    { "source": {"Access-Control-Expose-Headers": "HEADER2, HEADER1"},
      "expected": CORSPreflight(None, {"HEADER1", "HEADER2"}) },
    { "source": {"Access-Control-Expose-Headers": "HEADER1, HEADER1"},
      "expected": CORSPreflight(None, {"HEADER1"}) },
    { "source": {"Access-Control-Expose-Headers": "   HEADER1   , HEADER2  "},
      "expected": CORSPreflight(None, {"HEADER1", "HEADER2"}) },
    { "source": {"Access-Control-Expose-Headers": "" },
      "expected": CORSPreflight(None, set()) },
    # Access-Control-Max-Age: 86400
    { "source": {"Access-Control-Max-Age": "42"}, "expected": CORSPreflight(None, None, 42) },
    { "source": {"Access-Control-Max-Age": "42.23"}, "expected": CORSPreflight(None, None, None) },
    { "source": {"Access-Control-Max-Age": "asdf"}, "expected": CORSPreflight(None, None, None) },
    { "source": {"Access-Control-Max-Age": ""}, "expected": CORSPreflight(None, None, None) },
    # Access-Control-Allow-Credentials: true
    { "source": {"Access-Control-Allow-Credentials": "true"},
      "expected": CORSPreflight(None, None, None, True) },
    { "source": {"Access-Control-Allow-Credentials": "TrUe"},
      "expected": CORSPreflight(None, None, None, False) },
    { "source": {"Access-Control-Allow-Credentials": "   true    "},
      "expected": CORSPreflight(None, None, None, True) },
    { "source": {"Access-Control-Allow-Credentials": "asdf"},
      "expected": CORSPreflight(None, None, None, False) },
    { "source": {"Access-Control-Allow-Credentials": ""},
      "expected": CORSPreflight(None, None, None, False) },
    # Access-Control-Allow-Methods: <method>[, <method>]*
    { "source": {"Access-Control-Allow-Methods": "M1, M2"},
      "expected": CORSPreflight(None, None, None, False, {"M1", "M2"}) },
    { "source": {"Access-Control-Allow-Methods": "M2, M1"},
      "expected": CORSPreflight(None, None, None, False, {"M1", "M2"}) },
    { "source": {"Access-Control-Allow-Methods": "  M1  ,    M2   "},
      "expected": CORSPreflight(None, None, None, False, {"M1", "M2"}) },
    { "source": {"Access-Control-Allow-Methods": ""},
      "expected": CORSPreflight(None, None, None, False, set()) },
    # Access-Control-Allow-Headers: <field-name>[, <field-name>]*
    { "source": {"Access-Control-Allow-Headers": "H1, H2"},
      "expected": CORSPreflight(None, None, None, False, None, {"H1","H2"}) },
    { "source": {"Access-Control-Allow-Headers": "H2, H1"},
      "expected": CORSPreflight(None, None, None, False, None, {"H1","H2"}) },
    { "source": {"Access-Control-Allow-Headers": "   H1   ,   H2    "},
      "expected": CORSPreflight(None, None, None, False, None, {"H1","H2"}) },
    { "source": {"Access-Control-Allow-Headers": ""},
      "expected": CORSPreflight(None, None, None, False, None, set()) },
    # Everything together
    { "source": {"Access-Control-Allow-Origin": "https://a.com",
                 "Access-Control-Expose-Headers": "H1, H2",
                 "Access-Control-Max-Age": "42",
                 "Access-Control-Allow-Credentials": "true",
                 "Access-Control-Allow-Methods": "M1, M2",
                 "Access-Control-Allow-Headers": "H3, H4"},
      "expected": CORSPreflight("https://a.com", {"H1","H2"}, 42, True,
                       {"M1","M2"}, {"H3","H4"}) },
    { "source": {"Access-Control-Allow-Credentials": "true",
                 "Access-Control-Allow-Headers": "H3, H4",
                 "Access-Control-Allow-Origin": "https://a.com",
                 "Access-Control-Allow-Methods": "M1, M2",
                 "Access-Control-Max-Age": "42",
                 "Access-Control-Expose-Headers": "H1, H2"},
      "expected": CORSPreflight("https://a.com", {"H1","H2"}, 42, True,
                       {"M1","M2"}, {"H3","H4"}) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    cors = None;
    try:
      cors = CORSPreflight.FromStrings(test["source"]);
    except Exception as e:
      cors = str(e);
    if cors != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' parsed to '{2}' but '{3}' was expected.".format( \
             index, test["source"], cors, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerCORSPreflight():
  print("Running 'testMergeToWeakerCORS'...");
  test_cases = [
    { "policy1": CORSPreflight(), "policy2": CORSPreflight(),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": CORSPreflight(),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight(1234,set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),17,False,{"M2"},{"H2"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),17,False,{"M2"},{"H2"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://b.com",None,23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://b.com",None,23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",None,23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",{"H1","H2"},23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H2","H3"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",{"H1","H2","H3"},23,False,
                                   {"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H2"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",{"H1","H2"},23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),None,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://b.com",set(),None,False,{"M2"},{"H2"}),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),None,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://b.com",set(),42,False,{"M2"},{"H2"}),
      "expected": MergeResult(CORSPreflight("https://b.com",set(),42,False,{"M2"},{"H2"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),42,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,None,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,None,{"H1"}),
      "policy2": CORSPreflight("https://b.com",set(),42,False,None,{"H2"}),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,None,{"H1"}),
      "policy2": CORSPreflight("https://b.com",set(),42,False,{"M2"},{"H2"}),
      "expected": MergeResult(CORSPreflight("https://b.com",set(),42,False,{"M2"},{"H2"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M2"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1","M2"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1","M2"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M2","M3"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,
                                   {"M1","M2","M3"},{"H1"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},None),
      "policy2": CORSPreflight("https://b.com",set(),42,False,{"M2"},None),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},None),
      "policy2": CORSPreflight("https://b.com",set(),42,False,{"M2"},{"H2"}),
      "expected": MergeResult(CORSPreflight("https://b.com",set(),42,False,{"M2"},{"H2"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H2"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1","H2"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1","H2"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H2","H3"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},
                                   {"H1","H2","H3"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": CORSPreflight("https://a.com",{"H1"},22,False,{"M1"},{"H3","H4"}),
      "policy2": CORSPreflight("https://b.com",{"H1","H2"},42,True,{"M1","M2"},{"H3","H4"}),
      "expected": MergeResult(CORSPreflight("*",{"H1","H2"},22,False,{"M1","M2"},{"H3","H4"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    cors1 = mergeToWeakerCORSPreflight(test["policy1"],test["policy2"]);
    cors2 = mergeToWeakerCORSPreflight(test["policy2"],test["policy1"]);
    if cors1 != test["expected"] or cors2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], cors1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterCORSPreflight():
  print("Running 'testMergeToStricterCORSPreflight'...");
  test_cases = [
    { "policy1": CORSPreflight(), "policy2": CORSPreflight(),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight(),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight(1234,set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),17,False,{"M2"},{"H2"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://b.com",None,23,False,{"M1"},{"H1"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://b.com",None,23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://b.com",None,23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("*",None,23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",None,23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",None,23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",{"H1","H2"},23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H2","H3"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",{"H2"},23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",{"H1"},23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",{"H2"},23,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",set(),None,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://b.com",set(),None,False,{"M2"},{"H2"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("https://a.com",set(),None,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://b.com",set(),42,False,{"M2"},{"H2"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),42,False,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),42,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",set(),23,None,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,True,{"M1"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,None,{"H1"}),
      "policy2": CORSPreflight("https://b.com",set(),42,False,None,{"H2"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,None,{"H1"}),
      "policy2": CORSPreflight("https://b.com",set(),42,False,{"M2"},{"H2"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M2"},{"H1"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1","M2"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M2","M3"},{"H1"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M2"},{"H1"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},None),
      "policy2": CORSPreflight("https://b.com",set(),42,False,{"M2"},None),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},None),
      "policy2": CORSPreflight("https://b.com",set(),42,False,{"M2"},{"H2"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H2"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
    { "policy1": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H1","H2"}),
      "policy2": CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H2","H3"}),
      "expected": MergeResult(CORSPreflight("https://a.com",set(),23,False,{"M1"},{"H2"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": CORSPreflight("https://a.com",{"H1"},22,False,{"M1"},{"H3","H4"}),
      "policy2": CORSPreflight("https://b.com",{"H1","H2"},42,True,{"M1","M2"},{"H3","H4"}),
      "expected": MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    cors1 = mergeToStricterCORSPreflight(test["policy1"],test["policy2"]);
    cors2 = mergeToStricterCORSPreflight(test["policy2"],test["policy1"]);
    if cors1 != test["expected"] or cors2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], cors1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testCORSPreflighttoString():
  print("Running 'testCORSPreflighttoString'...");
  test_cases = [
    { "source": CORSPreflight(), "expected": "{}" },
    { "source": CORSPreflight("https://b.com",{"H1"},42,True,{"M2","M3"},{"H4"}),
      "expected": [json.dumps({"Access-Control-Allow-Origin": "https://b.com",
                               "Access-Control-Expose-Headers": "H1",
                               "Access-Control-Max-Age": "42",
                               "Access-Control-Allow-Credentials": "true",
                               "Access-Control-Allow-Methods": "M2, M3",
                               "Access-Control-Allow-Headers": "H4"}),
                   json.dumps({"Access-Control-Allow-Origin": "https://b.com",
                               "Access-Control-Expose-Headers": "H1",
                               "Access-Control-Max-Age": "42",
                               "Access-Control-Allow-Credentials": "true",
                               "Access-Control-Allow-Methods": "M3, M2",
                               "Access-Control-Allow-Headers": "H4"})] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output_str = test["source"].toString();
    if output_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' transformed to '{2}' but any in '{3}' was "
            "expected.".format(index, test["source"], output_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  print("TODO");
  test_cases = [
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output1_str = mergeToWeaker(test["policy1"],test["policy2"]);
    output2_str = mergeToWeaker(test["policy1"],test["policy2"]);
    if output1_str not in test["expected"] or output2_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], output1_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  print("TODO");
  test_cases = [
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output1_str = mergeToStricter(test["policy1"],test["policy2"]);
    output2_str = mergeToStricter(test["policy1"],test["policy2"]);
    if output1_str not in test["expected"] or output2_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], output1_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
