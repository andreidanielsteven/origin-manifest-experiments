import sys 

from .hpkp_merger import *
from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testHSTSfromString();
  testMergeToWeakerHPKP();
  testMergeToStricterHPKP();
  testHPKPtoString();
  testMergeToWeaker();
  testMergeToStricter();


def testHSTSfromString():
  print("Running 'testHSTSfromString'...");
  test_cases = [
    { "source": "", "expected": HPKP() },
    { "source": "", "expected": HPKP(set(),None,False) },
    { "source": "asdf=42", "expected": HPKP() },
    { "source": "pin-sha256=\"FOO\" max-age=23", "expected": HPKP() },
    { "source": "pin-sha256=\"FIRST=\"; pin-sha256=\"SECOND\";" 
                "max-age=23; includeSubDomains; report-uri=\"repo.rt/uri\"",
      "expected": HPKP({"FIRST=", "SECOND"}, 23, True) },
    { "source": "pin-sha256=\"FIRST=\"; pin-sha256=\"SECOND\"; max-age=23;",
      "expected": HPKP({"SECOND", "FIRST="}, 23) },
    { "source": "pin-sha256=\"SECOND\"; max-age=23; includeSubDomains; "
                "pin-sha256=\"FIRST=\";",
      "expected": HPKP({"FIRST=", "SECOND"}, 23, True) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    hpkp = HPKP.FromString(test["source"]);
    if hpkp != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' parsed to '{2}' but '{3}' was expected.".format( \
             index, test["source"], hpkp, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerHPKP():
  print("Running 'testMergeToWeakerHPKP'...");
  test_cases = [
    { "policy1": None, "policy2": None,
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": HPKP(), "policy2": HPKP(),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": HPKP(), "policy2": HPKP({"pin1"}),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": None, "policy2": HPKP({"pin1"},23),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": HPKP(), "policy2": HPKP({"pin1"},23),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": HPKP({"pin1"}), "policy2": HPKP({"pin2"}),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": HPKP({"pin1"},23), "policy2": HPKP({"pin1"},42),
      "expected": MergeResult(HPKP({"pin1"},23), Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HPKP({"pin1"}, 23), "policy2": HPKP({"pin2"}, 23),
      "expected": MergeResult(HPKP({"pin1","pin2"},23),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HPKP({"pin1","pin2"}, 23), "policy2": HPKP({"pin2","pin3"}, 23),
      "expected": MergeResult(HPKP({"pin1","pin2","pin3"},23),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HPKP({"pin1","pin2"},23), "policy2": HPKP({"pin1"},23),
      "expected": MergeResult(HPKP({"pin1","pin2"},23),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HPKP({"pin1"},23,False), "policy2": HPKP({"pin1"},23,False),
      "expected": MergeResult(HPKP({"pin1"},23,False),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HPKP({"pin1"},23,True), "policy2": HPKP({"pin1"},23,False),
      "expected": MergeResult(HPKP({"pin1"},23,False),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": HPKP({"pin1"},23,True), "policy2": HPKP({"pin1"},23,True),
      "expected": MergeResult(HPKP({"pin1"},23,True),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    hpkp1 = mergeToWeakerHPKP(test["policy1"],test["policy2"]);
    hpkp2 = mergeToWeakerHPKP(test["policy2"],test["policy1"]);
    if hpkp1 != test["expected"] or hpkp2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], hpkp1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterHPKP():
  print("Running 'testMergeToStricterHPKP'...");
  test_cases = [
    { "policy1": None, "policy2": None,
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": HPKP(), "policy2": HPKP(),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": HPKP(), "policy2": HPKP({"pin1"}),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": None, "policy2": HPKP({"pin1"},23),
      "expected": MergeResult(HPKP({"pin1"},23), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HPKP(), "policy2": HPKP({"pin1"},23),
      "expected": MergeResult(HPKP({"pin1"},23), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HPKP({"pin1"}), "policy2": HPKP({"pin2"}),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": HPKP({"pin1"},23), "policy2": HPKP({"pin1"},42),
      "expected": MergeResult(HPKP({"pin1"},42), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HPKP({"pin1"}, 23), "policy2": HPKP({"pin2"}, 23),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": HPKP({"pin1","pin2"}, 23), "policy2": HPKP({"pin2","pin3"}, 23),
      "expected": MergeResult(HPKP({"pin2"}, 23),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HPKP({"pin1","pin2"},23), "policy2": HPKP({"pin1"},23),
      "expected": MergeResult(HPKP({"pin1"},23), Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HPKP({"pin1"},23,False), "policy2": HPKP({"pin1"},23,False),
      "expected": MergeResult(HPKP({"pin1"},23,False),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HPKP({"pin1"},23,True), "policy2": HPKP({"pin1"},23,False),
      "expected": MergeResult(HPKP({"pin1"},23,True),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": HPKP({"pin1"},23,True), "policy2": HPKP({"pin1"},23,True),
      "expected": MergeResult(HPKP({"pin1"},23,True),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    hpkp1 = mergeToStricterHPKP(test["policy1"],test["policy2"]);
    hpkp2 = mergeToStricterHPKP(test["policy2"],test["policy1"]);
    if hpkp1 != test["expected"] or hpkp2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], hpkp1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testHPKPtoString():
  print("Running 'testHSTStoString'...");
  test_cases = [
    { "source": HPKP(), "expected": "" },
    { "source": HPKP(set(),0,False), "expected": "max-age=0" },
    { "source": HPKP(set(),23,True),
      "expected": ["max-age=23; includeSubDomains","includeSubDomains; max-age=23"] },
    { "source": HPKP(set(),23,False), "expected": "max-age=23" },
    { "source": HPKP({"VALUE"}, 23, True),
      "expected": ["pin-sha256=\"VALUE\"; max-age=23; includeSubDomains",
                   "pin-sha256=\"VALUE\"; includeSubDomains; max-age=23"
                   "includeSubDomains; max-age=23; pin-sha256=\"VALUE\""
                   "includeSubDomains; pin-sha256=\"VALUE\"; max-age=23"
                   "max-age=23; pin-sha256=\"VALUE\"; includeSubDomains"
                   "max-age=23; includeSubDomains; pin-sha256=\"VALUE\""] },
    { "source": HPKP({"SECOND", "FIRST"}, 23),
      "expected": ["pin-sha256=\"FIRST\"; pin-sha256=\"SECOND\"; max-age=23",
                 "pin-sha256=\"SECOND\"; pin-sha256=\"FIRST\"; max-age=23"] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output_str = test["source"].toString();
    if output_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' transformed to '{2}' but any in '{3}' was "
            "expected.".format(index, test["source"], output_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": [MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE)] },
    { "policy1": "", "policy2": "pin-sha256=\"pin1\"",
      "expected": [MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE)] },
    { "policy1": "", "policy2": "pin-sha256=\"pin1\"; max-age=23",
      "expected": [MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE)] },
    { "policy1": "pin-sha256=\"pin1\"", "policy2": "pin-sha256=\"pin1\"",
      "expected": [MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE)] },
    { "policy1": "pin-sha256=\"pin1\"; max-age=23",
      "policy2": "pin-sha256=\"pin1\"; max-age=42",
      "expected": [MergeResult("pin-sha256=\"pin1\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "pin-sha256=\"pin1\"; max-age=23",
      "policy2": "pin-sha256=\"pin2\"; max-age=23",
      "expected": [MergeResult("pin-sha256=\"pin1\"; pin-sha256=\"pin2\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("pin-sha256=\"pin2\"; pin-sha256=\"pin1\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "pin-sha256=\"pin1\"; pin-sha256=\"pin2\"; max-age=23",
      "policy2": "pin-sha256=\"pin2\"; pin-sha256=\"pin3\"; max-age=23",
      "expected": [MergeResult("pin-sha256=\"pin1\"; pin-sha256=\"pin2\"; "
                               "pin-sha256=\"pin3\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("pin-sha256=\"pin1\"; pin-sha256=\"pin3\"; "
                               "pin-sha256=\"pin2\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("pin-sha256=\"pin2\"; pin-sha256=\"pin1\"; "
                               "pin-sha256=\"pin3\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("pin-sha256=\"pin2\"; pin-sha256=\"pin3\"; "
                               "pin-sha256=\"pin1\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("pin-sha256=\"pin3\"; pin-sha256=\"pin1\"; "
                               "pin-sha256=\"pin2\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("pin-sha256=\"pin3\"; pin-sha256=\"pin2\"; "
                               "pin-sha256=\"pin1\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "pin-sha256=\"pin1\"; pin-sha256=\"pin2\"; max-age=23",
      "policy2": "pin-sha256=\"pin1\"; max-age=23",
      "expected": [MergeResult("pin-sha256=\"pin1\"; pin-sha256=\"pin2\"; "
                               "max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("pin-sha256=\"pin2\"; pin-sha256=\"pin1\"; "
                               "max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "pin-sha256=\"pin1\"; max-age=23; includeSubDomains",
      "policy2": "pin-sha256=\"pin1\"; max-age=23",
      "expected": [MergeResult("pin-sha256=\"pin1\"; max-age=23",
                               Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "pin-sha256=\"pin1\"; max-age=23; includeSubDomains",
      "policy2": "pin-sha256=\"pin1\"; max-age=23; includeSubDomains",
      "expected": [MergeResult("pin-sha256=\"pin1\"; max-age=23; includeSubDomains",
                               Ops.OP_USE_AS_MANIFEST_VALUE)] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToWeaker(test["policy1"],test["policy2"]);
    result2 = mergeToWeaker(test["policy2"],test["policy1"]);
    if result1 not in test["expected"] or result2 not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but expected was '{4}'."
              .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": [MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER)] },
    { "policy1": "", "policy2": "pin-sha256=\"pin1\"",
      "expected": [MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER)] },
    { "policy1": "", "policy2": "pin-sha256=\"pin1\"; max-age=23",
      "expected": [MergeResult("pin-sha256=\"pin1\"; max-age=23",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "pin-sha256=\"pin1\"", "policy2": "pin-sha256=\"pin1\"",
      "expected": [MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER)] },
    { "policy1": "pin-sha256=\"pin1\"; max-age=23",
      "policy2": "pin-sha256=\"pin1\"; max-age=42",
      "expected": [MergeResult("pin-sha256=\"pin1\"; max-age=42",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "pin-sha256=\"pin1\"; max-age=23",
      "policy2": "pin-sha256=\"pin2\"; max-age=23",
      "expected": [MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER)] },
    { "policy1": "pin-sha256=\"pin1\"; pin-sha256=\"pin2\"; max-age=23",
      "policy2": "pin-sha256=\"pin2\"; pin-sha256=\"pin3\"; max-age=23",
      "expected": [MergeResult("pin-sha256=\"pin2\"; max-age=23",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "pin-sha256=\"pin1\"; pin-sha256=\"pin2\"; max-age=23",
      "policy2": "pin-sha256=\"pin1\"; max-age=23",
      "expected": [MergeResult("pin-sha256=\"pin1\"; max-age=23",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "pin-sha256=\"pin1\"; max-age=23; includeSubDomains",
      "policy2": "pin-sha256=\"pin1\"; max-age=23",
      "expected": [MergeResult("pin-sha256=\"pin1\"; max-age=23; includeSubDomains",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "pin-sha256=\"pin1\"; max-age=23; includeSubDomains",
      "policy2": "pin-sha256=\"pin1\"; max-age=23; includeSubDomains",
      "expected": [MergeResult("pin-sha256=\"pin1\"; max-age=23; includeSubDomains",
                               Ops.OP_USE_IN_RESPONSE_HEADER)] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToStricter(test["policy1"],test["policy2"]);
    result2 = mergeToStricter(test["policy2"],test["policy1"]);
    if result1 not in test["expected"] or result2 not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but expected was '{4}'."
              .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
