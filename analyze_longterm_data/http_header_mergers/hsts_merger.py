import functools
import re 
import sys 

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Functions                                                                   #
###############################################################################

class HSTS:
  # Strict-Transport-Security: max-age=123; includeSubDomains; preload
  def __init__(self,max_age=None, includeSubDomains=False, preload=False):
    self.max_age = max_age;
    self.includeSubDomains = includeSubDomains;
    self.preload = preload;

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, HSTS) \
           and self.max_age == other.max_age \
           and self.includeSubDomains == other.includeSubDomains \
           and self.preload == other.preload;

  # This string parsing is inspired by
  # https://tools.ietf.org/html/rfc6797#section-6.1
  def FromString(input_str):
    if not isinstance(input_str, str):
      raise TypeError("'FromString' expects a string.");
    result = HSTS();
    for directive in map(str.strip, input_str.split(";")):
      match = re.match("^max-age=(.+)$",directive,re.I);
      if match != None and len(match.groups()) > 0:
        value = match.group(1).strip('"');
        if value.isdigit():
          result.max_age = int(value);
        continue;
      match = re.match("^includesubdomains$",directive,re.I);
      if match != None:
        result.includeSubDomains = True;
        continue;
      match = re.match("^preload$",directive,re.I);
      if match != None:
        result.preload = True;
        continue;
    return result;

  def isValidHeader(self):
    return self.max_age != None \
        and isinstance(self.max_age, int) and self.max_age >= 0;

  def toString(self):
    max_age_str = "max-age={0}".format(self.max_age) \
                      if self.isValidHeader() else "";
    incl_subd_str = "includeSubDomains" if self.includeSubDomains == True else "";
    preload_str = "preload" if self.preload == True else "";
    return "; ".join([x for x in [max_age_str, incl_subd_str, preload_str] if x != ""]);
# end class HSTS


def isValidHSTS(hsts):
  return isinstance(hsts, HSTS) and hsts.isValidHeader();


def mergeToWeakerHSTS(hsts1, hsts2):
  if not isValidHSTS(hsts1) or not isValidHSTS(hsts2):
    return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE);

  result = HSTS();
  result.max_age = min(hsts1.max_age, hsts2.max_age);
  result.includeSubDomains = hsts1.includeSubDomains and hsts2.includeSubDomains;
  result.preload = hsts1.preload and hsts2.preload;

  if result.isValidHeader():
    return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE);
  return MergeResult(result, Ops.OP_DELETE_MANIFEST_VALUE);


def mergeToStricterHSTS(hsts1, hsts2):
  if not isValidHSTS(hsts1) and isValidHSTS(hsts2):
    return MergeResult(hsts2, Ops.OP_USE_IN_RESPONSE_HEADER);
  elif isValidHSTS(hsts1) and not isValidHSTS(hsts2):
    return MergeResult(hsts1, Ops.OP_USE_IN_RESPONSE_HEADER);
  elif not isValidHSTS(hsts1) and not isValidHSTS(hsts2):
    return MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER);

  result = HSTS();
  result.max_age = max(hsts1.max_age, hsts2.max_age);
  result.includeSubDomains = hsts1.includeSubDomains or hsts2.includeSubDomains;
  result.preload = hsts1.preload or hsts2.preload;

  if result.isValidHeader():
    return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER);
  return MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER);


def mergeToWeaker(hsts1_str, hsts2_str):
  if not isinstance(hsts1_str, str) or not isinstance(hsts2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  hsts1_list = hsts1_str.split(",");
  if len(hsts1_list) > 0:
    hsts1_str = hsts1_list[0].strip();
  hsts2_list = hsts2_str.split(",");
  if len(hsts2_list) > 0:
    hsts2_str = hsts2_list[0].strip();
  result = mergeToWeakerHSTS(HSTS.FromString(hsts1_str),
                             HSTS.FromString(hsts2_str));
  if isinstance(result.value, HSTS):
    result.value = str(result.value);
  return result;


def mergeToStricter(hsts1_str, hsts2_str):
  if not isinstance(hsts1_str, str) or not isinstance(hsts2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  hsts1_list = hsts1_str.split(",");
  if len(hsts1_list) > 0:
    hsts1_str = hsts1_list[0].strip();
  hsts2_list = hsts2_str.split(",");
  if len(hsts2_list) > 0:
    hsts2_str = hsts2_list[0].strip();
  result = mergeToStricterHSTS(HSTS.FromString(hsts1_str),
                               HSTS.FromString(hsts2_str));
  if isinstance(result.value, HSTS):
    result.value = str(result.value);
  return result;


def applyToHeader(policy, header):
  return mergeToStricter(policy, header);
