## How to use it
### From the command line
```bash
python3 X_merger.py [--loose|--strict] "string1" "string2" # to merge the to header values, --strict might return "TODO"
python3 X_merger.py -h # for help
python3 X_merger.py -t # for running tests
```

Almost all mergers take the header values as `string1` and `string2` respectively.
The only exception is cors\_merger.py which expects the strings as a JSON formatted 
list of strings. These strings themselves are expected to be of format
`'CORS-header-name: value'`. Only the 6 CORS response headers as documented on 
https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#The\_HTTP\_response\_headers
are allowed and processed.

### Through imports
Every X\_merger.py has a `mergeToWeaker(str,str) : str` and sometimes also a `mergeToStricter(str,str) : str` function.
Both expect two header values and return a merged header value.

## Python 2 vs Python 3
All files expect Python 3 and will never go below.

## Implementation details
### Header classes
All X\_merger.py files contain a class representing the respective header X. These classes implement
* `__str__`, `__repr__`, `__eq__`
* a `FromString` constructor function which returns a new instance of the class
* `mergeToWeakerX(x1,x2)` which returns a new instance of the class based on x1, and x2
* `mergeToStricterX(x1,x2)` is implemented in the same fashion with the obvious differences

### mergeToXZY functions
In each X\_merger.py there are also
* `mergeToWeaker(str1,str2)` which takes two strings as received in HTTP response headers and returns a string with a merged header X value
* `mergeToStricter(str1,str2)` which is implemented similar to the above with the obvious differences

### Testing
There are also tests which I invite you to review and to extend if you feel something is missing.
