import json 
import re 
import sys 

from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();

UNMERGABLE = None;


###############################################################################
# Functions                                                                   #
###############################################################################

class CORSPreflight:
  # As listed on
  # https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#The_HTTP_response_headers
  #
  # Access-Control-Allow-Origin: http://foo.example (mandatory)
  # Access-Control-Expose-Headers: X-My-Custom-Header, X-Another-Custom-Header (optional)
  # Access-Control-Max-Age: 86400 (mandatory)
  # Access-Control-Allow-Credentials: true (optional)
  # Access-Control-Allow-Methods: <method>[, <method>]* (mandatory)
  # Access-Control-Allow-Headers: <field-name>[, <field-name>]* (mandatory)
  def __init__(self, origin=None,expose=None,maxage=None,creds=None,methods=None,headers=None):
    self.origin = origin if isinstance(origin, str) else None;
    self.expose = expose if isinstance(expose, set) else set();
    self.maxage = maxage if isinstance(maxage, int) else None;
    self.creds = creds if isinstance(creds, bool) else False;
    self.methods = methods if isinstance(methods, set) else set();
    self.headers = headers if isinstance(headers, set) else set();

  def __str__(self):
    return self.toString();

  def __repr__(self):
    return self.toString();

  def __eq__(self,other):
    return isinstance(other, CORSPreflight) \
      and self.origin == other.origin \
      and self.expose == other.expose \
      and self.maxage == other.maxage \
      and self.creds == other.creds \
      and self.methods == other.methods \
      and self.headers == other.headers;


  def FromStrings(input_dict):
    if not isinstance(input_dict, dict):
      raise TypeError("'FromStrings' expects a dict of strings.");
    result = CORSPreflight();
    for header in input_dict:
      if not isinstance(input_dict[header], str):
        continue;
      header = header.strip();
      value = input_dict[header].strip();
      if re.match("^Access-Control-Allow-Origin$", header, re.I):
        result.origin = value;
      elif re.match("^Access-Control-Expose-Headers$", header, re.I):
        result.expose = set([e.strip() for e in value.split(",") if e.strip() != ""]);
      elif re.match("^Access-Control-Max-Age$", header, re.I):
        if value.isdigit():
          result.maxage = int(value);
      elif re.match("^Access-Control-Allow-Credentials$", header, re.I):
        # Fetch 3.2.4 defines "true" to be case-sensitive
        result.creds = re.match("^true$", value) != None;
      elif re.match("^Access-Control-Allow-Methods$", header, re.I):
        result.methods = set([m.strip() for m in value.split(",") if m.strip() != ""]);
      elif re.match("^Access-Control-Allow-Headers$", header, re.I):
        result.headers = set([h.strip() for h in value.split(",") if h.strip() != ""]);
    return result;


  def isValidHeaderSet(self):
    return isinstance(self.origin, str) and isinstance(self.maxage, int) \
       and isinstance(self.creds, bool) and len(self.methods) > 0 and len(self.headers) > 0 \
       and not (len(self.methods) == 1 and "*" in self.methods and self.creds) \
       and not (len(self.headers) == 1 and "*" in self.headers and self.creds);


  def toString(self):
    result = {};
    if isinstance(self.origin, str) and len(self.origin) > 0:
      result["Access-Control-Allow-Origin"] = self.origin;
    if len(self.expose) > 0:
       result["Access-Control-Expose-Headers"] = ", ".join(self.expose);
    if isinstance(self.maxage, int):
      result["Access-Control-Max-Age"] = str(self.maxage);
    if self.creds == True:
      # "true" is the only valid value for this header
      result["Access-Control-Allow-Credentials"] = "true";
    if len(self.methods) > 0:
       result["Access-Control-Allow-Methods"] =", ".join(self.methods);
    if len(self.headers) > 0:
       result["Access-Control-Allow-Headers"] = ", ".join(self.headers);
    return json.dumps(result);

# end class CORSPreflight


def isValidCORSPreflight(cors):
  return isinstance(cors, CORSPreflight) and cors.isValidHeaderSet();


def mergeToWeakerCORSPreflight(cors1, cors2):
  # Not having a CORS response definitely blocks access.
  # So we take what we can get.
  if not isValidCORSPreflight(cors1) and not isValidCORSPreflight(cors2):
    return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE);
  elif not isValidCORSPreflight(cors1) and isValidCORSPreflight(cors2):
    return MergeResult(cors2, Ops.OP_USE_AS_MANIFEST_VALUE);
  elif isValidCORSPreflight(cors1) and not isValidCORSPreflight(cors2):
    return MergeResult(cors1, Ops.OP_USE_AS_MANIFEST_VALUE);

  result = CORSPreflight();
  result.origin = cors1.origin if cors1.origin == cors2.origin else "*";
  result.expose = cors1.expose.union(cors2.expose);
  result.maxage = min(cors1.maxage, cors2.maxage);
  # the wildcard and credentials do not work in combination. Note that:
  # - methods and headers are not part of the condition because of the "contains"
  #   in Fetch 4.7.6.5 and 4.7.6.6., respectively. I interpret the "contains" as 
  #   "can contain the * alongside other values which then work with credentials"
  # - expose is not part of the condition because of Fetch 4.1.7.1.3.
  result.creds = (cors1.creds or cors2.creds) and result.origin != "*";
  result.methods = cors1.methods.union(cors2.methods);
  result.headers = cors1.headers.union(cors2.headers);

  if result.isValidHeaderSet():
    return MergeResult(result, Ops.OP_USE_AS_MANIFEST_VALUE);
  return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE, "Invalid headers.");


def mergeToStricterCORSPreflight(cors1, cors2):
  # If we cannot merge it for sure we do not return a CORS decision.
  # Nothing is more restrictive because no CORS response blocks for sure.
  if not isValidCORSPreflight(cors1) or not isValidCORSPreflight(cors2):
    return MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT);

  result = CORSPreflight();
  # Access-Control-Allow-Origin may be set to the wildcard * but might be unmergable
  # example: how would you strict-merge a.com and b.com? Well, it's not possible
  if cors1.origin == "*" and isinstance(cors2.origin, str):
    result.origin = cors2.origin;
  elif (isinstance(cors1.origin, str) and cors2.origin == "*") \
       or (cors1.origin == cors2.origin):
    result.origin = cors1.origin;
  else:
    return MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT);
  # Access-Control-Expose-Headers may contain the wildcard * but in a special
  # case an exposed header can be even named *. So we do nothing special here.
  # See Fetch 4.1.7.1 for details
  # The header is not mandatory. So even when empty it is valid CORS
  result.expose = cors1.expose.intersection(cors2.expose);
  result.maxage = max(cors1.maxage, cors2.maxage);
  # the wildcard and credentials do not work in combination
  result.creds = cors1.creds == True and cors2.creds == True \
                   and isinstance(result.origin, str) and result.origin != "*";
  # Access-Control-Allow-Methods may contain the wildcard *
  # if in the last case * and result.creds == True should come together the 
  # validity check will detect this in the end.
  if "*" in cors1.methods and "*" not in cors2.methods:
    result.methods = set(cors2.methods);
  elif "*" not in cors1.methods and "*" in cors2.methods:
    result.methods = set(cors1.methods);
  else:
    result.methods = cors1.methods.intersection(cors2.methods);
  # Access-Control-Allow-Headers may contain the wildcard *
  # same as for methods regarding * and result.creds
  if "*" in cors1.headers and "*" not in cors2.headers:
    result.headers = set(cors2.headers);
  elif "*" not in cors1.headers and "*" in cors2.headers:
    result.headers = set(cors1.headers);
  else:
    result.headers = cors1.headers.intersection(cors2.headers);

  if result.isValidHeaderSet():
    return MergeResult(result, Ops.OP_USE_IN_RESPONSE_HEADER);
  return MergeResult(None, Ops.OP_SEND_CORS_PREFLIGHT, "Invalid headers.");



def mergeToWeaker(cors1_str, cors2_str):
  if not isinstance(cors1_str, str) or not isinstance(cors2_str, str):
    raise TypeError("'mergeToWeaker' expects strings only.");

  try:
    list1 = json.loads(cors1_str);
    list2 = json.loads(cors2_str);
  except Exception as e:
    return MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE,
               "Input data invalid because it is not valid JSON: {0}".format(e));
  result = mergeToWeakerCORSPreflight(CORSPreflight.FromStrings(list1),
                             CORSPreflight.FromStrings(list2));
  if isinstance(result.value, CORSPreflight):
    result.value = str(result.value);
  return result;


def mergeToStricter(cors1_str, cors2_str):
  if not isinstance(cors1_str, str) or not isinstance(cors2_str, str):
    raise TypeError("'mergeToStricter' expects strings only.");

  try:
    list1 = json.loads(cors1_str);
    list2 = json.loads(cors2_str);
  except Exception as e:
    return MergeResult(None, Ops.OP_FALLBACK_TO_CORS_RESPONSE,
               "Input data invalid because it is not valid JSON: {0}".format(e));
  result = mergeToStricterCORSPreflight(CORSPreflight.FromStrings(list1),
                               CORSPreflight.FromStrings(list2));
  if isinstance(result.value, CORSPreflight):
    result.value = str(result.value);
  return result;


# Note that this is just for convenience and code safety. 
# Since CORS headers in Origin Manifest are meant as a way of sending out a CORS
# request over the network, there should not be a situation where you need to 
# merge actual headers against the policy from the Origin Manifest.
def applyToHeader(policy, header):
  return mergeToStricter(policy,header);
