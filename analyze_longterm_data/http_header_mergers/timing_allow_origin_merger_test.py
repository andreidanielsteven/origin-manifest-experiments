import sys 

from .timing_allow_origin_merger import *


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testTimingAllowOriginFromString();
  testMergeToWeakerTimingAllowOrigin();
  testMergeToStricterTimingAllowOrigin();
  testTimingAllowOrigintoString();
  testMergeToWeaker();
  testMergeToStricter();


def testTimingAllowOriginFromString():
  print("Running 'testTimingAllowOriginFromString'...");
  test_cases = [
    { "source": "", "expected": TimingAllowOrigin() },
    { "source": "", "expected": TimingAllowOrigin(False) },
    { "source": "*", "expected": TimingAllowOrigin(True) },
    { "source": "asdf", "expected": TimingAllowOrigin(False,{"asdf"}) },
    { "source": "* a.com", "expected": TimingAllowOrigin(False,{"* a.com"}) },
    { "source": "*, a.com", "expected": TimingAllowOrigin(True,{"a.com"}) },
    { "source": "a.com,b.com",
      "expected": TimingAllowOrigin(False,{"a.com","b.com"}) },
    { "source": "   a.com   ,   b.com   ",
      "expected": TimingAllowOrigin(False,{"a.com","b.com"}) },
    { "source": ",a.com,b.com,",
      "expected": TimingAllowOrigin(False,{"a.com","b.com"}) },
    { "source": "a.com, b.com, a.com",
      "expected": TimingAllowOrigin(False,{"a.com","b.com"}) },
    { "source": "b.com,a.com",
      "expected": TimingAllowOrigin(False,{"a.com","b.com"}) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    tao = TimingAllowOrigin.FromString(test["source"]);
    if tao != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' parsed to '{2}' but '{3}' was expected.".format( \
             index, test["source"], tao, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerTimingAllowOrigin():
  print("Running 'testMergeToWeakerTimingAllowOrigin'...");
  test_cases = [
    { "policy1": TimingAllowOrigin(), "policy2": TimingAllowOrigin(),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": TimingAllowOrigin(False), "policy2": TimingAllowOrigin(False),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": TimingAllowOrigin(True), "policy2": TimingAllowOrigin(False),
      "expected": MergeResult(TimingAllowOrigin(True),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": TimingAllowOrigin(True), "policy2": TimingAllowOrigin(True),
      "expected": MergeResult(TimingAllowOrigin(True),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": TimingAllowOrigin(True),
      "policy2": TimingAllowOrigin(False,{"a.com"}),
      "expected": MergeResult(TimingAllowOrigin(True),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": TimingAllowOrigin(True,{"a.com"}),
      "policy2": TimingAllowOrigin(False,{"b.com"}),
      "expected": MergeResult(TimingAllowOrigin(True, set()),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": TimingAllowOrigin(False, set()),
      "policy2": TimingAllowOrigin(False, set()),
      "expected": MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": TimingAllowOrigin(False,{"a.com"}),
      "policy2": TimingAllowOrigin(False,{"a.com"}),
      "expected": MergeResult(TimingAllowOrigin(False, {"a.com"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": TimingAllowOrigin(False,{"a.com"}),
      "policy2": TimingAllowOrigin(False,{"b.com"}),
      "expected": MergeResult(TimingAllowOrigin(False, {"a.com","b.com"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": TimingAllowOrigin(False,{"a.com","b.com"}),
      "policy2": TimingAllowOrigin(False,{"b.com","c.com"}),
      "expected": MergeResult(TimingAllowOrigin(False, {"a.com","b.com","c.com"}),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    tao1 = mergeToWeakerTimingAllowOrigin(test["policy1"],test["policy2"]);
    tao2 = mergeToWeakerTimingAllowOrigin(test["policy2"],test["policy1"]);
    if tao1 != test["expected"] or tao2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], tao1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterTimingAllowOrigin():
  print("Running 'testMergeToStricterTimingAllowOrigin'...");
  test_cases = [
    { "policy1": TimingAllowOrigin(), "policy2": TimingAllowOrigin(),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": TimingAllowOrigin(False), "policy2": TimingAllowOrigin(False),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": TimingAllowOrigin(True), "policy2": TimingAllowOrigin(False),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": TimingAllowOrigin(True), "policy2": TimingAllowOrigin(True),
      "expected": MergeResult(TimingAllowOrigin(True),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": TimingAllowOrigin(True),
      "policy2": TimingAllowOrigin(False,{"a.com"}),
      "expected": MergeResult(TimingAllowOrigin(False,{"a.com"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": TimingAllowOrigin(True,{"a.com"}),
      "policy2": TimingAllowOrigin(False,{"b.com"}),
      "expected": MergeResult(TimingAllowOrigin(False,{"b.com"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": TimingAllowOrigin(False, set()),
      "policy2": TimingAllowOrigin(False, set()),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": TimingAllowOrigin(False,{"a.com"}),
      "policy2": TimingAllowOrigin(False,{"a.com"}),
      "expected": MergeResult(TimingAllowOrigin(False,{"a.com"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": TimingAllowOrigin(False,{"a.com"}),
      "policy2": TimingAllowOrigin(False,{"b.com"}),
      "expected": MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": TimingAllowOrigin(False,{"a.com","b.com"}),
      "policy2": TimingAllowOrigin(False,{"b.com","c.com"}),
      "expected": MergeResult(TimingAllowOrigin(False,{"b.com"}),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    tao1 = mergeToStricterTimingAllowOrigin(test["policy1"],test["policy2"]);
    tao2 = mergeToStricterTimingAllowOrigin(test["policy2"],test["policy1"]);
    if tao1 != test["expected"] or tao2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], tao1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testTimingAllowOrigintoString():
  print("Running 'testTimingAllowOrigintoString'...");
  test_cases = [
    { "source": TimingAllowOrigin(), "expected": "" },
    { "source": TimingAllowOrigin(False), "expected": "" },
    { "source": TimingAllowOrigin(False,set()), "expected": "" },
    { "source": TimingAllowOrigin(True), "expected": "*" },
    { "source": TimingAllowOrigin(True, {"a.com"}), "expected": ["*, a.com", "a.com, *"] },
    { "source": TimingAllowOrigin(False, {"a.com"}), "expected": "a.com" },
    { "source": TimingAllowOrigin(False, {"a.com","b.com"}),
      "expected": ["a.com, b.com", "b.com, a.com"] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output_str = test["source"].toString();
    if output_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' transformed to '{2}' but any in '{3}' was "
            "expected.".format(index, test["source"], output_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": [MergeResult(None, Ops.OP_DELETE_MANIFEST_VALUE)] },
    { "policy1": "*", "policy2": "",
      "expected": [MergeResult("*", Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "*", "policy2": "*",
      "expected": [MergeResult("*", Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "*", "policy2": "a.com",
      "expected": [MergeResult("*", Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "*, a.com", "policy2": "b.com",
      "expected": [MergeResult("*", Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "a.com, *", "policy2": "b.com",
      "expected": [MergeResult("*", Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "a.com", "policy2": "a.com",
      "expected": [MergeResult("a.com", Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "a.com", "policy2": "b.com",
      "expected": [MergeResult("a.com, b.com", Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("b.com, a.com", Ops.OP_USE_AS_MANIFEST_VALUE)] },
    { "policy1": "a.com, b.com", "policy2": "b.com, c.com",
      "expected": [MergeResult("a.com, b.com, c.com", Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("a.com, c.com, b.com", Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("b.com, a.com, c.com", Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("b.com, c.com, a.com", Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("c.com, a.com, b.com", Ops.OP_USE_AS_MANIFEST_VALUE),
                   MergeResult("c.com, b.com, a.com", Ops.OP_USE_AS_MANIFEST_VALUE)] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToWeaker(test["policy1"],test["policy2"]);
    result2 = mergeToWeaker(test["policy1"],test["policy2"]);
    if result1 not in test["expected"] or result2 not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": [MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER)] },
    { "policy1": "*", "policy2": "",
      "expected": [MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER)] },
    { "policy1": "*", "policy2": "*",
      "expected": [MergeResult("*", Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "*", "policy2": "a.com",
      "expected": [MergeResult("a.com", Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "*, a.com", "policy2": "b.com",
      "expected": [MergeResult("b.com", Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "a.com, *", "policy2": "b.com",
      "expected": [MergeResult("b.com", Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "a.com", "policy2": "a.com",
      "expected": [MergeResult("a.com", Ops.OP_USE_IN_RESPONSE_HEADER)] },
    { "policy1": "a.com", "policy2": "b.com",
      "expected": [MergeResult(None, Ops.OP_DELETE_RESPONSE_HEADER)] },
    { "policy1": "a.com, b.com", "policy2": "b.com, c.com",
      "expected": [MergeResult("b.com", Ops.OP_USE_IN_RESPONSE_HEADER)] },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToStricter(test["policy1"],test["policy2"]);
    result2 = mergeToStricter(test["policy1"],test["policy2"]);
    if result1 not in test["expected"] or result2 not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
