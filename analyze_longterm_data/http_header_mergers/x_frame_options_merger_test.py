import sys 

from .x_frame_options_merger import *
from .merge_result import MergeResult
from .merge_operation import Ops


###############################################################################
# Configuration and constants                                                 #
###############################################################################

# Python version check
if (sys.version_info.major != 3):
  print("Please run this script with python3.");
  sys.exit();


###############################################################################
# Tests                                                                       #
###############################################################################

def test():
  testXFrameOptionsfromString();
  testMergeToWeakerXFrameOptions();
  testMergeToStricterXFrameOptions();
  testXFrameOptionstoString();
  testMergeToWeaker();
  testMergeToStricter();


def testXFrameOptionsfromString():
  print("Running 'testXFrameOptionsfromString'...");
  test_cases = [
    { "source": "", "expected": XFrameOptions() },
    { "source": "", "expected": XFrameOptions(None) },
    { "source": "asdf", "expected": XFrameOptions() },
    { "source": "ALLOW-FROM", "expected": XFrameOptions() },
    { "source": "ALLOW-FROM asdf", "expected": XFrameOptions() },
    { "source": "SAMEORIGIN", "expected": XFrameOptions("SAMEORIGIN") },
    { "source": "  SAMEORIGIN   ", "expected": XFrameOptions("SAMEORIGIN") },
    { "source": "SAMEORIGIN asdf", "expected": XFrameOptions() },
    { "source": "asdf DENY", "expected": XFrameOptions() },
    { "source": "DENY", "expected": XFrameOptions("DENY") },
    { "source": "   DENY     ", "expected": XFrameOptions("DENY") },
    { "source": "DENY asdf", "expected": XFrameOptions() },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    xfo = XFrameOptions.FromString(test["source"]);
    if xfo != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' parsed to '{2}' but '{3}' was expected.".format( \
             index, test["source"], xfo, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeakerXFrameOptions():
  print("Running 'testMergeToWeakerXFrameOptions'...");
  test_cases = [
    { "policy1": XFrameOptions(),
      "policy2": XFrameOptions(),
      "expected": MergeResult(XFrameOptions(None), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XFrameOptions("ALLOW-FROM asdf"),
      "policy2": XFrameOptions(),
      "expected": MergeResult(XFrameOptions(None), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XFrameOptions("ALLOW-FROM asdf"),
      "policy2": XFrameOptions("SAMEORIGIN"),
      "expected": MergeResult(XFrameOptions(None), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XFrameOptions("ALLOW-FROM asdf"),
      "policy2": XFrameOptions("DENY"),
      "expected": MergeResult(XFrameOptions(None), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XFrameOptions("SAMEORIGIN"),
      "policy2": XFrameOptions(),
      "expected": MergeResult(XFrameOptions(None), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XFrameOptions("SAMEORIGIN"),
      "policy2": XFrameOptions("SAMEORIGIN"),
      "expected": MergeResult(XFrameOptions("SAMEORIGIN"),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": XFrameOptions("SAMEORIGIN"),
      "policy2": XFrameOptions("DENY"),
      "expected": MergeResult(XFrameOptions("SAMEORIGIN"),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": XFrameOptions("DENY"),
      "policy2": XFrameOptions(),
      "expected": MergeResult(XFrameOptions(None), Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": XFrameOptions("DENY"),
      "policy2": XFrameOptions("DENY"),
      "expected": MergeResult(XFrameOptions("DENY"),
                              Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    xfo1 = mergeToWeakerXFrameOptions(test["policy1"],test["policy2"]);
    xfo2 = mergeToWeakerXFrameOptions(test["policy2"],test["policy1"]);
    if xfo1 != test["expected"] or xfo2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], xfo1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricterXFrameOptions():
  print("Running 'testMergeToStricterXFrameOptions'...");
  test_cases = [
    { "policy1": XFrameOptions(),
      "policy2": XFrameOptions(),
      "expected": MergeResult(XFrameOptions(None), Ops.OP_DELETE_RESPONSE_HEADER) },
  ];
  '''
    { "policy1": XFrameOptions("ALLOW-FROM asdf"),
      "policy2": XFrameOptions(),
      "expected": MergeResult(XFrameOptions(None), Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": XFrameOptions("ALLOW-FROM asdf"),
      "policy2": XFrameOptions("SAMEORIGIN"),
      "expected": MergeResult(XFrameOptions("SAMEORIGIN"),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XFrameOptions("ALLOW-FROM asdf"),
      "policy2": XFrameOptions("DENY"),
      "expected": MergeResult(XFrameOptions("DENY"),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XFrameOptions("SAMEORIGIN"),
      "policy2": XFrameOptions(),
      "expected": MergeResult(XFrameOptions("SAMEORIGIN"),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XFrameOptions("SAMEORIGIN"),
      "policy2": XFrameOptions("SAMEORIGIN"),
      "expected": MergeResult(XFrameOptions("SAMEORIGIN"),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XFrameOptions("SAMEORIGIN"),
      "policy2": XFrameOptions("DENY"),
      "expected": MergeResult(XFrameOptions("DENY"),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XFrameOptions("DENY"),
      "policy2": XFrameOptions(),
      "expected": MergeResult(XFrameOptions("DENY"),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": XFrameOptions("DENY"),
      "policy2": XFrameOptions("DENY"),
      "expected": MergeResult(XFrameOptions("DENY"),
                              Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  '''
  success = True;
  for index,test in enumerate(test_cases):
    xfo1 = mergeToStricterXFrameOptions(test["policy1"],test["policy2"]);
    xfo2 = mergeToStricterXFrameOptions(test["policy2"],test["policy1"]);
    if xfo1 != test["expected"] or xfo2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], xfo1, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testXFrameOptionstoString():
  print("Running 'testXFrameOptionstoString'...");
  test_cases = [
    { "source": XFrameOptions(), "expected": "" },
    { "source": XFrameOptions("ALLOW-Origin asdf"), "expected": "" },
    { "source": XFrameOptions("SAMEORIGIN"), "expected": "SAMEORIGIN" },
    { "source": XFrameOptions("    SAMEORIGIN    "), "expected": "SAMEORIGIN" },
    { "source": XFrameOptions("DENY"), "expected": "DENY" },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    output_str = test["source"].toString();
    if output_str not in test["expected"]:
      success = False;
      print("test {0} failed: '{1}' transformed to '{2}' but any in '{3}' was "
            "expected.".format(index, test["source"], output_str, test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToWeaker():
  print("Running 'testMergeToWeaker'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "asdf", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "ALLOW-FROM asdf",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "ALLOW-FROM qwer",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "SAMEORIGIN",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "DENY",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "SAMEORIGIN", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "SAMEORIGIN", "policy2": "SAMEORIGIN",
      "expected": MergeResult("SAMEORIGIN", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "SAMEORIGIN", "policy2": "DENY",
      "expected": MergeResult("SAMEORIGIN", Ops.OP_USE_AS_MANIFEST_VALUE) },
    { "policy1": "DENY", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_MANIFEST_VALUE) },
    { "policy1": "DENY", "policy2": "DENY",
      "expected": MergeResult("DENY", Ops.OP_USE_AS_MANIFEST_VALUE) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToWeaker(test["policy1"],test["policy2"]);
    result2 = mergeToWeaker(test["policy1"],test["policy2"]);
    if result1 != test["expected"] or result1 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1,
                    test["expected"]));
      break;
  print("success." if success else "FAILED.");


def testMergeToStricter():
  print("Running 'testMergeToStricter'...");
  test_cases = [
    { "policy1": "", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "asdf", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "ALLOW-FROM asdf",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "ALLOW-FROM qwer",
      "expected": MergeResult("", Ops.OP_DELETE_RESPONSE_HEADER) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "SAMEORIGIN",
      "expected": MergeResult("SAMEORIGIN", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "ALLOW-FROM asdf", "policy2": "DENY",
      "expected": MergeResult("DENY", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "SAMEORIGIN", "policy2": "",
      "expected": MergeResult("SAMEORIGIN", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "SAMEORIGIN", "policy2": "SAMEORIGIN",
      "expected": MergeResult("SAMEORIGIN", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "SAMEORIGIN", "policy2": "DENY",
      "expected": MergeResult("DENY", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "DENY", "policy2": "",
      "expected": MergeResult("DENY", Ops.OP_USE_IN_RESPONSE_HEADER) },
    { "policy1": "DENY", "policy2": "DENY",
      "expected": MergeResult("DENY", Ops.OP_USE_IN_RESPONSE_HEADER) },
  ];
  success = True;
  for index,test in enumerate(test_cases):
    result1 = mergeToStricter(test["policy1"],test["policy2"]);
    result2 = mergeToStricter(test["policy1"],test["policy2"]);
    if result1 != test["expected"] or result2 != test["expected"]:
      success = False;
      print("test {0} failed: '{1}' and '{2}' merged to '{3}' but '{4}' was expected."
            .format(index, test["policy1"], test["policy2"], result1,
                    test["expected"]));
      break;
  print("success." if success else "FAILED.");


###############################################################################
# Main                                                                        #
###############################################################################

if __name__ == "__main__":
  test();
