# -*- coding: utf-8 -*-

import http.client, urllib.parse, json, sys, time
from collections import OrderedDict

# **********************************************
# *** Update or verify the following values. ***
# **********************************************


# Verify the endpoint URI.  At this writing, only one endpoint is used for Bing
# search APIs.  In the future, regional endpoints may be available.  If you
# encounter unexpected authorization errors, double-check this value against
# the endpoint for your Bing Web search instance in your Azure dashboard.
HOST = "api.cognitive.microsoft.com"
PATH = "/bing/v7.0/search"


def BingWebSearch(subscriptionKey,search,page,count=50):
    "Performs a Bing Web search and returns the results."

    headers = {'Ocp-Apim-Subscription-Key': subscriptionKey}
    conn = http.client.HTTPSConnection(HOST)
    query = urllib.parse.quote(search)
    params = [
      "safeSearch=off",
      "count={0}".format(count),
      "offset={0}".format(page * count),
      "responseFilter=webpages",
      "q=" + query
    ];
    conn.request("GET", PATH + "?" + "&".join(params), headers=headers)
    response = conn.getresponse()
    return response.read().decode("utf8")


def GetURLsForDomain(subscriptionKey,domain, min_results, max_pages, new_page_query_delay):
      success = True
      urls = OrderedDict()
      message = "okay"

      term = "site:{0}".format(domain)

      page = 0
      result = BingWebSearch(subscriptionKey,term,page)
      try:
        result = json.loads(result)
        urls = OrderedDict([(r['url'],None) for r in result['webPages']['value']])
        while page < max_pages and len(urls) < min_results:
          page = page + 1
          time.sleep(new_page_query_delay)
          result = BingWebSearch(subscriptionKey,term,page)
          result = json.loads(result)
          if 'webPages' in result and 'value' in result['webPages']:
            urls.update([(r['url'],None) for r in result['webPages']['value']])
          else:
            break
      except:
        success = False
        message = json.dumps(result, indent=2)

      return { "success": success, "urls": list(urls.keys()), "pages": page, "message": message }


def extractDomainFromLine(line):
  line = line.splitlines()
  line = [x.strip() for x in line[0].split(',')]
  if len(line) == 1 and line[0] != "":
    return line[0]
  elif len(line) > 1:
    return line[1]
  else:
    return None 


def main():
    subscriptionKey = None
    domains_file = None
    output_file = None
    first_domain = None
    max_domains = None
    min_results = None
    max_pages = None
    new_page_query_delay = None
    if len(sys.argv) > 7:
      subscriptionKey = sys.argv[1]
      domains_file = sys.argv[2]
      output_file = sys.argv[3]
      # there is no check for negative values. But hey, screw it!
      first_domain = int(sys.argv[4]) if sys.argv[4].isdigit() else None
      max_domains = int(sys.argv[5]) if sys.argv[5].isdigit() else None
      min_results = int(sys.argv[6]) if sys.argv[6].isdigit() else None
      max_pages = int(sys.argv[7]) if sys.argv[7].isdigit() else None
      new_page_query_delay = float(sys.argv[8]) if sys.argv[8].replace('.','',1).isdigit() else None

    if not (subscriptionKey and len(subscriptionKey) == 32 and domains_file and output_file \
            and first_domain != None and max_domains != None and min_results != None \
            and max_pages != None and new_page_query_delay != None):
      print("Usage: {0} <API key> <domains file> <output file> <first domain position> "
            "<max #domains> <#urls per domain> <#retries per domain> <delay between queries in sec>"
              .format(sys.argv[0]))
      return

    results = dict()
    success_counter = 0
    page_limit_exeeded = 0
    fail_messages = dict()
    with open(domains_file) as domains:
      domains_counter = 0
      t_start = time.time()
      for domain in domains:
        # skip everything until we reach the first domain to process
        if domains_counter < first_domain:
          domains_counter += 1
          continue

        # after processing a certain number of domains we stop
        if domains_counter-first_domain >= max_domains:
          break

        domains_counter += 1
        domain = extractDomainFromLine(domain)
        if domain == None:
          continue

        print("Processing '{0}'... ({1})".format(domain, domains_counter))
        result = GetURLsForDomain(subscriptionKey, domain, min_results, max_pages, new_page_query_delay)
        results[domain] = result["urls"]
        if result["success"]:
          success_counter += 1
        else:
          fail_messages[domain] = result["message"]
        if len(result["urls"]) < min_results and result["pages"] == max_pages:
          page_limit_exeeded += 1
        print("Done (success: {0}, #urls: {1}, #pages: {2})."
                .format(result["success"],len(result["urls"]),result["pages"]))
    t_delta = time.time() - t_start
    json.dump(results, open(output_file, "w"))

    print("-"*80)
    print("Result written to file '{0}'".format(output_file))
    print("#domains: {0}".format(len(results)))
    print("successes: {0}, failures: {1}".format(success_counter, len(fail_messages)))
    print("page limit exceeded: {0}".format(page_limit_exeeded))
    print("time: {0}".format(round(t_delta,1)))
    if len(fail_messages) > 0:
      print("\nFailed domains:")
      for domain in fail_messages:
        print("  {0}".format(domain))


if __name__ == '__main__':
    main()

