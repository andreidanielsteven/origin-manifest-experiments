import json, os, re, sys
from datetime import datetime

STATUS_FILES = set([
  'mid-tcp-counter-after-cached.txt',
  'mid-tcp-counter-after-uncached.txt',
  'mid-tcp-counter-before-cached.txt',
  'mid-tcp-counter-before-uncached.txt',
  'post-tcp-counter-after-cached.txt',
  'post-tcp-counter-after-uncached.txt',
  'post-tcp-counter-before-cached.txt',
  'post-tcp-counter-before-uncached.txt',
  'pre-tcp-counter-after-cached.txt',
  'pre-tcp-counter-after-uncached.txt',
  'pre-tcp-counter-before-cached.txt',
  'pre-tcp-counter-before-uncached.txt',
])
SERVERMANIFESTS = 'servermanifests'
COMMENTS_SUFFIX = "_comments"

BASE_DIR = sys.argv[1].rstrip("/")
RESULT_FILE = sys.argv[2]
DETAILED_RESULT_FILE = sys.argv[3]


result = dict([(5,dict())]+[(i,dict()) for i in range(10,101,10)])
detailed_result = dict()

def recordGeneralStistics(origin, servermanifest, section, header):
  global result, detailed_result
  comment_header = servermanifest[section][header]
  if "alternativeValue" in comment_header and "msg" in comment_header \
      and len(comment_header["alternativeValue"]) > 0:
    actual_section = re.match("(.+)_comments", section)[1]
    digits = re.match("Result of (\d+) merged headers. Without (\d+) headers",
                      comment_header["msg"])
    ratio = (int(digits[2])/int(digits[1]))*100
    for i in [i for i in result if ratio <= i]:
      if "total" not in result[i]:
        result[i]["total"] = 0
      result[i]["total"] += 1
      if actual_section not in result[i]:
        result[i][actual_section] = dict()
      if header not in result[i][actual_section]:
        result[i][actual_section][header] = 0
      result[i][actual_section][header] += 1
    if ratio <= 5:
      recordDetailedStatistics(origin, actual_section, header,
                               servermanifest[actual_section][header]
                                 if header in servermanifest[actual_section] else "",
                               comment_header["alternativeValue"])


def recordDetailedStatistics(origin, actual_section, header, actualValue, alternativeValue):
  global detailed_result
  if actual_section not in detailed_result:
    detailed_result[actual_section] = dict()
  if header not in detailed_result[actual_section]:
    detailed_result[actual_section][header] = list()
  detailed_values = {
    "actualValue": actualValue,
    "alternativeValue": alternativeValue
  }
  for dr in (dr for dr in detailed_result[actual_section][header] if dr["values"] == detailed_values):
    dr["counter"] += 1
    dr["origins"].append(origin)
    break
  else:
    detailed_result[actual_section][header].append({
      "counter": 1,
      "origins": [origin],
      "values": detailed_values
    })


def main():
  start_time = datetime.now()
  print("Start: {0}".format(start_time))

  localhosts = list()
  domain_dirs = next(os.walk(BASE_DIR))[1]
  dir_counter = 0
  for domain_dir in domain_dirs:
    dir_counter += 1
    print("Processing... [{0}/{1}]".format(dir_counter, len(domain_dirs)), end="\r")
    domain_dir_content = next(os.walk("{0}/{1}".format(BASE_DIR,domain_dir)))
    if not STATUS_FILES.issubset(domain_dir_content[2]) \
        or SERVERMANIFESTS not in domain_dir_content[1]:
      continue
    servermanifests_dir = next(os.walk("{0}/{1}".format(domain_dir_content[0],SERVERMANIFESTS)))
    for json_file in servermanifests_dir[2]:
      # let's ignore localhosts
      if re.match(".*,localhost,.*", json_file):
        localhosts.append(json_file)
        continue
      servermanifest = json.load(open("{0}/{1}".format(servermanifests_dir[0],json_file), "r"))
      for section in (s for s in servermanifest if s.endswith(COMMENTS_SUFFIX)):
        manifest_section = servermanifest[section]
        for header in (h for h in manifest_section if isinstance(manifest_section[h],dict)):
          recordGeneralStistics(json_file, servermanifest, section, header)
  print("Finished...   [{0}/{1}]".format(dir_counter, len(domain_dirs)))
  json.dump(result, open(RESULT_FILE, "w"), indent=2)
  json.dump(detailed_result, open(DETAILED_RESULT_FILE, "w"), indent=2)
  end_time = datetime.now()
  print("End: {0}".format(end_time))
  print("Duration: {0}".format(end_time - start_time))
  print("The following origins were ignored:")
  for lo in localhosts:
    print("\t{0}".format(lo))


if __name__ == '__main__':
  main()
