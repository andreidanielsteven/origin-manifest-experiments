import json, os, re, sys
from datetime import datetime

COUNTER_FILES = set([
  'pre-tcp-counter-before-uncached.txt',
  'mid-tcp-counter-before-uncached.txt',
  'post-tcp-counter-before-uncached.txt',

  'pre-tcp-counter-before-cached.txt',
  'mid-tcp-counter-before-cached.txt',
  'post-tcp-counter-before-cached.txt',

  'pre-tcp-counter-after-uncached.txt',
  'mid-tcp-counter-after-uncached.txt',
  'post-tcp-counter-after-uncached.txt',

  'mid-tcp-counter-after-cached.txt',
  'post-tcp-counter-after-cached.txt',
  'pre-tcp-counter-after-cached.txt',
])

BASE_DIR = sys.argv[1].rstrip("/")

result = {
  "before": {
    "uncached": dict(
      [(k, { "received": 0, "sent": 0 }) for k in ["pre","mid","post"]]
    ),
    "cached": dict(
      [(k, { "received": 0, "sent": 0 }) for k in ["pre","mid","post"]]
    ),
  },
  "after": {
    "uncached": dict(
      [(k, { "received": 0, "sent": 0 }) for k in ["pre","mid","post"]]
    ),
    "cached": dict(
      [(k, { "received": 0, "sent": 0 }) for k in ["pre","mid","post"]]
    ),
  }
}


def main():
  start_time = datetime.now()
  print("Start: {0}".format(start_time))

  domain_dirs = next(os.walk(BASE_DIR))[1]
  dir_counter = 0
  total = len(domain_dirs)
  analysed_domains = 0
  ignored = 0
  for domain_dir in domain_dirs:
    dir_counter += 1
    print("Processing... [{0}/{1}]".format(dir_counter, total), end="\r")
    domain_dir_content = next(os.walk("{0}/{1}".format(BASE_DIR,domain_dir)))
    if not COUNTER_FILES.issubset(domain_dir_content[2]):
      ignored += 1
      continue
    for counter_file in COUNTER_FILES:
      tcp_counter = json.load(open("{0}/{1}".format(domain_dir_content[0],counter_file), "r"))
      # example: mid-tcp-counter-after-uncached.txt
      name_parts = re.match("(.+)-tcp-counter-(.+)-(.+).txt", counter_file)
      result[name_parts[2]][name_parts[3]][name_parts[1]]["received"] += tcp_counter["received"]
      result[name_parts[2]][name_parts[3]][name_parts[1]]["sent"] += tcp_counter["sent"]
    analysed_domains += 1
  # compute "before" and "after" average values
  computeAverageValues(analysed_domains)
  # compute relative diff (after / before)*100
  computeRelativeDiff()
  print("Finished...   [{0}/{1}]".format(dir_counter, len(domain_dirs)))
  if len(sys.argv) > 2:
    json.dump(open(sys.argv[2],"w"),indent=2)
  else:
    print(json.dumps(result, indent=2))
  end_time = datetime.now()
  print("End: {0}".format(end_time))
  print("Duration: {0}".format(end_time - start_time))
  print("ignored: {0}".format(ignored))


def computeAverageValues(total):
  global result
  for before_after in ["before","after"]:
    for un_cached in ["cached","uncached"]:
      for pre_mid_post in ["pre","mid","post"]:
        for rec_sent in ["received","sent"]:
          result[before_after][un_cached][pre_mid_post]["total_{0}".format(rec_sent)] = \
              result[before_after][un_cached][pre_mid_post][rec_sent]
          result[before_after][un_cached][pre_mid_post][rec_sent] = \
              round(result[before_after][un_cached][pre_mid_post][rec_sent] / total, 2)
  return result


# the relative change in percent, e.g. 98% -> -2% because it was 2% less than before
def computeRelativeDiff():
  global result
  result["diff"] = dict()
  for un_cached in ["cached","uncached"]:
    result["diff"][un_cached] = dict()
    for pre_mid_post in ["pre","mid","post"]:
      result["diff"][un_cached][pre_mid_post] = dict()
      for rec_sent in ["received","sent"]:
        result["diff"][un_cached][pre_mid_post][rec_sent] = \
          round((result["after"][un_cached][pre_mid_post][rec_sent] \
                 / result["before"][un_cached][pre_mid_post][rec_sent]-1) * 100,2)


if __name__ == '__main__':
  main()
