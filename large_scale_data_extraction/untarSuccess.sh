#!/bin/bash

SOURCE_DIR=$1
TARGET_DIR=$2

date

TOTAL=`ls -1 $SOURCE_DIR/*.tar.xz | wc -l`
EXTRACTED=0
for a in `ls -1 $SOURCE_DIR/*.tar.xz`; do
  echo -n -e "Extracting... [$EXTRACTED/$TOTAL]\r"
  NEW_DIR=`basename $a .tar.xz`
  NEW_DIR=$TARGET_DIR/$NEW_DIR
  mkdir -p $NEW_DIR
  tar xf $a -C $NEW_DIR
  EXTRACTED=$((EXTRACTED+1))
done
echo "Extraction finished! [$EXTRACTED/$TOTAL]"

date
